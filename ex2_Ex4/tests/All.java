package teme.ex2_Ex4.tests;

import teme.ex2_Ex4.tests.Categories.Other;
import teme.ex2_Ex4.tests.Categories.Concatenate;
import teme.ex2_Ex4.GameSession;
import teme.ex2_Ex4.Producer;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertNotSame;

public class All {

    private GameSession g1;
    private GameSession g2;

    @Category(Other.class)
    @Test
    public void getPlayers() {
        g1 = new GameSession("HOF", 5, 5);
        String[] p1 = {"a", "b", "c"};
        g1.setPlayers(p1);
        assertNotSame(g1.getPlayers(), p1);

        //assertThat(g1.getPlayers()). == p1);
    }

    @Category(Other.class)
    @Test
    public void setPlayers() {
        g1 = new GameSession("HOF", 5, 5);
        String[] p1 = {"a", "b", "c"};
        g1.setPlayers(p1);
        assertNotSame(p1, g1.getPlayers());
        //assertThat(p1 == g1.getPlayers());
    }

    @Category(Other.class)
    @Test
    public void checkPlayer() {
        g1 = new GameSession("HOF", 5, 5);
        String[] p1 = {"a", "b", "c"};
        g1.setPlayers(p1);

        assertThat(g1.checkPlayer("b")).isTrue();
    }

    @Category(Other.class)
    @Test
    public void cloneAddress() throws CloneNotSupportedException {
        g1 = new GameSession();
        g2 = (GameSession) g1.clone();
        assertNotSame(g1, g2);
        //assertThat(g1 == g2);
    }

    @Category(Concatenate.class)
    @Test
    public void ifSameNameStartTime() {
        g1 = new GameSession("HOF", 1, 5);
        g2 = new GameSession("Ceasars", 0, 2);
        g1.concatenate(g2);

        assertThat(g1.getStartTime()).isNotEqualTo(g2.getStartTime());
    }

    @Category(Concatenate.class)
    @Test
    public void ifSameNameEndTime() {
        g1 = new GameSession("HOF", 1, 5);
        g2 = new GameSession("Ceasars", 0, 8);
        g1.concatenate(g2);

        assertThat(g1.getEndTime()).isNotEqualTo(g2.getEndTime());
    }

    @Category(Concatenate.class)
    @Test
    public void ifSameNameProducer() {
        g1 = new GameSession("HOF", 1, 5);
        g2 = new GameSession("Ceasars", 0, 8);
        g1.setProducer(Producer.PLAYTIKA);
        g2.setProducer(Producer.OTHER_VENDOR);
        g1.concatenate(g2);

        assertThat(g1.getProducer()).isNotEqualTo(g2.getProducer());
    }

    @Category(Concatenate.class)
    @Test
    public void ifSameNamePlayers() {
        g1 = new GameSession("HOF", 1, 5);
        g2 = new GameSession("Ceasars", 0, 8);
        String[] p1 = {"a", "b", "c"};
        String[] p2 = {"d", "b", "e"};
        g1.setPlayers(p1);
        g2.setPlayers(p2);
        g1.concatenate(g2);

        assertThat(g1.getPlayers()).containsExactly(p1);
    }

    @Category(Concatenate.class)
    @Test
    public void checkStartTime() {
        g1 = new GameSession("HOF", 1, 5);
        g2 = new GameSession("HOF", 0, 2);
        g1.concatenate(g2);

        assertThat(g1.getStartTime()).isEqualTo(0);
    }

    @Category(Concatenate.class)
    @Test
    public void checkEndTime() {
        g1 = new GameSession("HOF", 1, 5);
        g2 = new GameSession("HOF", 0, 2);
        g2.concatenate(g1);

        assertThat(g2.getEndTime()).isEqualTo(6);
    }

    @Category(Concatenate.class)
    @Test
    public void checkProducer() {
        g1 = new GameSession();
        g2 = new GameSession();
        g1.setProducer(Producer.PLAYTIKA);
        g2.setProducer(Producer.OTHER_VENDOR);
        g1.concatenate(g2);

        assertThat(g2.getProducer()).isEqualTo(Producer.OTHER_VENDOR);
    }

    @Category(Concatenate.class)
    @Test
    public void checkPlayers() {
        g1 = new GameSession();
        g2 = new GameSession();
        String[] players = {"a", "b"};
        String[] players2 = {"a", "f", "g"};
        g1.setPlayers(players);
        g2.setPlayers(players2);
        g1.concatenate(g2);
        String[] result = {"a", "b", "f", "g"};

        assertThat(g1.getPlayers()).containsExactly(result);
    }

    @Category(Concatenate.class)
    @Test
    public void checkIfPlayersNull() {
        g1 = new GameSession();
        g2 = new GameSession();
        String[] players = {"a", "b"};
        g2.setPlayers(players);
        g1.concatenate(g2);

        assertThat(g1.getPlayers()).isEqualTo(g2.getPlayers());
    }

}
