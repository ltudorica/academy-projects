package teme.ex2_Ex4.tests.suites;


import teme.ex2_Ex4.tests.All;
import teme.ex2_Ex4.tests.Categories.Concatenate;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Suite.SuiteClasses(All.class)
@Categories.IncludeCategory(Concatenate.class)
public class ConcatenateTests {
}
