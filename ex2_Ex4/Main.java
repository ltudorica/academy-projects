package teme.ex2_Ex4;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        GameSession g1 = new GameSession();
        GameSession g2 = new GameSession("HOF", 2, 3);

        System.out.println(g2.getGameName());
        g1.setGameName("HOF");
        System.out.println(g1.getGameName());

        String[] players = {"Ana", "George", "Andreea"};
        g1.setPlayers(players);

        String[] players2 = {"Oana", "Ioana", "George"};
        g2.setPlayers(players2);

        System.out.println(Arrays.toString(g2.getPlayers())); //print the array in a string
        System.out.println(g1.checkPlayer("Ana"));
        System.out.println(g1.checkPlayer("Alina"));

        /*GameSession g3 = null;
        if (g1 instanceof Cloneable) {
            System.out.println("testing");
            g3 = (GameSession) g1.clone();
        }*/

        GameSession g3 = (GameSession) g1.clone();

        System.out.println(g1.getPlayers());
        System.out.println(g2.getPlayers());
        g1.concatenate(g2);
        System.out.println("g1 game name " + g1.getGameName());
        System.out.println("g2 game name " + g2.getGameName());
        System.out.println("g1 start time" + g1.getStartTime());
        System.out.println("g2 start time" + g2.getStartTime());
        System.out.println("g1 end time" + g1.getEndTime());
        System.out.println("g2 end time" + g2.getEndTime());

        for (String s : g1.getPlayers()) {
            System.out.print(s + " ");
        }

    }
}
