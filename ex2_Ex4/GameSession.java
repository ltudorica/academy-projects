package teme.ex2_Ex4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameSession implements Cloneable {
    private String gameName;
    private int startTime;
    private int endTime;
    private Producer producer;
    private String[] players;

    public GameSession() {
        this.gameName = "";
        this.startTime = 0;
        this.endTime = 1;
        this.producer = Producer.PLAYTIKA;
    }

    public GameSession(String gameName, int startTime, int duration) {
        this.gameName = gameName;
        this.startTime = startTime;
        this.endTime = startTime + duration;
        this.producer = Producer.PLAYTIKA;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String[] getPlayers() {
        if (this.players != null) {
            String[] s1 = new String[this.players.length];
            for (int i = 0; i < s1.length; i++) {
                s1[i] = this.players[i];
            }

            return s1;
        } else {
            return null;
        }
    }

    public void setPlayers(String[] players) {
        if (players != null) {
            String[] s1 = new String[players.length];
            for (int i = 0; i < s1.length; i++) {
                s1[i] = players[i];
            }

            this.players = s1;

            // this.players = new String[players.length];
            // System.arraycopy(players, 0, this.players, 0, players.length);
        }
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public boolean checkPlayer(String s) {
        return Arrays.asList(players).contains(s);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        GameSession copy = new GameSession(this.gameName, this.startTime, this.endTime - this.startTime);
        copy.producer = this.producer;
        copy.players = getPlayers();
        return copy;
    }

    public void concatenate(GameSession gs) {
        if (gs != null && gs.gameName != null && gameName != null) {
            if (this.gameName.equals(gs.gameName)) {
                this.startTime = Math.min(this.startTime, gs.startTime);
                this.endTime = Math.max(this.endTime, gs.endTime);
                this.producer = gs.producer;

                if (this.players == null) {
                    this.players = gs.getPlayers();
                } else if (gs.players != null) {
                    List<String> p = new ArrayList<>(Arrays.asList(players));   // Arrays.asList returneaza lista read only - nu poti modif

                    for (int i = 0; i < gs.players.length; i++) {
                        if (!p.contains(gs.players[i])) {
                            p.add(gs.players[i]);
                        }
                    }

                    String[] newPlayers = new String[p.size()];
                    this.players = p.toArray(newPlayers);
                }

                /*String[] commonPlayers = new String[players.length + gs.players.length];
                int noOfPlayers = 0;
                for (String p : players) {
                    if (!gs.checkPlayer(p)) {
                        commonPlayers[noOfPlayers] = p;
                        noOfPlayers++;
                    }
                }

                for (String p : gs.players) {
                    commonPlayers[noOfPlayers++] = p;
                }

                String[] result = new String[noOfPlayers];
                for (int i = 0; i < noOfPlayers; i++) {
                    result[i] = commonPlayers[i];
                }

                this.players = result;*/

            }
        }

    }
}
