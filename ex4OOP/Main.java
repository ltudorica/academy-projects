package teme.ex4OOP;

import teme.ex4OOP.Players.FreePlayer;
import teme.ex4OOP.Players.PlayerAccount;
import teme.ex4OOP.Players.PremiumPlayer;

public class Main {
    public static void main(String[] args) {
        PlayerAccount p1 = new PlayerAccount("Ghita", "abc", 20);
        System.out.println("Description: " + p1.getDescription());
        System.out.println("Pass: " + p1.getPassword());
        p1.play(10);
        System.out.println();

        PremiumPlayer premium1 = new PremiumPlayer("PremiumP", "1234", 50.5f);
        premium1.payMembership();
        premium1.printDescription();
        System.out.println();

        FreePlayer freePlayer = new FreePlayer("FreeP", "csdhjbc", 23, 50);
        freePlayer.printDescription();
        System.out.println(freePlayer.playedHours);
        System.out.println(freePlayer.getPassword());
    }
}
