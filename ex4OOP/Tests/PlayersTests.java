package teme.ex4OOP.Tests;

import org.junit.Test;
import teme.ex4OOP.Players.FreePlayer;
import teme.ex4OOP.Players.GenericAccount;
import teme.ex4OOP.Players.PlayerAccount;
import teme.ex4OOP.Players.PremiumPlayer;

import static org.assertj.core.api.Assertions.*;

public class PlayersTests {

    @Test
    public void getPassPlayerAcc() {
        PlayerAccount p = new PlayerAccount("Nick", "123", 21);
        assertThat(p.getPassword()).isEqualTo("123");
    }

    @Test
    public void getPassGenericAcc() {
        GenericAccount p = new PlayerAccount("Nick", "123", 21);
        assertThat(p.getPassword()).isEqualTo("123");
    }

    @Test
    public void getPassPremiumP() {
        PremiumPlayer p = new PremiumPlayer("Nick", "123", 5.3f);
        assertThat(p.getPassword()).isEqualTo("123");
    }

    @Test
    public void getPassFreeP() {
        FreePlayer p = new FreePlayer("Nick", "123", 20, 5);
        assertThat(p.getPassword()).isEqualTo("123");
    }

}
