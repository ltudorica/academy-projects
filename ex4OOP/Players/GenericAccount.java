package teme.ex4OOP.Players;

public abstract class GenericAccount {
    protected final String userName;

    public GenericAccount(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }

    public abstract String getPassword();

    public abstract String getDescription();
}
