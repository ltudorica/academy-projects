package teme.ex4OOP.Players;

import teme.ex4OOP.Printable;

public class FreePlayer extends PlayerAccount implements Printable {
    public int playedHours;

    public FreePlayer(String userName, String password, int age, int playedHours) {
        super(userName, password, age);
        this.playedHours = playedHours;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder(super.getDescription());
        sb.append("\nThe player have played ").append(this.playedHours).append(" hours.");
        return sb.toString();
    }

    public void printDescription() {
        System.out.println(this.getDescription());
    }
}
