package teme.ex4OOP.Players;

import teme.ex4OOP.Printable;


public class PremiumPlayer extends PlayerAccount implements Printable {
    public float membershipFee;

    public PremiumPlayer(String userName, String password, float membershipFee) {
        super(userName, password, 0);
        this.membershipFee = membershipFee;
    }

    public void payMembership() {
        System.out.println(String.format("The player payed the membership fee of %.2f", this.membershipFee));
    }

    @Override
    public String getDescription() {

        StringBuilder sb = new StringBuilder(super.getDescription());
        sb.append("\nMembership fee for premium player: ").append(String.format("%.2f", this.membershipFee));
        return sb.toString();
    }

    public void printDescription() {
        System.out.println(this.getDescription());
    }
}
