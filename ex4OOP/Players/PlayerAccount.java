package teme.ex4OOP.Players;

public class PlayerAccount extends GenericAccount {
    private String password;
    protected int age;

    public PlayerAccount(String userName, String password, int age) {
        super(userName);
        this.password = password;
        this.age = age;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder("Username: ");
        sb.append(this.userName).append("\nPassword: ");
        sb.append(this.password).append("\nAge: ").append(this.age);
        return sb.toString();
    }

    public void play(int hours) {
        System.out.println(String.format("The player %s spend %d hours playing", this.userName, hours));
    }
}
