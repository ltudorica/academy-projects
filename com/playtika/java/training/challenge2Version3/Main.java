package teme.com.playtika.java.training.challenge2Version3;

import teme.com.playtika.java.training.challenge2Version3.config.GameSettings;
import teme.com.playtika.java.training.challenge2Version3.models.Caller;
import teme.com.playtika.java.training.challenge2Version3.models.CardsGenerator;
import teme.com.playtika.java.training.challenge2Version3.exceptions.NoAvailablePlayersException;
import teme.com.playtika.java.training.challenge2Version3.exceptions.NoGameSettingsException;
import teme.com.playtika.java.training.challenge2Version3.models.Player;
import teme.com.playtika.java.training.challenge2Version3.models.PlayerCard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {
    public static Caller caller = new Caller();

    public static void main(String[] args) throws Exception {
        String settingsFileName = "";
        if (args.length != 0) {
            settingsFileName = args[0];
        } else {
            settingsFileName = "src/teme/com/settings.text";
        }

        GameSettings gs = null;

        try {
            gs = new GameSettings(settingsFileName);
        } catch (NoGameSettingsException e) {
            System.out.println("No settings file");
        } catch (NoAvailablePlayersException e) {
            System.out.println("No Players");
        } catch (IOException e) {
            System.out.println("File issues");
        }

        CardsGenerator cg = new CardsGenerator(gs);
        List<PlayerCard> allPlayingCards = cg.getInitialCards();
        List<Player> playersInGame = setPlayerWithCards(allPlayingCards, cg, gs);
        System.out.println();

        ExecutorService executor = Executors.newFixedThreadPool(playersInGame.size());
        List<Callable<Boolean>> startGame = new ArrayList<>();
        for (int i = 0; i < playersInGame.size(); i++) {
            startGame.add(playersInGame.get(0));
        }
        List<Future<Boolean>> response = executor.invokeAll(startGame);

        new Thread(() -> {
            caller.callNo();
        }).start();

        for (Future<Boolean> res : response) {
            Boolean aux = res.get();
            System.out.println(aux);
        }

        executor.shutdown();
        if (executor.awaitTermination(3, TimeUnit.SECONDS)) {
            executor.shutdownNow();
        }

    }

    public static List<Player> setPlayerWithCards(List<PlayerCard> allPlayingCards, CardsGenerator cg, GameSettings gs) {
        List<Player> playersInGame = new ArrayList<>();

        int countCardsInGame = 0;
        for (int i = 0; i < gs.getPlayerNames().size(); i++) {
            playersInGame.add(new Player(gs.getPlayerName(i), i + 1, caller));
            List<PlayerCard> cards = new ArrayList<>();
            int noCards = playersInGame.get(i).getNoCards();
            for (int j = countCardsInGame; j < noCards + countCardsInGame; j++) {
                cards.add(allPlayingCards.get(j));
            }
            playersInGame.get(i).setPlayerCards(cards);
            System.out.println(playersInGame.get(i));
            countCardsInGame += noCards;
        }

        return playersInGame;
    }
}
