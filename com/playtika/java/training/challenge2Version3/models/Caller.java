package teme.com.playtika.java.training.challenge2Version3.models;

import java.beans.BeanInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Caller {
    private static final String[] possibleBingo = {"B", "I", "N", "G", "O"};
    private volatile String lastNumberColumn = " ";
    private volatile int lastNumberNumber = 0;
    private AtomicInteger noRounds = new AtomicInteger();
    private List<BingoNumber> previousCombinations = new ArrayList<>();
    private volatile boolean isGameFinished = false;
    private static final Random RANDOM = new Random();


    public Caller() {
        this.noRounds.set(0);
    }

    public boolean wasPicked(BingoNumber bingoNumber) {
        return previousCombinations.contains(bingoNumber);
    }

    public String getLastNumberColumn() {
        return this.lastNumberColumn;
    }

    public int getLastNumberNumber() {
        return this.lastNumberNumber;
    }

    public int getNoOfPlayedNumbers() {
        return this.noRounds.get();
    }

    public List<BingoNumber> getPreviousCombinations() {
        return (List<BingoNumber>) ((ArrayList<BingoNumber>) previousCombinations).clone();
    }

    public BingoNumber getBingoNumber() {
        return new BingoNumber(lastNumberColumn, lastNumberNumber);
    }

    public boolean checkGameStatus() {
        return this.isGameFinished;
    }

    public void setGameStatus(boolean value) {
        this.isGameFinished = value;
    }

    public synchronized void callNo() {
        while (!isGameFinished) {
            lastNumberColumn = possibleBingo[RANDOM.nextInt(5)];
            lastNumberNumber = RANDOM.nextInt(75) + 1;
            BingoNumber bingoNumber = new BingoNumber(lastNumberColumn, lastNumberNumber);
            while (wasPicked(bingoNumber) || !bingoNumber.isValidCombination(lastNumberColumn, lastNumberNumber)) {
                lastNumberColumn = possibleBingo[RANDOM.nextInt(5)];
                lastNumberNumber = RANDOM.nextInt(75) + 1;
                bingoNumber = new BingoNumber(lastNumberColumn, lastNumberNumber);
            }
            previousCombinations.add(bingoNumber);
            this.noRounds.incrementAndGet();
            if (noRounds.get() == 75) {
                isGameFinished = true;
            }
            bingoNumber.print();
            try {
                this.notifyAll();
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
