package teme.com.playtika.java.training.challenge2Version3.models;

import teme.com.playtika.java.training.challenge2Version3.interfaces.Printable;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class BingoNumber implements Cloneable, Printable {
    private String column;
    private int number;
    protected static HashMap<String, List<Integer>> validComb;

    public BingoNumber(String column, int number) {
        this.column = column;
        this.number = number;
        BingoNumber.validComb = getCombinations();
    }

    public String getColumn() {
        return column;
    }

    public int getNumber() {
        return number;
    }

    public boolean isValidCombination(String column, int number) {
        return BingoNumber.validComb.get(column).contains(number);
    }

    public static HashMap<String, List<Integer>> getCombinations() {
        List<Integer> B = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        List<Integer> I = Arrays.asList(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29);
        List<Integer> N = Arrays.asList(30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45);
        List<Integer> G = Arrays.asList(46, 47, 48, 49, 50, 51, 52, 53, 56, 57, 58, 59, 60);
        List<Integer> O = Arrays.asList(61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75);
        HashMap<String, List<Integer>> comb = new HashMap<>();
        comb.put("B", B);
        comb.put("I", I);
        comb.put("N", N);
        comb.put("G", G);
        comb.put("O", O);

        return comb;
    }

    @Override
    public BingoNumber clone() throws CloneNotSupportedException {
        return new BingoNumber(this.column, this.number);
    }

    @Override
    public void print() {
        System.out.println(this.column + " - " + this.number + " ");
    }

    @Override
    public String toString() {
        return " " + this.number;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = prime * this.column.hashCode();
        result = result + this.number;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        BingoNumber bn = (BingoNumber) obj;
        return this.column.equals(bn.column) && this.number == bn.number;
    }
}
