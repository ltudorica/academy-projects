package teme.com.playtika.java.training.challenge2Version3.models;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Player implements Callable<Boolean> {
    private int noCards;
    private String name;
    private List<PlayerCard> ownedCards = new ArrayList<>();
    private volatile boolean isBingoTime;
    private String winningSchema;
    private List<BingoNumber> calledNo = new ArrayList<>();
    private Caller caller;

    public Player(String name, int noCards, Caller caller) {
        this.name = name;
        this.noCards = noCards;
        this.caller = caller;
    }

    public void setPlayerCards(List<PlayerCard> ownedCards) {
        this.ownedCards = (List<PlayerCard>) ((ArrayList<PlayerCard>) ownedCards).clone();
    }

    public int getNoCards() {
        return noCards;
    }

    public synchronized void checkCardsToWin(BingoNumber bn) {
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (PlayerCard pc : ownedCards) {
            BingoNumber[][] numbers = pc.getNumbers();
            for (int i = 0; i < numbers.length; i++) {
                for (int j = 0; j < numbers[i].length; j++) {
                    if (bn.equals(numbers[i][j])) {
                        isBingoTime = true;
                        break;
                    }
                }
                if (isBingoTime) {
                    break;
                }
            }
            if (isBingoTime) {
                break;
            }
        }
        if (!isBingoTime) {
            this.calledNo.add(bn);
        }
        this.notifyAll();
    }

    public boolean hasSaidBingo() {
        return isBingoTime;
    }

    @Override
    public Boolean call() throws Exception {
        System.out.println("Calling call method for player " + this.name);
        while (!caller.getBingoNumber().equals(new BingoNumber(" ", 0))) {
            checkCardsToWin(caller.getBingoNumber());
            caller.setGameStatus(isBingoTime);

        }
        return isBingoTime;
    }

    @Override
    public String toString() {
        return this.name + " " + this.noCards + "cards:\n" + this.ownedCards;
    }

    public void addBingoNumberToList(BingoNumber bn) {
        this.calledNo.add(bn);
    }
}
