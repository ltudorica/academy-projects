package teme.com.playtika.java.training.challenge2Version3.models;

import teme.com.playtika.java.training.challenge2Version3.config.GameSettings;
import teme.com.playtika.java.training.challenge2Version3.models.PlayerCard;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CardsGenerator {
    private int noThreads;
    private int noCards = GameSettings.NO_CARDS;
    private boolean hasFinishedGeneratingData;
    private GameSettings gameSettings;

    public CardsGenerator(GameSettings gameSettings) throws CloneNotSupportedException {
        this.gameSettings = gameSettings.clone();
        this.noThreads = gameSettings.getNoPlayers();
    }

    public List<PlayerCard> getInitialCards() throws InterruptedException, ExecutionException {
        ArrayList<Callable<List<PlayerCard>>> threads = new ArrayList<>();
        threadsDirect(threads);
        ExecutorService service = Executors.newFixedThreadPool(this.noThreads);
        List<Future<List<PlayerCard>>> results = service.invokeAll(threads);
        List<PlayerCard> result = extractPlayerCards(results);
//        hasFinishedGeneratingData = service.awaitTermination(3, TimeUnit.MINUTES);
        shutDownService(service);
        hasFinishedGeneratingData = true;
        return result;
    }

    private void threadsDirect(ArrayList<Callable<List<PlayerCard>>> threads) {
        for (int i = 0; i < this.noThreads; i++) {
            int finalI = i;
            threads.add(() -> {
                int lowLimit = (noCards / noThreads) * finalI;
                int upLimit = finalI == this.noThreads - 1 ? this.noCards : (noCards / noThreads) * (finalI + 1);
                List<PlayerCard> list = new ArrayList<>();
                for (; lowLimit < upLimit; lowLimit++) {
                    PlayerCard pc = new PlayerCard();
                    list.add(pc);
                }
                return list;
            });
        }
    }

    public boolean hasFinished() {
        return this.hasFinishedGeneratingData;
    }

    private List<PlayerCard> extractPlayerCards(List<Future<List<PlayerCard>>> futures) throws ExecutionException, InterruptedException {
        List<PlayerCard> result = new ArrayList<>();
        for (Future<List<PlayerCard>> res : futures) {
            List<PlayerCard> aux = res.get();
            for (PlayerCard p : aux) {
                result.add(p);
            }
        }
        return result;
    }

    private void shutDownService(ExecutorService service) throws InterruptedException {
        service.shutdown();
        if (service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }
    }
}
