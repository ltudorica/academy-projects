package teme.com.playtika.java.training.challenge2Version3.models;

import teme.com.playtika.java.training.challenge2Version3.interfaces.Printable;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PlayerCard implements Printable {
    private static final AtomicInteger toUse = new AtomicInteger(0);
    private final int UID;
    private BingoNumber[][] numbers = new BingoNumber[5][5];
    private static Random RANDOM = new Random();
    private static final List<String> COLUMNS = Arrays.asList("B", "I", "N", "G", "O");
    private Set<Integer> pulledNo = new HashSet<>();
    private static final BingoNumber FREE = new BingoNumber("N", -1);

    public PlayerCard() {
        this.UID = toUse.incrementAndGet();
        this.generate();
    }

    // TODO check if the numbers are in the forbidden combo + equals / hashcode
    public void generate() {
        int increase = 1;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (i == 2 && j == 2) {
                    this.numbers[j][i] = FREE;
                    continue;
                }
                String column = COLUMNS.get(i);
                int number = pickNumber(increase);
                BingoNumber toAdd = new BingoNumber(column, number);
                while (pulledNo.contains(number) || !toAdd.isValidCombination(column, number)) {
                    number = pickNumber(increase);
                }
                toAdd = new BingoNumber(column, number);
                pulledNo.add(number);
                this.numbers[j][i] = toAdd;
            }
            increase++;
        }
    }

    private int pickNumber(int increase) {
        return RANDOM.nextInt(14 * increase) + increase;
    }

    public BingoNumber[][] getNumbers() {
        return this.numbers;
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Card UID: ");
        sb.append(this.UID).append("\n");
        for (int i = 0; i < this.numbers.length; i++) {
            sb.append(COLUMNS.get(i)).append(":");
            for (int j = 0; j < this.numbers[i].length; j++) {
                sb.append(this.numbers[j][i]).append(" | ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
