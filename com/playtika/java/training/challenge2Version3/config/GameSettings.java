package teme.com.playtika.java.training.challenge2Version3.config;

import teme.com.playtika.java.training.challenge2Version3.exceptions.NoAvailablePlayersException;
import teme.com.playtika.java.training.challenge2Version3.exceptions.NoGameSettingsException;
import teme.com.playtika.java.training.challenge2Version3.models.BingoNumber;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GameSettings implements Cloneable {
    public static int NO_CARDS;
    private int noPlayers;
    private List<String> playerNames = new ArrayList<>();
    private List<BingoNumber> forbiddenCombination = new ArrayList<>();

    private GameSettings() {
    }

    public GameSettings(String settingsFileName) throws IOException {
        List<String> result = getSettingsFromFile(settingsFileName);
        if (result.isEmpty()) {
            throw new NoGameSettingsException();
        }
        if (result.size() == 1 || Integer.parseInt(result.get(0)) == 0) {
            throw new NoAvailablePlayersException();
        }
        GameSettings.NO_CARDS = Integer.parseInt(result.get(0));
        this.noPlayers = Integer.parseInt(result.get(1));
        for (int i = 2; i < (noPlayers + 2); i++) {
            this.playerNames.add(result.get(i));
        }

        for (int i = noPlayers + 2; i < result.size(); i++) {
            String[] bingoLine = result.get(i).split(",");
            String column = bingoLine[0];
            setForbiddenCombination(bingoLine, column);
        }
    }

    private void setForbiddenCombination(String[] bingoLine, String column) {
        for (int j = 1; j < bingoLine.length; j++) {
            int no = Integer.parseInt(bingoLine[j]);
            BingoNumber bingoNumber = new BingoNumber(column, no);
            if (bingoNumber.isValidCombination(column, no)) {
                this.forbiddenCombination.add(bingoNumber);
            }
        }
    }

    private List<String> getSettingsFromFile(String settingsFileName) throws IOException {
        File file = new File(settingsFileName);
        FileReader fileReader = new FileReader(file);
        List<String> result = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    public int getNoPlayers() {
        return this.noPlayers;
    }

    public String getPlayerName(int index) {
        return this.playerNames.get(index);
    }

    public List<String> getPlayerNames() {
        return (List<String>) ((ArrayList<String>) playerNames).clone();
    }

    private List<BingoNumber> getForbiddenCombination() {
        return (List<BingoNumber>) ((ArrayList<BingoNumber>) forbiddenCombination).clone();
    }

    @Override
    public GameSettings clone() throws CloneNotSupportedException {
        GameSettings gs = new GameSettings();
        gs.noPlayers = this.noPlayers;
        gs.playerNames = this.playerNames;
        gs.forbiddenCombination = getForbiddenCombination();
        return gs;
    }
}
