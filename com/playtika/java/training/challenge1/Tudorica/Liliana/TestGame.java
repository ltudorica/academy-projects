package teme.com.playtika.java.training.challenge1.Tudorica.Liliana;

import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Exceptions.PlayerProfileDataException;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.*;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Enums.CharacterType;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces.Usable;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.PlayerProfile;

import java.io.IOException;
import java.util.ArrayList;

public class TestGame {
    public static void main(String[] args) throws PlayerProfileDataException, CloneNotSupportedException, ClassNotFoundException {

        PlayerProfile p1 = new PlayerProfile("Sara", "sara@vhdjsfbvc.com");
        PlayerProfile p2 = new PlayerProfile("John", "john@aol.com");
        p1.addNewPlaySession();
        p1.updateLastPlayedTime(15);
        p1.addNewPlaySession();
        p1.updateLastPlayedTime(17);
        p1.addNewPlaySession();
        p1.updateLastPlayedTime(30);

        p2.addNewPlaySession();
        p2.updateLastPlayedTime(50);
        p2.addNewPlaySession();
        p2.updateLastPlayedTime(1);
        p2.addNewPlaySession();
        p2.updateLastPlayedTime(3);
        p2.addNewPlaySession();
        p2.updateLastPlayedTime(10);


        System.out.println(p1);
        System.out.println(p2);
        System.out.println("Players max played time: " + PlayerProfile.getAllPlayesMAX());

        PlayerProfile p3 = p1.clone();
        p3.addNewPlaySession();
        p3.updateLastPlayedTime(20);

        System.out.println(p1.getTotalPlayedTime());
        System.out.println(p3.getTotalPlayedTime());
        System.out.println("Players have same ref.? " + (p1 == p3));


        try {
            p1.setEmail("fdjbnfd@cdjbcnds.co");
        } catch (PlayerProfileDataException e) {
            e.printStackTrace();
        }

        System.out.println("P1 email: " + p1.getEmail());
        System.out.println(p1.getPlayerAgeInDays());
        System.out.println(p3.getUserName());

        System.out.println(p1.getTotalPlayedTime());
        System.out.println(p2.getMaxPlayedMinutes());

        System.out.println("Test for characters");
        System.out.println();

        Knight k1 = new Knight(3, "Thor", CharacterType.PALADIN, 0);
        BlackHat bh1 = new BlackHat(2, "Rambo", CharacterType.GNOME, 20);

        ArrayList<SuperHero> test = new ArrayList<>();
        test.add(k1);
        test.add(bh1);

        for (SuperHero hero : test) {
            hero.takesAHit();
            System.out.println(hero);
            System.out.println();
        }

        bh1.useMagic();
        System.out.println();

        for (SuperHero hero : test) {
            hero.doAction(new Usable() {
                @Override
                public String action() {
                    return "playing";
                }
            });
        }

        k1.doAction(() -> "playing");

        System.out.println("last testing");


        PlayerProfile player1 = new PlayerProfile("Bruno", "bruno@test.com");
        PlayerProfile player2 = new PlayerProfile("Micki", "micki@test.com");

        p1.addCharacters(k1, bh1);
        System.out.println(p1.getCharactersList());
        System.out.println(p2.getCharactersList());

        p2.setCharactersList(p1.getCharactersList());

        System.out.println(p1.getCharactersList() == p2.getCharactersList());

//        try{
//            p1.saveProfile("challenge1.bin");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        System.out.println();
        System.out.println("Load from file the profile");

        try {
            p1.loadProfile("challenge1.bin");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
