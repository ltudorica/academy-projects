package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.tests;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Exceptions.PlayerProfileDataException;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.PlayerProfile;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.tests.Categories.Important;

import java.lang.reflect.Constructor;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertNotSame;

public class PlayerProfileTests {
    PlayerProfile p;
    PlayerProfile p2;
    PlayerProfile p3 = new PlayerProfile("Ana", "ana@wefjh.com"); //as putea baga intr o metoda cu adnotarea before
// ar trebui pusa constanta cu email - gen final

    public PlayerProfileTests() throws PlayerProfileDataException {  //ca sa nu mai treb aruncata exceptia aici
    }

    @Category(Important.class)
    @Test
    public void setEmailWrongEmailTest() {
        try {
            p = new PlayerProfile("Ana", "ana.com");
            fail("no errror");
        } catch (PlayerProfileDataException e) {
            assertThat(true);
        }
    }

    @Test
    public void setEmailTest() {
        assertThat(p3.getEmail()).isEqualTo("ana@wefjh.com");
    }

    @Category(Important.class)
    @Test
    public void getTotalPlayedTimeTest() throws PlayerProfileDataException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p.addNewPlaySession();
        p.updateLastPlayedTime(20);
        p.addNewPlaySession();
        p.updateLastPlayedTime(23);
        p.addNewPlaySession();
        p.updateLastPlayedTime(21);
// ar trebui un for aici -> valorile adaugate din lista
        assertThat(p.getTotalPlayedTime()).isEqualTo(64);

    }

    @Category(Important.class)
    @Test
    public void getTotalPlayedTimeNullTest() {
        try {
            p3.getTotalPlayedTime();
            fail("failed");
        } catch (PlayerProfileDataException e) {
            assertThat(true);
        }
    }

    @Test
    public void getPlayerAgeInDaysTest() throws PlayerProfileDataException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");

        assertThat(p.getPlayerAgeInDays()).isEqualTo(3);
    }

    @Test
    public void getMaxPlayedMinutesTest() throws PlayerProfileDataException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p.addNewPlaySession();
        p.updateLastPlayedTime(20);
        p.addNewPlaySession();
        p.updateLastPlayedTime(23);
        p.addNewPlaySession();
        p.updateLastPlayedTime(21);

        assertThat(p.getMaxPlayedMinutes()).isEqualTo(23);
    }

    @Test
    public void addNewPlaySession() throws PlayerProfileDataException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p.addNewPlaySession();

        assertThat(p.getMinutesPlayedPerSession().length).isEqualTo(1);
    }

    @Test
    public void updateLastPlayedTime() throws PlayerProfileDataException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p.addNewPlaySession();
        int initialMinutes = p.getMinutesPlayedPerSession()[0];
        p.updateLastPlayedTime(15);

        assertThat(initialMinutes).isNotEqualTo(15);
    }

    @Test
    public void updateLastPlayedTime2() throws PlayerProfileDataException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p.addNewPlaySession();
        int initialMinutes = p.getMinutesPlayedPerSession()[0];
        p.updateLastPlayedTime(15);

        assertThat(p.getMinutesPlayedPerSession()[0]).isEqualTo(15);
    }

    @Category(Important.class)
    @Test
    public void cloneTest() throws PlayerProfileDataException, CloneNotSupportedException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p2 = p.clone();

        assertNotSame(p, p2);
    }

    @Category(Important.class)
    @Test
    public void cloneMinutesTest() throws PlayerProfileDataException, CloneNotSupportedException {
        p = new PlayerProfile("Ana", "ana@wefjh.com");
        p2 = p.clone();

        assertNotSame(p.getMinutesPlayedPerSession(), p2.getMinutesPlayedPerSession());
    }

    @Test
    public void noConstr() {
        Class toHack = PlayerProfile.class;
        Constructor[] allConstructors = toHack.getDeclaredConstructors();

        assertThat(allConstructors.length).isEqualTo(1);
    }

    @Test
    public void typeParamConstr() {
        Class toHack = PlayerProfile.class;
        Constructor[] allConstructors = toHack.getDeclaredConstructors();
        Class<?>[] types = allConstructors[0].getParameterTypes();
        int noArgs = types.length;
        boolean isString = true;

        for (Class type : types) {
            if (!type.equals(String.class)) {
                isString = false;
                break;
            }
        }

        assertThat(isString).isTrue();
    }


}
