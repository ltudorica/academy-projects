package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.tests.Suites;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.tests.Categories.Important;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.tests.PlayerProfileTests;

@RunWith(Categories.class)
@Suite.SuiteClasses(PlayerProfileTests.class)
@Categories.IncludeCategory(Important.class)
public class ImportantTests {
}
