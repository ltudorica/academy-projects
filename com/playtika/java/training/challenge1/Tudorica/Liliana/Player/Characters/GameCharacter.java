package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters;

import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces.Playable;

import java.io.Serializable;

public abstract class GameCharacter implements Playable, Serializable {

    public abstract int getLifePoints();

    public abstract String getName();

}
