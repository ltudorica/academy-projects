package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Enums;

public enum CharacterType {
    HEALER("heals"),
    PALADIN("hit"),
    SORCERER("spell cast"),
    GNOME("move");

    private String specialAbility;

    private CharacterType(String specialAbility) {
        this.specialAbility = specialAbility;
    }

    public String getSpecialAction() {
        return this.specialAbility;
    }
}


