package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters;

import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Enums.CharacterType;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces.Usable;

public class SuperHero extends GameCharacter {
    protected boolean isVillain;
    protected int lifePoints;
    protected String name;
    CharacterType type;

    public SuperHero(int lifePoints, String name, CharacterType type) {
        this.lifePoints = lifePoints;
        this.name = name;
        this.type = type;
    }

    public boolean isVillain() {
        return this.isVillain;
    }

    public void breakingBad() {
        this.isVillain = true;
    }

    public void turnGood() {
        this.isVillain = false;
    }

    @Override
    public int getLifePoints() {
        return this.lifePoints;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public CharacterType takesAHit() {
        this.lifePoints -= 10;
        return this.type;
    }

    @Override
    public CharacterType heals(int lifePoints) {
        this.lifePoints += lifePoints;
        return this.type;
    }

    @Override
    public void move() {
        System.out.println("The superhero moves");
    }

    public void doAction(Usable item) {
        //item.action();
        System.out.println(item.action());

    }

    @Override
    public String toString() {
        return "The Character's name: " + this.name + "\nLife points: " + this.lifePoints +
                "\nCharacter type: " + this.type + "\nThe character is " +
                (this.isVillain ? "" : "not ") + "bad";
    }

    @Override
    public SuperHero clone() throws CloneNotSupportedException {
        return new SuperHero(this.lifePoints, this.name, this.type);
    }
}
