package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces;

import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Enums.CharacterType;

public interface Playable {
    int MAX_POINTS = 20;

    CharacterType takesAHit();

    CharacterType heals(int lifePoints);

    void move();
}
