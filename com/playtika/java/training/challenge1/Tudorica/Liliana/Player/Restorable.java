package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Restorable {

    void saveProfile(String fileName) throws IOException;

    void loadProfile(String fileName) throws IOException, ClassNotFoundException;
}
