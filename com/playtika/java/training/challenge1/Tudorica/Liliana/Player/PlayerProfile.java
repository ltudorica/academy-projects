package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player;

import com.sun.jdi.ClassType;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Exceptions.PlayerProfileDataException;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.GameCharacter;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlayerProfile implements Cloneable, Restorable, Serializable {
    private final String userName;
    private String email;
    private LocalDateTime creationDate;
    private int noPlayedSessions;
    private int[] minutesPlayedPerSession;
    private static int MAX_PLAYED_MINUTES;
    private List<GameCharacter> charactersList = new ArrayList<>();

    public PlayerProfile(String userName, String email) throws PlayerProfileDataException {
        this.userName = userName;
        setEmail(email);
        this.creationDate = LocalDateTime.of(2021, 8, 1, 12, 00);
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public int getNoPlayedSessions() {
        return noPlayedSessions;
    }

    public int getTotalPlayedTime() throws PlayerProfileDataException {
        if (this.minutesPlayedPerSession == null) {
            throw new PlayerProfileDataException();
        }

        int count = 0;
        for (int i : this.minutesPlayedPerSession) {
            count += i;
        }

        return count;
    }

    public int getPlayerAgeInDays() {
        LocalDateTime now = LocalDateTime.now();
        return (int) Duration.between(this.creationDate, now).toDays();
    }

    public int getMaxPlayedMinutes() {
        int max = this.minutesPlayedPerSession[0];

        for (int i = 1; i < this.minutesPlayedPerSession.length; i++) {
            max = Math.max(max, this.minutesPlayedPerSession[i]);
        }

        return max;
    }

    public int[] getMinutesPlayedPerSession() {
        return minutesPlayedPerSession;
    }

    public List<GameCharacter> getCharactersList() {
        List<GameCharacter> newList = new ArrayList<>();

        for (GameCharacter g : this.charactersList) {
            newList.add(g);
        }

        return charactersList;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public void setCharactersList(List<GameCharacter> charactersList) {

        this.charactersList = new ArrayList<>();

        for (GameCharacter g : charactersList) {
            this.charactersList.add(g);
        }

    }

    public void setEmail(String email) throws PlayerProfileDataException {
        String matchingString = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern p = Pattern.compile(matchingString);
        Matcher m = p.matcher(email);

        if (m.matches()) {
            this.email = email;
        } else {
            throw new PlayerProfileDataException();
        }
    }

    public static int getAllPlayesMAX() {
        return PlayerProfile.MAX_PLAYED_MINUTES;
    }

    public void addNewPlaySession() {
        int[] newMinutesPlayedPerSession;
        if (this.minutesPlayedPerSession == null) {
            newMinutesPlayedPerSession = new int[1];
        } else {
            newMinutesPlayedPerSession = new int[noPlayedSessions + 1];


            for (int i = 0; i < noPlayedSessions; i++) {
                newMinutesPlayedPerSession[i] = this.minutesPlayedPerSession[i];
            }
        }
        noPlayedSessions++;
        this.minutesPlayedPerSession = newMinutesPlayedPerSession;
    }

    public void updateLastPlayedTime(int min) { // ar treb combinata cu addNewPlaySession
        this.minutesPlayedPerSession[noPlayedSessions - 1] = min;
        PlayerProfile.MAX_PLAYED_MINUTES = Math.max(MAX_PLAYED_MINUTES, min);
    }

    public void addCharacters(GameCharacter... ch) throws NullPointerException {
        if (ch == null) {
            throw new NullPointerException();
        }

        for (GameCharacter g : ch) {
            this.charactersList.add(g);
        }
    }

    public void removeCharacters(GameCharacter ch) throws NullPointerException {
        if (ch == null) {
            throw new NullPointerException();
        }

        this.charactersList.remove(ch);
    }

    public GameCharacter getCharacter(String name) throws NullPointerException {
        if (name == null) {
            throw new NullPointerException();
        }

        if (this.charactersList.isEmpty()) {
            System.out.println("There is no character in the list");
        }

        for (GameCharacter g : this.charactersList) {
            if (g.getName().equals(name)) {
                return g;
            }
        }

        System.out.println("No character like this!");
        return this.charactersList.get(0);

        // era mai ok daca aruncam exceptie aici?
    }

    public GameCharacter getCharacter(ClassType classType) {
        if (classType == null) {
            throw new NullPointerException();
        }

        if (this.charactersList.isEmpty()) {
            System.out.println("There is no character in the list");
        }

        for (GameCharacter g : this.charactersList) {
            if (g.getClass().equals(classType)) {
                return g;
            }
        }

        System.out.println("No character like this!");
        return this.charactersList.get(0);
        // era mai ok daca aruncam exceptie aici?
    }

    public int getNoCharacters() {
        return this.charactersList.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append("Username: ").append(this.userName);
        sb.append("\nEmail: ").append(this.email);
        sb.append("\nPlaying since: ").append(this.creationDate);
        sb.append("\nNo of played sessions: ").append(this.noPlayedSessions);
        sb.append("\nMinutes per session: ").append(Arrays.toString(this.minutesPlayedPerSession));

        return sb.toString();
    }

    @Override
    public PlayerProfile clone() throws CloneNotSupportedException {
        PlayerProfile newPlayer = null;

        try {
            newPlayer = new PlayerProfile(this.userName, this.email);
            newPlayer.creationDate = this.creationDate;
            newPlayer.noPlayedSessions = this.noPlayedSessions;
            int[] minPerSessions = new int[noPlayedSessions];

            for (int i = 0; i < noPlayedSessions; i++) {
                minPerSessions[i] = this.minutesPlayedPerSession[i];
            }
            newPlayer.minutesPlayedPerSession = minPerSessions;
        } catch (PlayerProfileDataException e) {
            e.printStackTrace();
        }

        return newPlayer;
    }

    @Override
    public void saveProfile(String fileName) throws IOException {
        File objectFile = new File(fileName);

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(objectFile))) {
            oos.writeObject(this);
        }

    }

    @Override
    public void loadProfile(String fileName) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            PlayerProfile p = (PlayerProfile) ois.readObject();
            System.out.println(p);
        }
    }

}
