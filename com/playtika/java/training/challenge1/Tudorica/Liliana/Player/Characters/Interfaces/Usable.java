package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces;

public interface Usable {
    String action();
}
