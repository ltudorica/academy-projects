package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters;

import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Enums.CharacterType;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces.Playable;

public class Knight extends SuperHero {
    protected String ability;
    protected int shieldStrength;

    public Knight(int lifePoints, String name, CharacterType type, int shieldStrength) {
        super(lifePoints, name, type);
        this.ability = type.getSpecialAction();
        this.shieldStrength = shieldStrength;
    }

    @Override
    public CharacterType takesAHit() {

        if (this.shieldStrength < 1) {
            this.lifePoints--;
            this.shieldStrength = Playable.MAX_POINTS;
        } else {
            this.shieldStrength--;
        }

        return this.type;
    }

    @Override
    public CharacterType heals(int lifePoints) {

        if (this.shieldStrength >= Playable.MAX_POINTS) {
            this.lifePoints++;
            this.shieldStrength = Playable.MAX_POINTS;
        } else {
            this.shieldStrength++;
        }

        return this.type;
    }

    @Override
    public void move() {
        System.out.println("The Knight is moving!");
    }

    @Override
    public String toString() {
        return super.toString() + "\nThe Knight has his shield strength at " +
                this.shieldStrength + " and the ability to " + this.ability;
    }

    @Override
    public Knight clone() throws CloneNotSupportedException {
        return new Knight(this.lifePoints, this.name, this.type, this.shieldStrength);
    }

}
