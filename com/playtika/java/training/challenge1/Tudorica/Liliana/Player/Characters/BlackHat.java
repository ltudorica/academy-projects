package teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters;

import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Enums.CharacterType;
import teme.com.playtika.java.training.challenge1.Tudorica.Liliana.Player.Characters.Interfaces.Playable;

public class BlackHat extends SuperHero {
    protected int spellPower;


    public BlackHat(int lifePoints, String name, CharacterType type, int spellPower) {
        super(lifePoints, name, type);
        this.spellPower = spellPower;
    }

    public void useMagic() {
        this.spellPower--;
        System.out.println("The BlackHat used a spell");
    }

    @Override
    public CharacterType takesAHit() {
        if (this.spellPower < 1) {
            this.lifePoints--;
            this.spellPower = Playable.MAX_POINTS;
        } else {
            this.spellPower--;
        }

        return this.type;
    }

    @Override
    public CharacterType heals(int lifePoints) {
        if (this.spellPower >= Playable.MAX_POINTS) {
            this.lifePoints++;
            this.spellPower = Playable.MAX_POINTS;
        } else {
            this.spellPower++;
        }

        return this.type;
    }

    @Override
    public void move() {
        System.out.println("The BlackHat is moving!");
    }

    @Override
    public String toString() {
        return super.toString() + "\nThe BlackHat has his spell power at " +
                this.spellPower + " and the ability to " + this.type.getSpecialAction();
    }

    @Override
    public BlackHat clone() throws CloneNotSupportedException {
        return new BlackHat(this.lifePoints, this.name, this.type, this.spellPower);
    }
}
