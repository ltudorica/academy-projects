package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory.EmailServer;

public class ServerMonitor implements Observer {
    public EmailServer emailServer;
    public static int countEmails = 1;

    public ServerMonitor(EmailServer server) {
        emailServer = server;
    }

    @Override
    public synchronized void update() {
        if (countEmails < 20) {
            countEmails++;
        } else {
            new Thread(emailServer::stop).start();
            new Thread(emailServer::start).start();
            System.out.println("*******Server restarted");
            countEmails = 0;
        }
    }

    @Override
    public String toString() {
        return "*****testing observer";
    }
}
