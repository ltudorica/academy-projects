package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.Repository;

import java.time.Duration;
import java.time.LocalDateTime;

public abstract class AbstractEmailServer implements EmailServer {
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    private Repository repo;

    public AbstractEmailServer() {
    }

    public void setRepo(Repository repo) {
        this.repo = repo;
    }

    @Override
    public synchronized void start() {
        this.startTime = LocalDateTime.now();
        System.out.print("Start @ " + this.startTime);
        System.out.println();
    }

    @Override
    public synchronized void stop() {
        this.stopTime = LocalDateTime.now();
        Duration duration = Duration.between(startTime, stopTime);
        System.out.print("Duration active: " + duration.getSeconds());
        System.out.println();
    }
}
