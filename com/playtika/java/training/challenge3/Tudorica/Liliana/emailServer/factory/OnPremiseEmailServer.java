package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory;

public class OnPremiseEmailServer extends AbstractEmailServer {

    protected OnPremiseEmailServer() {
    }

    @Override
    public synchronized void start() {
        System.out.print("OnPremisesEmailServer -> ");
        super.start();
    }

    @Override
    public synchronized void stop() {
        System.out.print("OnPremisesEmailServer -> ");
        super.stop();
    }
}
