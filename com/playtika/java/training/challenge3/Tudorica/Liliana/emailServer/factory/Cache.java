package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory;

import java.util.ArrayList;
import java.util.List;

public class Cache {
    private static List<EmailServer> existingServers = new ArrayList<>();

    private String getTypeServer(String type) {
        if(type.equals("P")) {
            return "OnPremiseEmailServer";
        } else if((type.equals("P"))) {
            return "CloudEmailServer";
        }
        return null;
    }

    public EmailServer getServer(String type) {
        if (existingServers != null && !existingServers.isEmpty()) {
            for (EmailServer emailServer : existingServers) {
                if (emailServer.getClass().toGenericString().endsWith(getTypeServer(type))) {
                    return emailServer;
                }
            }
        }
        return null;
    }

    public void addServer(EmailServer newServer) {
        if (existingServers != null) {
            existingServers.add(newServer);
        }
    }

}
