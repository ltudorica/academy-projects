package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory;

public interface EmailServer {
    void start();

    void stop();
}
