package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory;

public class EmailServerCreator {
    private static Cache cache = new Cache();

    public static EmailServer createNewServer(String type) {
        if (Type.P.toString().equals(type)) {
            OnPremiseEmailServer onPremiseEmailServer = new OnPremiseEmailServer();
            cache.addServer(onPremiseEmailServer);
            return onPremiseEmailServer;
        } else if (Type.C.toString().equals(type)) {
            CloudEmailServer cloudEmailServer = new CloudEmailServer();
            cache.addServer(cloudEmailServer);
            return cloudEmailServer;
        }
        return null;
    }

    public static EmailServer createOrGetExistent(String type) {
        EmailServer server = null;
        if (cache.getServer(type) != null) {
            server = cache.getServer(type);
        } else {
            server = createNewServer(type);
            cache.addServer(server);
        }
        return server;
    }

}
