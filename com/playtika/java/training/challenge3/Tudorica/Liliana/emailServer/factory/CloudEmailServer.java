package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory;

public class CloudEmailServer extends AbstractEmailServer {

    protected CloudEmailServer() {

    }

    @Override
    public synchronized void start() {
        System.out.println("AbstractEmailServer -> ");
        super.start();
    }

    @Override
    public synchronized void stop() {
        System.out.println("AbstractEmailServer -> ");
        super.stop();
    }
}
