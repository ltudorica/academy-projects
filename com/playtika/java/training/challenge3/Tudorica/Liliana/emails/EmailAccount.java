package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.Observer;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages.Message;

public class EmailAccount implements Account, Observable {
    private String emailAddress;
    private String name;
    private String surname;
    private String signature;
    private Observer observer;

    public EmailAccount(String emailAddress, String name, String surname, String signature) {
        this.emailAddress = emailAddress;
        this.name = name;
        this.surname = surname;
        this.signature = signature;
    }

    @Override
    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    public Observer getObserver() {
        return this.observer;
    }

    @Override
    public synchronized void receive(Message message) {
        EmailAccount sender = (EmailAccount) message.getSender();
        System.out.println(sender + ": " + message.getMessage() + " " + sender.signature);
        notifyObserver();
    }

    @Override
    public String toString() {
        return "Email: " + this.emailAddress + ", name: " + this.name + " " + this.surname;
    }

    @Override
    public void addEmail(Account account) {
        throw new UnsupportedOperationException("You cannot add an email to another one");
    }

    @Override
    public void removeEmail(Account account) {
        throw new UnsupportedOperationException("You cannot remove an email from another one");
    }

    @Override
    public Account getEmail(int index) {
        throw new UnsupportedOperationException("There is only one email to display" + this.emailAddress);
    }

    @Override
    public synchronized void subscribe(Observer observer) {
        this.observer = observer;
    }

    @Override
    public synchronized void unsubscribe(Observer observer) {
        this.observer = null;
    }

    @Override
    public synchronized void notifyObserver() {
        if (observer != null) {
            observer.update();
        }
    }
}
