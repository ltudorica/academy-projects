package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.Observer;

public interface Observable {
    void subscribe(Observer observer);

    void unsubscribe(Observer observer);

    void notifyObserver();
}
