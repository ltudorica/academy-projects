package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages.Message;

public interface Receivable {
    void receive(Message message);
}
