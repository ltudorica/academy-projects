package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.Observer;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages.Message;

import java.util.ArrayList;
import java.util.List;

public class EmailGroup implements Account {
    private String groupName;
    private List<Account> groupsAndAccountList = new ArrayList<>();
    private Observer observer;

    public EmailGroup(String groupName) {
        this.groupName = groupName;
    }


    @Override
    public void receive(Message message) {
        System.out.println(groupName.toUpperCase());
        for (Account e : groupsAndAccountList) {
            e.receive(message);
        }
    }

    @Override
    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void addEmail(Account account) {
        groupsAndAccountList.add(account);
    }

    @Override
    public void removeEmail(Account account) {
        groupsAndAccountList.remove(account);

    }

    @Override
    public Account getEmail(int index) {
        if (index >= 0 && index < groupsAndAccountList.size()) {
            return groupsAndAccountList.get(index);
        }
        throw new UnsupportedOperationException();
    }

}
