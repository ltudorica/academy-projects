package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.Observer;

public interface Account extends Receivable {
    void setObserver(Observer observer);

    void addEmail(Account account);

    void removeEmail(Account account);

    Account getEmail(int index);
}
