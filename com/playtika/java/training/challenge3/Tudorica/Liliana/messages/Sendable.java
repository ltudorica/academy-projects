package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.Account;

public interface Sendable {
    void send();

}
