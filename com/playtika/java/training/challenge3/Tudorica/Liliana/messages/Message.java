package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.Account;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class Message implements Sendable, Serializable {
    private Account sender;
    private Account receiver;
    private String message;
    private static AtomicInteger ID = new AtomicInteger(0);

    public Message(Account sender, Account receiver, String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        ID.incrementAndGet();
    }

    public Account getSender() {
        return sender;
    }

    public Account getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }

    public int getID() {
        return this.ID.get();
    }

    @Override
    public void send() {
        receiver.receive(this);
    }


}
