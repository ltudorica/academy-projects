package teme.com.playtika.java.training.challenge3.Tudorica.Liliana;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.Account;

import java.io.*;
import java.util.List;
import java.util.Vector;

public class MessagesFileRepo<Message> implements Repository<Message> {
    private List<Message> messages = new Vector<>();

    @Override
    public void addMessage(Message message) {
        File file = new File("messages.bin");
        readMessagesList(file);
        this.messages.add(message);
        writeMessagesList(file);
    }

    @Override
    public void updateMessage(int id) {

    }

    @Override
    public void deleteMessage(int id) {
        if (messages.get(id) != null) {
            messages.remove(id);
        }
    }

    @Override
    public List<Message> getAllSentMessages(Account sender) {
        List<Message> result = new Vector<>();
        for (Message m : messages) {

        }
        return null;
    }

    @Override
    public List<Message> getAllReceivedMessages(Account receiver) {
        return null;
    }

    @Override
    public List<Message> getAllMessagesContaining(String string) {
        return null;
    }


    public List<Message> getAllMessages() {
        return this.messages;
    }

    public void readMessagesList(File file) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            if (ois.readObject() != null) {
                messages = (List<Message>) ois.readObject();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void writeMessagesList(File file) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(messages);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
