package teme.com.playtika.java.training.challenge3.Tudorica.Liliana;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory.EmailServer;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.Observer;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.ServerMonitor;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.Account;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.EmailAccount;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.EmailGroup;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages.Message;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory.EmailServerCreator;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory.Type;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.messages.Sendable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File file = new File("config.txt");
        EmailServer test1 = EmailServerCreator.createNewServer(readFromFile(file));
        test1.start();
        test1.stop();

        EmailServer test2 = EmailServerCreator.createOrGetExistent(Type.P.toString());
        test2.start();
        test2.stop();

        ServerMonitor serverMonitor = new ServerMonitor(test1);

        Observer observer = new ServerMonitor(test1);
        Account sender = new EmailAccount("SenderEmail", "Ioana", "bla", "dfvdfvvd");
        Account email1 = new EmailAccount("test1", "ana", "mara", "zsdjcgbds");
        Account email2 = new EmailAccount("test2", "ion", "bla", "123");
        Account group1 = new EmailAccount("test3", "ada ", "pop", "123");

        Account group2 = new EmailGroup("subgroup2");
        group2.addEmail(new EmailAccount("group2", "1st", "email", "csdkjcnbjsdn"));
        group2.addEmail(new EmailAccount("group2", "2nd", "email", "vdfvfdv"));

        Account superGroup = new EmailGroup("Super Group");
        superGroup.addEmail(group1);
        superGroup.addEmail(group2);
        superGroup.addEmail(email1);
        superGroup.addEmail(email2);

        Sendable message = new Message(sender, superGroup, "500023");
        List<Sendable> messages = new ArrayList<>();
        message.send();

        email1.setObserver(observer);

        for (int i = 0; i < 20; i++) {
            messages.add(new Message(sender, email1, "fdkjnvkjdf"));
        }

        for (Sendable msg : messages) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg.send();
        }


    }

    public static String readFromFile(File file) {
        String result = null;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            if ((result = bufferedReader.readLine()) != null) {
                return result;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
