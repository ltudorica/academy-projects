package teme.com.playtika.java.training.challenge3.Tudorica.Liliana;

import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.Account;

import java.util.List;

public interface Repository<T> {
    void addMessage(T t);

    void updateMessage(int id);

    void deleteMessage(int id);

    List<T> getAllSentMessages(Account sender);

    List<T> getAllReceivedMessages(Account receiver);

    List<T> getAllMessagesContaining(String string);

}
