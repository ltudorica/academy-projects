package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.tests;

import org.junit.Test;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory.*;
import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertNotSame;


import java.util.ArrayList;
import java.util.List;

public class EmailFactoryTests {

    private List<EmailServer> cacheServers = new ArrayList<>();
    private CloudEmailServer cloudEmailServer;
    private OnPremiseEmailServer onPremiseEmailServer;
    private Cache cache;

    @Test
    public void createNewServerCloudType() {
        EmailServer result = EmailServerCreator.createNewServer(Type.C.toString());
        assertThat(result).isInstanceOf(CloudEmailServer.class);
    }

    @Test
    public void createNewServerOnPremisesType() {
        EmailServer result = EmailServerCreator.createNewServer(Type.P.toString());
        assertThat(result).isInstanceOf(OnPremiseEmailServer.class);
    }

    @Test
    public void createNewServerNull() {
        EmailServer result = EmailServerCreator.createNewServer("B");
        assertThat(result).isNull();
    }


    @Test
    public void createNewServerNewInstance() {
        EmailServer result1 = EmailServerCreator.createNewServer(Type.P.toString());
        EmailServer result2 = EmailServerCreator.createNewServer(Type.P.toString());
        assertNotSame(result1, result2);

    }



}
