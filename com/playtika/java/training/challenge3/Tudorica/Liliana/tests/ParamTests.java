package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.factory.*;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ParamTests {

    private String input;
    private Class output;

    public ParamTests(String input, Class server) {
        this.input = input;
        this.output = server;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getTestData() { //met asta intoarce mereu o col de obj
        return Arrays.asList(new Object[][]{
                {"P", OnPremiseEmailServer.class},
                {"C", CloudEmailServer.class}
        });
    }

    @Test
    public void testServerFactory() {
        EmailServer actualOutput = EmailServerCreator.createNewServer(input);
        assertEquals(actualOutput.getClass(), output);
    }

}
