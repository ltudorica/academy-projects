package teme.com.playtika.java.training.challenge3.Tudorica.Liliana.tests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.Observer;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emailServer.ServerMonitor;
import teme.com.playtika.java.training.challenge3.Tudorica.Liliana.emails.EmailAccount;

import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest(fullyQualifiedNames = "com/playtika/java/training/challenge3/Tudorica/Liliana.*")
public class ObserverTests {


    @Test
    public void observerTest() {
        Observer mockServerMonitor = mock(ServerMonitor.class);
        EmailAccount emailAccount = new EmailAccount("dhjc", "cdcc", "fdvf", "dfvfv");
//        Message msg = new Message(emailAccount, emailAccount, "dsvf");
//        emailAccount.receive(msg);
        emailAccount.notifyObserver();
        verify(mockServerMonitor, atLeastOnce()).update();
    }

}
