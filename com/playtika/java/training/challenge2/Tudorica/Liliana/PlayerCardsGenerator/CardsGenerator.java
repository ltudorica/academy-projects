package teme.com.playtika.java.training.challenge2.Tudorica.Liliana.PlayerCardsGenerator;

import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings.GameSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CardsGenerator {
    private int noThreads;
    private int noCards;
    private boolean hasFinishedGeneratingData;
    private GameSettings gameSettings;

    public CardsGenerator(int noThreads, GameSettings gameSettings) throws CloneNotSupportedException {
        this.noCards = GameSettings.noCards;
        this.noThreads = noThreads;
        this.gameSettings = gameSettings.clone();
    }


    public List<PlayerCard> getInitialCards() throws InterruptedException, ExecutionException {
        ArrayList<Callable<List<PlayerCard>>> threads = new ArrayList<>();

        for (int i = 0; i < this.noThreads; i++) { // pt fiecare
            int finalI = i;
            threads.add(() -> {
                int lowLimit = (noCards / noThreads) * finalI;
                int upLimit = finalI == this.noThreads - 1 ? this.noCards : (noCards / noThreads) * (finalI + 1);
                List<PlayerCard> list = new ArrayList<>();

//                System.out.println("Another Thread goes ... ");

                for (; lowLimit < upLimit; lowLimit++) {
                    PlayerCard pc = new PlayerCard();
                    list.add(pc);
                }

//            System.out.println("Another Thread goes ... ");
                return list;
            });

//            System.out.println(" Thread done ... ");
        }

        ExecutorService service = Executors.newFixedThreadPool(this.noThreads);
        List<Future<List<PlayerCard>>> results = service.invokeAll(threads);

        List<PlayerCard> result = new ArrayList<>();

        for (Future<List<PlayerCard>> res : results) {
            List<PlayerCard> aux = res.get();
            for (PlayerCard p : aux) {
                result.add(p);
            }
        }

        for (Future<List<PlayerCard>> res : results) {
            if (res.isDone()) {
                hasFinishedGeneratingData = true;
            } else {
                res.wait();
            }
        }

        service.shutdown();
        if (service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }

        return result;
    }

    public boolean hasFinished() {
        return this.hasFinishedGeneratingData;
    }


}
