package teme.com.playtika.java.training.challenge2.Tudorica.Liliana.PlayerCardsGenerator;

import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings.BingoNumber;
import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings.Printable;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PlayerCard implements Printable {
    private static final AtomicInteger toUse = new AtomicInteger(0);
    private final int UID;
    //    private List<List<BingoNumber>> numbers;
    BingoNumber[][] numbers = new BingoNumber[5][5];

    public PlayerCard() {
        this.UID = toUse.incrementAndGet();
        this.generate();
    }

    public void generate() {
        Random random = new Random();

        Set<Integer> pulledNo = new HashSet<>();
        List<String> columns = Arrays.asList("B", "I", "N", "G", "O");

        BingoNumber free = new BingoNumber("N", -1);
        int increase = 1;
        for (int i = 0; i < 5; i++) {
//            System.out.println("Starting second" + i);

            for (int j = 0; j < 5; j++) {
//                System.out.println("Starting second FOR j" + i);
                if (i == 2 && j == 2) {
                    this.numbers[j][i] = free;
                    continue;
                }

                String column = columns.get(i);
//                System.out.println("column" + column);
                int number = (random.nextInt(14 * increase)) + increase;
                BingoNumber toAdd = new BingoNumber(column, number);

                while (pulledNo.contains(number)) {
                    number = random.nextInt(14 * increase) + increase;
//                    System.out.println("*****no in 1st while" + number);
                }

                while (!toAdd.isValidCombination(column, number)) {
                    number = random.nextInt(14 * increase) + increase;
//                    System.out.println("*****no in 2nd while" + number);
                }

//                System.out.println("after while");
                toAdd = new BingoNumber(column, number);
                pulledNo.add(number);
                this.numbers[j][i] = toAdd;
//                System.out.println("Number" + column + " " + number + " finished adding");
            }
//            System.out.println("First itreration done..");
            increase++;
        }
//        System.out.println("A player card is generated ");
    }

    @Override
    public void print() {
        StringBuilder sb = new StringBuilder("Card UID: ");
        sb.append(this.UID).append("\n");
        String bingo = "BINGO";

        for (int i = 0; i < this.numbers.length; i++) {
            sb.append(bingo.charAt(i)).append(":");
            for (int j = 0; j < this.numbers[i].length; j++) {
                sb.append(this.numbers[j][i]).append(" | ");
            }
            sb.append("\n");
        }
//        for (BingoNumber[] vector : this.numbers) {
//                sb.append(Arrays.toString(vector));
//        }

        System.out.println(sb.toString());
    }
}
