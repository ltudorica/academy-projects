package teme.com.playtika.java.training.challenge2.Tudorica.Liliana;

import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings.GameSettings;
import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings.NoGameSettingsException;
import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.PlayerCardsGenerator.CardsGenerator;
import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.PlayerCardsGenerator.PlayerCard;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException, CloneNotSupportedException {
        GameSettings gs = null;

        try {
            gs = new GameSettings("src/teme/com/settings.text");
        } catch (NoGameSettingsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CardsGenerator cg = new CardsGenerator(12, gs);
        List<PlayerCard> list = cg.getInitialCards();
        System.out.println(list.size());


        for (PlayerCard p : list) {
            p.print();
        }

    }
}
