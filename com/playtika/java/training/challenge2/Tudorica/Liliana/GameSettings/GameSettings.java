package teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings;

import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.Main;
import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.Player.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GameSettings implements Cloneable {
    public static int noCards;
    private int noPlayers;
    private List<String> playerNames;
    private List<BingoNumber> forbiddenCombination;

    public GameSettings(String settingsFileName) throws NoGameSettingsException, IOException {
        File file = new File(settingsFileName);
        FileReader fileReader = new FileReader(file);
        List<String> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                result.add(line);
            }
        }

        if (result.isEmpty()) {
            throw new NoGameSettingsException();
        } else if (result.size() == 1) {
            throw new NoAvailablePlayersException();
        } else {
            GameSettings.noCards = Integer.parseInt(result.get(0));
            this.noPlayers = Integer.parseInt(result.get(1));
            this.playerNames = new ArrayList<>();
            for (int i = 2; i < (noPlayers + 2); i++) {
                this.playerNames.add(result.get(i));
            }
        }

    }

    private GameSettings(int noCards, int noPlayers, List<String> playerNames) throws CloneNotSupportedException {
        GameSettings.noCards = noCards;
        this.noPlayers = noPlayers;
        //playerNames = new ArrayList<>();
        this.playerNames = new ArrayList<>();
        for (String s : playerNames) {
            this.playerNames.add(s);
        }

        this.forbiddenCombination = new ArrayList<>();
//        for (BingoNumber b : forbiddenCombination) {
//            this.forbiddenCombination.add(b.clone());
//        }
    }

    public int getNoPlayers() {
        return this.noPlayers;
    }

    public String getPlayerName(int index) {
        return this.playerNames.get(index);
    }

    @Override
    public GameSettings clone() throws CloneNotSupportedException {
        GameSettings gs = new GameSettings(GameSettings.noCards, this.noPlayers, this.playerNames);
        return gs;
    }
}
