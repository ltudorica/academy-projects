package teme.com.playtika.java.training.challenge2.Tudorica.Liliana;

import teme.com.playtika.java.training.challenge2.Tudorica.Liliana.GameSettings.BingoNumber;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Caller {
    private static final char[] possibleBingo = "BINGO".toCharArray();
    private char lastNumberColumn;
    private int lastNumberNumber;
    private AtomicInteger noRounds;
    private List<BingoNumber> previousCombinations;
    private boolean isGameFinished;

    public Caller() {
        Random random = new Random();
        this.lastNumberColumn = possibleBingo[random.nextInt(5)];
        this.lastNumberNumber = random.nextInt(75);

    }
}
