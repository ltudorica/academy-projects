package teme.ex5Threads;

import java.util.*;
import java.util.concurrent.*;

public class YourSolution {

    final int noThreads = Runtime.getRuntime().availableProcessors();

    /**
     * Sequentially computes the average age of players who are registered for the game
     *
     * @param playersList all players data
     * @return the average age of players who are registered for the game
     */
    public double computeSequantially_PlayersAgeAverageWhichAreRegistered(
            final Player[] playersList) throws ExecutionException, InterruptedException {

        Callable<Double> t = () -> {
            int sumAge = 0;
            for (Player player : playersList) {
                if (player.isRegistered()) {
                    sumAge += player.getAge();
                }
            }

            return (double) (sumAge / playersList.length);
        };

        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<Double> result = service.submit(t);

        double finalRes = 0;

        finalRes = result.get();


        service.shutdown();

        if (!service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }


        return finalRes;
    }

    /**
     * TODO computes on multiple threads the average age of players who are registered for the game
     *
     * @param playersList all players data
     * @return the average age of players who are registered for the game
     */
    public double computeConcurrently_PlayersAgeAverageWhichAreRegistered(
            final Player[] playersList) throws InterruptedException, ExecutionException {


        ArrayList<Callable<Integer>> threads = new ArrayList<>();
        int jump = playersList.length / noThreads;

        for (int i = 0; i < noThreads; i++) {
            int finalI = i;
            threads.add(() -> {
                int sumAge = 0;
                int lowLimit = jump * finalI;
                int upLimit = (finalI == noThreads - 1) ? playersList.length : (jump * (finalI + 1));
                for (int j = lowLimit; j < upLimit; j++) {
                    if (playersList[j].isRegistered()) {
                        sumAge += playersList[j].getAge();
                    }
                }

                return sumAge;
            });
        }

        ExecutorService service = Executors.newFixedThreadPool(noThreads);
        List<Future<Integer>> results = service.invokeAll(threads);

        int sum = 0;

        for (Future<Integer> res : results) {
            sum += res.get();
        }

        service.shutdown();
        if (!service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }

        return sum / playersList.length;
    }


    /**
     * Sequential calculation of the most common first name for students which are not enrolled in the game
     *
     * @param playerArray all players data
     * @return he most common first name
     */
    public String computeSequantially_MostCommonFirstNameForNotRegisteredPlayers(
            final Player[] playerArray) throws ExecutionException, InterruptedException {
//
//
//        Callable<String> t = () -> {
//            HashMap<String, Integer> map = new HashMap<>();
//
//            for (String firstName : TestChallenge.firstNames) {
//                map.put(firstName, 0);
//            }
//
//            for (Player p : playerArray) {
//                if(!p.isRegistered()) {
//                    String name = p.getFirstName();
//                    map.put(name, map.get(name) + 1);
//                }
//            }
//
//            String res = "";
//            int max = 0;
//
//            for (String s : map.keySet()) {
//                if (map.get(s) > max) {
//                    max = map.get(s);
//                    res = s;
//                }
//            }
//
//
//            return res;
//        };
//
//        ExecutorService service = Executors.newSingleThreadExecutor();
//        Future<String> result = service.submit(t);
//
//        String finalRes = "";
//
//        finalRes = result.get();
//
//        service.shutdown();
//
//        if(!service.awaitTermination(3, TimeUnit.SECONDS)) {
//            service.shutdownNow();
//        }
//
//        return finalRes;

        Callable<Integer> t = () -> {
            int[] names = new int[TestChallenge.firstNames.length];
            ArrayList<String> namesForReal = new ArrayList<>(Arrays.asList(TestChallenge.firstNames));

            for (Player p : playerArray) {
                if (!p.isRegistered()) {
                    names[namesForReal.indexOf(p.getFirstName())]++;
                }
            }

            int maxElem = 0;
            int res = 0;

            for (int i = 0; i < names.length; i++) {
                if (names[i] > maxElem) {
                    maxElem = names[i];
                    res = i;
                }
            }

            return res;
        };

        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<Integer> result = service.submit(t);

        String finalRes = TestChallenge.firstNames[result.get()];

        service.shutdown();

        if (!service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }

        return finalRes;
    }

    /**
     * TODO concurrent solution to determine the most common first name for players which are not registered for the game
     *
     * @param playerArray all players data
     * @return the most common first name
     */
    public String computeConcurrently_MostCommonFirstNameForNotRegisteredPlayers(
            final Player[] playerArray) throws InterruptedException, ExecutionException {

//       ****1**** ArrayList<Callable<ConcurrentHashMap<String, Integer>>> threads = new ArrayList<>();
//        int jump = playerArray.length / noThreads;
//
//        for (int i = 0; i < noThreads; i++) {
//            int finalI = i;
//
//            threads.add( () -> {
//                ConcurrentHashMap<String, Integer> result = new ConcurrentHashMap<>();
//
//                for (String name : TestChallenge.firstNames) {
//                    result.put(name, 0);
//                }
//
//                int lowLimit = jump * finalI;
//                int upLimit = (finalI == noThreads - 1) ? playerArray.length : (jump * (finalI + 1));
//                for (int j = lowLimit; j < upLimit; j++) {
//                    if (result.containsKey(playerArray[j].getFirstName()) && !playerArray[j].isRegistered()) {
//                        result.put(playerArray[j].getFirstName(), result.get(playerArray[j].getFirstName()) + 1);
//                    }
//                }
//
//                return result;
//            });
//
//        }
//
//        ExecutorService service = Executors.newFixedThreadPool(noThreads);
//        List<Future<ConcurrentHashMap<String, Integer>>> results = service.invokeAll(threads);
//
//        int max = 0;
//        String res = "";
//
//        ConcurrentHashMap<String, Integer> result = new ConcurrentHashMap<>();
//
//        for (String name : TestChallenge.firstNames) {
//            result.put(name, 0);
//        }
//
//        for (Future<ConcurrentHashMap<String, Integer>> maps : results) {
//            for (String s : maps.get().keySet()) {
//                result.put(s, result.get(s) + maps.get().get(s));
//            }
//        }
//
//        for (String s : result.keySet()) {
//            if (result.get(s) > max) {
//                max = result.get(s);
//                res = s;
//            }
//        }
//
//        service.shutdown();
//        if (service.awaitTermination(3, TimeUnit.SECONDS)) {
//            service.shutdownNow();
//        }
// ****1****


// ***2****
        ArrayList<Callable<Integer>> threads = new ArrayList<>();


        int j = 0;

        for (int i = 0; i < noThreads; i++) {
            int finalI = i;
            int finalJ = j;
            threads.add(() -> {
                boolean isEven = finalI % 2 == 0;
                int lowLimit = isEven ? 0 : (playerArray.length / 2);
                int upLimit = isEven ? (playerArray.length / 2) : playerArray.length;
                Integer res = 0;

                for (; lowLimit < upLimit; lowLimit++) {
                    if (!playerArray[lowLimit].isRegistered() && playerArray[lowLimit].getFirstName() == (TestChallenge.firstNames[finalJ])) {
                        res++;
                    }
                }

                return res;
            });

            if (i % 2 == 1) {
                j++;
            }
        }

        ExecutorService service = Executors.newFixedThreadPool(noThreads);
        List<Future<Integer>> results = service.invokeAll(threads);

        int[] toCompare = new int[TestChallenge.firstNames.length];
        int jump = 0;

        for (int i = 0; i < results.size(); i++) {
            toCompare[jump] += results.get(i).get();
            if (i % 2 == 1) {
                jump++;
            }
        }

        int elem = 0;
        int max = 0;
        for (int i = 0; i < toCompare.length; i++) {
            if (toCompare[i] > max) {
                max = toCompare[i];
                elem = i;
            }
        }

        service.shutdown();
        if (service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }

        return TestChallenge.firstNames[elem];

    }

    /**
     * Sequentially determine the number of players who have less than 50 points and who are> 20 years old
     *
     * @param players all players data
     * @return Number of players
     */
    public int computeSequantially_NoOfPlayersWithGivenAgeAndPoints(
            final Player[] players) throws ExecutionException, InterruptedException {

        Callable<Integer> t = () -> {
            int count = 0;

            for (Player p : players) {
                if (p.getNota() < 50 && p.getAge() > 20) {
                    count++;
                }
            }

            return count;
        };

        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<Integer> result = service.submit(t);

        int res = result.get();

        service.shutdown();
        if (service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }


        return res;
    }

    /**
     * TODO Concurrently determine the number of players who have less than 50 points and who are> 20 years old
     * <p>
     * <p>
     * You must use at least 2 threads
     *
     * @param playerArray Players data.
     * @return Number of players.
     */
    public int computeConcurrently_NoOfPlayersWithGivenAgeAndPoints(
            final Player[] playerArray) throws InterruptedException, ExecutionException {

        ArrayList<Callable<Integer>> threads = new ArrayList<>();

        int jump = playerArray.length / noThreads;

        for (int i = 0; i < noThreads; i++) {
            int finalI = i;
            threads.add(() -> {
                int count = 0;
                int lowLimit = jump * finalI;
                int upLimit = (finalI == noThreads - 1) ? playerArray.length : (jump * (finalI + 1));
                for (int j = lowLimit; j < upLimit; j++) {
                    if (playerArray[j].getNota() < 50 && playerArray[j].getAge() > 20) {
                        count++;
                    }
                }

                return count;
            });
        }

        ExecutorService service = Executors.newFixedThreadPool(noThreads);
        List<Future<Integer>> result = service.invokeAll(threads);

        int res = 0;

        for (Future<Integer> total : result) {
            res += total.get();
        }

        service.shutdown();
        if (service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }


        return res;

    }
}
