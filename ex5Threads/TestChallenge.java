package teme.ex5Threads;

import java.util.Random;
import java.util.concurrent.ExecutionException;

public class TestChallenge {

    final static int NO_REPETITIONS = 10;
    final static String[] firstNames = {"Alina", "Ionut", "Ion", "Alex", "Bianca", "Mihaela"};
    final static String[] lastNames = {"Popescu", "Ionescu", "Alexandru", "Tudor", "Avram", "Mihalache"};


    static int getNoProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    static Player[] generateTestData() {
        final int NO_PLAYERS = (int) 3e7;
        final int NO_PLAYERS_IN_GAME = (int) 1e6;

        Player[] players = new Player[NO_PLAYERS];
        Random r = new Random(123);

        for (int player = 0; player < NO_PLAYERS; player++) {
            final String firstName = firstNames[r.nextInt(firstNames.length)];
            final String lastName = lastNames[r.nextInt(lastNames.length)];
            final double age = r.nextDouble() * 100.0;
            final int points = 1 + r.nextInt(100);
            final boolean isRegisteredForGame = (player < NO_PLAYERS_IN_GAME);

            players[player] = new Player(firstName, lastName, age, points, isRegisteredForGame);
        }

        return players;
    }

    private static double computeAgeAverageRegisteredPlayers(final int noRepetitions) throws ExecutionException, InterruptedException {

        System.out.println("*********Test: compute age average");

        final Player[] players = generateTestData();
        final YourSolution resurse = new YourSolution();

        final double ref = resurse.computeSequantially_PlayersAgeAverageWhichAreRegistered(players);

        final long startSecvential = System.currentTimeMillis();
        for (int r = 0; r < noRepetitions; r++) {
            resurse.computeSequantially_PlayersAgeAverageWhichAreRegistered(players);
        }
        final long finalSecvential = System.currentTimeMillis();

        System.out.println("Durata solutie secventiala: " + (finalSecvential - startSecvential));

        final double calc = resurse.computeConcurrently_PlayersAgeAverageWhichAreRegistered(players);
        final double err = Math.abs(calc - ref);
        final String msg = "Expected " + ref + " but was " + calc + ", err = " + err;

        if (err < 1E-5)
            System.out.println("Test passed");
        else
            System.out.println("Test FAILED - " + msg);

        final long startParalel = System.currentTimeMillis();
        for (int r = 0; r < noRepetitions; r++) {
            resurse.computeConcurrently_PlayersAgeAverageWhichAreRegistered(players);
        }
        final long finalParalel = System.currentTimeMillis();

        System.out.println("Durata solutie paralela: " + (finalParalel - startParalel));

        return (double) (finalSecvential - startSecvential) / (double) (finalParalel - startParalel);
    }

    /*
     * Verifica daca valorile obtinute prin cele 2 metode sunt identice
     */
    public static void testComputeAgeAverageRegisteredPlayers() throws ExecutionException, InterruptedException {
        computeAgeAverageRegisteredPlayers(1);
    }

    /*
     * Test performanta versiune paralela vs secventiala
     */
    public static void performanceTestAgeAverageRegisteredPlayers() throws ExecutionException, InterruptedException {
        final int nrProcesoare = getNoProcessors();
        final double speedup = computeAgeAverageRegisteredPlayers(NO_REPETITIONS);
        String msg = "Solutia paralela trebuie sa ruleze de cel putin 1.2x mai repede ";

        System.out.println("Test: " + msg);
        System.out.println("Nr procesoare: " + nrProcesoare);

        if (speedup > 1.2)
            System.out.println("Test PASSED. Factor crestere viteza " + speedup);
        else
            System.out.println("Test FAILED. Factor crestere viteza " + speedup);
    }

    private static double computesMostCommonFirstNameForNoRegisteredPlayers(final int repeats) throws ExecutionException, InterruptedException {

        final Player[] players = generateTestData();
        final YourSolution solution = new YourSolution();

        System.out.println("*********Test: determina cel mai comun prenume");

        final String ref = solution.computeSequantially_MostCommonFirstNameForNotRegisteredPlayers(players);

        final long startSecvential = System.currentTimeMillis();
        for (int r = 0; r < repeats; r++) {
            solution.computeSequantially_MostCommonFirstNameForNotRegisteredPlayers(players);
        }
        final long finalSecvential = System.currentTimeMillis();

        System.out.println("Durata solutie secventiala: " + (finalSecvential - startSecvential));

        final String calc = solution.computeConcurrently_MostCommonFirstNameForNotRegisteredPlayers(players);

        // **********************************************
        if (ref == calc)
            System.out.println("Test PASSED. Valorile calculate sunt identice ");
        else
            System.out.println("Test FAILED. Valorile calculate sunt DIFERITE");


        final long startParalel = System.currentTimeMillis();
        for (int r = 0; r < repeats; r++) {
            solution.computeConcurrently_MostCommonFirstNameForNotRegisteredPlayers(players);
        }
        final long finalParalel = System.currentTimeMillis();

        System.out.println("Durata solutie paralela: " + (finalParalel - startParalel));

        return (double) (finalSecvential - startSecvential) /
                (double) (finalParalel - startParalel);
    }

    /*
     * Test correctness of mostCommonFirstNameForNoRegisteredPlayers.
     */
    public static void testComputesMostCommonFirstNameForNotRegisteredPlayers() throws ExecutionException, InterruptedException {
        computesMostCommonFirstNameForNoRegisteredPlayers(1);
    }

    /*
     * Test performance of mostCommonFirstNameForNoRegisteredPlayers.
     */
    public static void testPerformanceMostCommonFirstNameForNotRegisteredPlayers() throws ExecutionException, InterruptedException {
        final int noProcessors = getNoProcessors();
        final double speedup = computesMostCommonFirstNameForNoRegisteredPlayers(NO_REPETITIONS);
        final double expectedSpeedup = (double) noProcessors * 0.5;
        String msg = "crestere viteza executie cu cel putin " + expectedSpeedup + "x ";

        System.out.println("Test: " + msg);
        System.out.println("Nr cores: " + noProcessors);

        if (speedup >= expectedSpeedup)
            System.out.println("Test PASSED. Factor crestere viteza " + speedup);
        else
            System.out.println("Test FAILED. Factor crestere viteza " + speedup);

    }

    private static double computeNoPlayers(final int repeats) throws ExecutionException, InterruptedException {
        final Player[] players = generateTestData();
        final YourSolution solutie = new YourSolution();

        System.out.println("*********Test: determina numarul player-ilor peste 20 de ani care nu peste");

        final int ref = solutie.computeSequantially_NoOfPlayersWithGivenAgeAndPoints(players);

        final long startSecvential = System.currentTimeMillis();
        for (int r = 0; r < repeats; r++) {
            solutie.computeSequantially_NoOfPlayersWithGivenAgeAndPoints(players);
        }
        final long finalSecvential = System.currentTimeMillis();

        System.out.println("Durata solutie paralela: " + (finalSecvential - startSecvential));

        final int calc = solutie.computeConcurrently_NoOfPlayersWithGivenAgeAndPoints(players);

        // **********************************************
        if (ref == calc)
            System.out.println("Test PASSED. Valorile calculate sunt identice ");
        else
            System.out.println("Test FAILED. Valorile calculate sunt DIFERITE");


        final long startParalel = System.currentTimeMillis();
        for (int r = 0; r < repeats; r++) {
            solutie.computeConcurrently_NoOfPlayersWithGivenAgeAndPoints(players);
        }
        final long finalParalel = System.currentTimeMillis();

        System.out.println("Durata solutie paralela: " + (finalParalel - startParalel));

        return (double) (finalSecvential - startSecvential) / (double) (finalParalel - startParalel);
    }

    /*
     * Test correctness ComputeNoPlayers.
     */
    public static void testComputeNoPlayers() throws ExecutionException, InterruptedException {
        computeNoPlayers(1);
    }

    /*
     * Test performance
     */
    public static void testPerformanceNoPlayers() throws ExecutionException, InterruptedException {
        final int noProcessors = getNoProcessors();
        final double speedup = computeNoPlayers(NO_REPETITIONS);
        String msg = "Solutia paralela trebuie sa ruleze de cel putin 1.2x mai repede ";

        System.out.println("Test: " + msg);
        System.out.println("Nr procesoare: " + noProcessors);

        if (speedup >= 1.2)
            System.out.println("Test PASSED. Factor crestere viteza " + speedup);
        else
            System.out.println("Test FAILED. Factor crestere viteza " + speedup);

    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("Start...");
        testComputeAgeAverageRegisteredPlayers();
        performanceTestAgeAverageRegisteredPlayers();

        testComputesMostCommonFirstNameForNotRegisteredPlayers();
        testPerformanceMostCommonFirstNameForNotRegisteredPlayers();

        testComputeNoPlayers();
        testPerformanceNoPlayers();
        System.out.println("The end");
    }

}
