package teme.ex5Threads;

public class Player {
    private final String firstName;
    private final String lastName;
    private final double age;
    private final int points;
    /**
     * Whether the student is currently enrolled, or has already completed the
     * course.
     */
    private final boolean isRegistered;

    public Player(final String firstName, final String lastName,
                  final double age, final int points,
                  final boolean isRegistered) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.points = points;
        this.isRegistered = isRegistered;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getAge() {
        return age;
    }

    public int getNota() {
        return points;
    }

    public boolean isRegistered() {
        return isRegistered;
    }
}
