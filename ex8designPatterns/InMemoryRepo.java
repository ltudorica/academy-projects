package teme.ex8designPatterns;

import java.util.List;
import java.util.Vector;

public class InMemoryRepo<User> implements Repository<User> {
    List<User> users = new Vector<>();

    @Override
    public void insert(User entry) {
        users.add(entry);
    }

    @Override
    public void delete(User entry) {
        users.remove(entry);
    }

    @Override
    public void update(User oldEntry, User newEntry) {
        users.set(users.indexOf(oldEntry), newEntry);
    }

    @Override
    public List<User> getAll() {
        List<User> result = new Vector<>();
        for (User t : users) {
            result.add(t);
        }
        return result;
    }

}
