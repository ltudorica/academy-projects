package teme.ex8designPatterns.userView;


import teme.ex8designPatterns.userController.Observer;

public interface Observable {
    void subscribe(Observer observer);

    void unsubscribe(Observer observer);

    void notify(String field, String value);
}
