package teme.ex8designPatterns.userView;

import teme.ex8designPatterns.Command;
import teme.ex8designPatterns.LoginCommand;
import teme.ex8designPatterns.Repository;
import teme.ex8designPatterns.userController.Observer;
import teme.ex8designPatterns.userController.UserController;
import teme.ex8designPatterns.userModel.User;

import java.util.Scanner;

public class UserView implements Observable {
    public static final Scanner SCANNER = new Scanner(System.in);
    private String email;
    private String password;
    private Observer observer;
    private Command command;


    public UserView(Observer observer, Command command) {
        this.observer = observer;
        this.command = command;
    }

    public void display() {
        do {
            System.out.println("Email: ");
            email = SCANNER.nextLine();
            System.out.println("Password: ");
            password = SCANNER.nextLine();
            System.out.println("Press enter to login ");
            SCANNER.nextLine();
            observer.update("email", email);
            observer.update("password", password);
        } while (!command.execute());

    }

    @Override
    public void subscribe(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void unsubscribe(Observer observer) {
        this.observer = null;
    }

    @Override
    public void notify(String field, String value) {
        if (observer != null) {
            observer.update(field, value);
        }

    }
}
