package teme.ex8designPatterns;


import java.util.List;

public interface Repository<T> {
    void insert(T entry);

    void delete(T entry);

    void update(T oldEntry, T newEntry);

    List<T> getAll();
}
