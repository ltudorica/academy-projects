package teme.ex8designPatterns;

import teme.ex8designPatterns.userController.UserController;

public class LoginCommand implements Command {
    private UserController userController;

    public LoginCommand(UserController userController) {
        this.userController = userController;
    }

    @Override
    public boolean execute() {
        return this.userController.login();
    }
}
