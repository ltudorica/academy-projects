package teme.ex8designPatterns;

import teme.ex8designPatterns.userController.UserController;
import teme.ex8designPatterns.userModel.User;
import teme.ex8designPatterns.userView.UserView;

public class Main {
    public static void main(String[] args) {

        Repository<User> repo = new InMemoryRepo<>();
        User u1 = new User("bla", "blabla");
        repo.insert(u1);
        UserController controller = new UserController(repo);
        Command loginCommand = new LoginCommand(controller);
        UserView userView = new UserView(controller, loginCommand);
        userView.display();


        System.out.println(repo.getAll());


    }

}
