package teme.ex8designPatterns.userController;

public interface Observer {
    void update(String field, String value);

}
