package teme.ex8designPatterns.userController;

import teme.ex8designPatterns.Repository;
import teme.ex8designPatterns.userModel.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserController implements Observer {
    private User user;
    private Repository<User> repo;
    private static final int MIN_PASSWORD_LENGTH = 5;

    public UserController(Repository<User> repo) {
        this.user = new User();
        this.repo = repo;
    }

    public User getUser() {
        return this.user;
    }

    public String getEmail() {
        return user.getEmail();
    }

    public Repository<User> getRepo() {
        return this.repo;
    }

    public boolean login() {
        threadSleep();

        if (!isEmailValid(user.getEmail())) {
            System.out.println("Email is not valid!");
            return false;
        }
        if (!isPasswordLengthMinLength(user.getEmail())) {
            System.out.println("The password should contain minimum " + MIN_PASSWORD_LENGTH + " characters! ");
            return false;
        }
        // verif daca userul e in repo
        user.resetUser();
        return true;
    }

    public boolean isEmailValid(String email) {
        Pattern p = Pattern.compile("^(.+)@(.+)$");
        Matcher m = p.matcher(email);
        return m.find();
    }

    public boolean isPasswordLengthMinLength(String pass) {
        return pass.length() >= MIN_PASSWORD_LENGTH;
    }

    private void threadSleep() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(String field, String value) {
        if (field.equals("password")) {
            user.setPassword(value);
        } else if (field.equals("email")) {
            user.setEmail(value);
        }
    }
}