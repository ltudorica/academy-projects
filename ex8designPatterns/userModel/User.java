package teme.ex8designPatterns.userModel;

import teme.exemple.trials.ex8DesignPattersV2.ObserverUser;

import java.time.LocalDateTime;

public class User implements ObserverUser {
    private String email;
    private String password;
    private LocalDateTime lastLoggedIn;

    public User() {
    }

    public User(String email, String password) {
        setEmail(email);
        setPassword(password);
        lastLoggedIn = LocalDateTime.now();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User email: " + this.email + ", password: " + this.password +
                ", last logged in at " + this.lastLoggedIn;
    }

    @Override
    public void resetUser() {
        this.email = null;
        this.password = null;
        this.lastLoggedIn = null;
    }

    @Override
    public User clone() throws CloneNotSupportedException {
        User copy = new User();
        copy.email = this.email;
        copy.password = this.password;
        copy.lastLoggedIn = this.lastLoggedIn;
        return copy;
    }
}
