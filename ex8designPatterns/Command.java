package teme.ex8designPatterns;

public interface Command {
    boolean execute();
}
