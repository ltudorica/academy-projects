package teme.ex6CreationalDesignPatterns.rest;

import teme.ex6CreationalDesignPatterns.passwords.PasswordGenerator;

import java.util.Map;

public class RestApiHelper {
    private RestApiCall.RestApiBuilder builder;

    public RestApiHelper(RestApiCall.RestApiBuilder builder) {
        this.builder = builder;
    }

    public RestApiCall createRegisterCall(String userDetails) {
        if (builder == null) {
            return null;
        }
        return builder.verb(Verb.POST)
                .address(" http://mysite.com/register")
                .body(PasswordGenerator.registeredUsers.toString()) // TODO SAVED AS JSON? CHECK TASK
                // .contentType ("application/json) .body(userDetails)
                .body(userDetails) // TODO -= user details in main ar trebui sa ia metoda to string cu toate detaliile userului
                .useHttps(false)
                .build();
    }

    public RestApiCall createLoginCall(Map<String, String> params) {
        if (builder == null) {
            return null;
        }

        return builder.verb(Verb.GET)
                .address("http://mysite.com/login")
                .contentType("text")
                .params(params)
                .useHttps(true)
                .build();
    }
}
