package teme.ex6CreationalDesignPatterns.rest;

public enum Verb {
    GET, POST, PUT, DELETE;
}
