package teme.ex6CreationalDesignPatterns.rest;

import teme.ex6CreationalDesignPatterns.passwords.PasswordGenerator;

import java.util.HashMap;
import java.util.Map;

public class RestApiCall {
    private String address = "";
    private Verb verb = Verb.GET;
    private String contentType = "text";
    private String header = "";
    private String body = "";
    private Map<String, String> params = new HashMap<>();
    private boolean useHttps = true;
    private String authorizationToken = "";
    private static int http = 200;

    private RestApiCall() {

    }

    public int response(String email) {
        boolean isRegistered = PasswordGenerator.registeredUsers.contains(email);
        if (http == -1 && isRegistered) {
            return 200;
        } else if (http == -1 && !isRegistered) {
            return 401;
        }

        return RestApiCall.http;
    }

    private void sethttp(Verb verb) {
        switch (verb) {
            case GET:
                RestApiCall.http = -1;
                break;
            case POST:
                RestApiCall.http = 201;
                break;
            default:
                RestApiCall.http = 400;
                break;
        }
    }

    @Override
    public RestApiCall clone() throws CloneNotSupportedException {
        RestApiCall restApiCall = new RestApiCall();
        restApiCall.address = address;
        restApiCall.verb = verb;
        restApiCall.contentType = contentType;
        restApiCall.header = header;
        restApiCall.body = body;
        restApiCall.params = new HashMap<>(params);
        restApiCall.useHttps = useHttps;
        restApiCall.authorizationToken = authorizationToken;
        return restApiCall;
    }

    public static class RestApiBuilder {
        private RestApiCall restApiCall = new RestApiCall();

        public RestApiBuilder address(String address) {
            restApiCall.address = address;
            return this;
        }

        public RestApiBuilder verb(Verb verb) {
            restApiCall.sethttp(verb);
            restApiCall.verb = verb;
            return this;
        }

        public RestApiBuilder contentType(String contentType) {
            restApiCall.contentType = contentType;
            return this;
        }

        public RestApiBuilder header(String header) {
            restApiCall.header = header;
            return this;
        }

        public RestApiBuilder body(String body) {
            restApiCall.body = body;
            return this;
        }

        public RestApiBuilder params(Map<String, String> params) {
            restApiCall.params = params;
            return this;
        }

        public RestApiBuilder useHttps(boolean useHttps) {
            restApiCall.useHttps = useHttps;
            return this;
        }

        public RestApiBuilder authorizationToken(String authorizationToken) {
            restApiCall.authorizationToken = authorizationToken;
            return this;
        }


        public RestApiCall build() {
            RestApiCall copy = null;
            try {
                copy = restApiCall.clone();
                restApiCall = new RestApiCall();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            } finally {
                return copy;
            }
        }
    }


}
