package teme.ex6CreationalDesignPatterns;

import teme.ex6CreationalDesignPatterns.passwords.PasswordGenerator;
import teme.ex6CreationalDesignPatterns.passwords.User;
import teme.ex6CreationalDesignPatterns.passwords.UserType;
import teme.ex6CreationalDesignPatterns.rest.RestApiCall;
import teme.ex6CreationalDesignPatterns.rest.Verb;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Logger logger;
        if (args == null || args.length == 0) {
            logger = Logger.getInstance();
        } else {
            logger = Logger.getInstance(args[0]);
        }

        User user1 = PasswordGenerator.generateUser("user1@yahoo.com", UserType.REGULAR);
        User user2 = PasswordGenerator.generateUser("user2@yahoo.com", UserType.MANAGER);
        User user3 = PasswordGenerator.generateUser("user3@yahoo.com", UserType.ADMIN);

//        logger.info("- " + user1 + " - " + user1.getPassword() + " - ");
//        logger.info("- " + user2 + " - " + user2.getPassword() + " - ");
//        logger.info("- " + user3 + " - " + user3.getPassword() + " - ");

        RestApiCall.RestApiBuilder restApiBuilder = new RestApiCall.RestApiBuilder();
        RestApiCall restApiCall1 = restApiBuilder.verb(Verb.POST)
                .address(" http://mysite.com/register")
                .body(PasswordGenerator.registeredUsers.toString()) // TODO SAVED AS JSON? CHECK TASK
                .useHttps(false)
                .build();

        System.out.println(restApiCall1.response("user2@yahoo.com"));

        Map<String, String> params = new HashMap<>();
        params.put("user", user1.getEmail());
        params.put("password", user2.getEmail());
        params.put("user_type", user3.getEmail());
        RestApiCall restApiCall2 = restApiBuilder.verb(Verb.GET)
                .address("http://mysite.com/login")
                .contentType("text").params(params)
                .useHttps(true)
                .build();

        System.out.println(restApiCall2.response("bla@yahoo.com"));

//        RestApiHelper restApiHelper = new RestApiHelper((restApiBuilder));
////        restApiHelper.createRegisterCall(user1.toString()); -> V2 cu Director


    }
}
