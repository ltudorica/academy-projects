package teme.ex6CreationalDesignPatterns.passwords;

import teme.ex6CreationalDesignPatterns.exceptions.InvalidEmailException;

public class RegularUser extends User {

    RegularUser(String email) throws InvalidEmailException {
        super(email);
    }

    @Override
    public String toString() {
        return "Regular User";
    } //met asta treb sa fie pt a baga in fisier email user parola

}
