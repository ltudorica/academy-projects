package teme.ex6CreationalDesignPatterns.passwords;

import teme.ex6CreationalDesignPatterns.exceptions.InvalidEmailException;

public class AdminUser extends User {
    AdminUser(String email) throws InvalidEmailException {
        super(email);
    }

    @Override
    public String toString() {
        return "Admin User";
    }
}
