package teme.ex6CreationalDesignPatterns.passwords;

import teme.ex6CreationalDesignPatterns.Logger;
import teme.ex6CreationalDesignPatterns.exceptions.InvalidEmailException;

import java.util.ArrayList;
import java.util.List;

public class PasswordGenerator {
    public static List<String> registeredUsers = new ArrayList<>();

    public static User generateUser(String email, UserType type) {
        if (email != null) {
            Logger.info(email + ", " + type + " user");
            registeredUsers.add(email); //TODO take this out and read the emails from the file
        }

        if (UserType.REGULAR == type) {
            try {
                return new RegularUser(email);
            } catch (InvalidEmailException e) {
                e.printStackTrace();
            }
        } else if (UserType.ADMIN == type) {
            try {
                return new AdminUser(email);
            } catch (InvalidEmailException e) {
                e.printStackTrace();
            }
        } else if (UserType.MANAGER == type) {
            try {
                return new ManagerUser(email);
            } catch (InvalidEmailException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
