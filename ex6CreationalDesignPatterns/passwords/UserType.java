package teme.ex6CreationalDesignPatterns.passwords;

public enum UserType {
    REGULAR, MANAGER, ADMIN;
}
