package teme.ex6CreationalDesignPatterns.passwords;

import teme.ex6CreationalDesignPatterns.exceptions.InvalidEmailException;

public class ManagerUser extends User {

    ManagerUser(String email) throws InvalidEmailException {
        super(email);
    }

    @Override
    public String toString() {
        return "Manager User";
    }
}
