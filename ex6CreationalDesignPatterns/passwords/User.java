package teme.ex6CreationalDesignPatterns.passwords;

import teme.ex6CreationalDesignPatterns.exceptions.InvalidEmailException;

import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class User {
    protected String email;
    protected String password;
    Random random = new Random();

    User(String email) throws InvalidEmailException {
        setEmail(email);
        setPassword();
    }

    void setEmail(String email) throws InvalidEmailException {
        String matchingString = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern p = Pattern.compile(matchingString);
        Matcher m = p.matcher(email);

        if (m.matches()) {
            this.email = email;
        } else {
            throw new InvalidEmailException();
        }
    }

    void setPassword() {
        String pass = UUID.randomUUID().toString();
        pass = pass.replace("-", "");
        int start = 0;
        int range = random.nextInt(8) + 8;
        this.password = pass.substring(start, range);
    }

    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return email;
    }
}
