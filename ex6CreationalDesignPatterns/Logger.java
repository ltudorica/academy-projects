package teme.ex6CreationalDesignPatterns;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class Logger {
    private static Logger instance = null;
    private static String fileName;

    private Logger() {

    }

    public static synchronized Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
            Logger.fileName = "log.txt";
        }
        return instance;
    }

    public static synchronized Logger getInstance(String fileName) {
        if (instance == null) {
            instance = new Logger();
            Logger.fileName = fileName;
        }
        return instance;
    }

    public static synchronized void error(String message) {
        writeInFile(message, "ERROR");
    }

    public static synchronized void warning(String message) {
        writeInFile(message, "WARNING");
    }

    public static synchronized void info(String message) {
        writeInFile(message, "INFO");
    }

    private static synchronized void writeInFile(String message, String type) {

        File file = new File(Logger.fileName);
        createFileIfItsNotCreated(file);

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(file, true))) {
            printWriter.println();
            printWriter.print(type + ": ");
            printWriter.print(LocalDateTime.now() + " ");
            printWriter.print(" " + message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void createFileIfItsNotCreated(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
