package teme.exemple.company.OOP;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Student s1 = new Student();
        s1.afisare();

        Student s2 = new Student("Ion Popescu");
        s2.afisare();

        // s2 = s1 -> shallow copy
        // s2 = new Student(s1); // clone copy

        s2 = s1.clona(); // clone copy, la alta adresa de memorie

        System.out.println(s1.getNume());
        s1.setNume("Ion");

        System.out.println("bla" + s1.getNume());

        Student s4 = (Student) s1.clone();
        System.out.println(s4.tipLoc);
    }
}
