package teme.exemple.company.OOP;

public class Student implements Cloneable {
    private String nume;
    private float medie;
    private int[] note = {1, 2};
    public TipLoc tipLoc; // poti crea si setari pt enum

    Student() {
        nume = "";
        medie = 1;
    }

    Student(String nume) {
        this.nume = nume;
    }

    public int[] getNote() { // se evita returnarea atripbutului, pt a nu putea fi schimbat - cu ref. obiectului
        int[] copy = new int[note.length];
        System.arraycopy(note, 0, copy, 0, note.length);
        return copy;
    }

    Student(Student st) {  // clone methos
        this.nume = st.nume;
        this.medie = st.medie;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        if (nume != null && nume.length() > 3) { // e evita scurcircuitarea
            this.nume = nume;
        }

    }

    void afisare() {
        System.out.printf("Studentul %s  are media %.2f", nume, medie);
    }

    /*@Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }*/ // methoda predefinita(built in)

    public Student clona() {
        Student s = new Student();
        s.nume = nume;
        s.medie = medie;
        return s;
    }

    @Override
    public String toString() {
        return String.format("Studentul %s  are media %.2f", nume, medie);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Student copy = (Student) super.clone(); // nu se fol constructorul in cazul in care cls e abstracta (student)
        copy.nume = nume;
        copy.medie = medie;
        copy.tipLoc = tipLoc;
        copy.note = new int[note.length]; // sau System.arraycopy();
        for (int i = 0; i < note.length; i++) {
            copy.note[i] = note[i];
        }
        return copy;
    }
}
