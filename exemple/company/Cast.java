package teme.exemple.company;

public class Cast {
    public static void main(String[] args) {
        int x = 2;
        long y = x; // cast implicit
        int z = (int) y; //cast explicit

        String a = "abc";
        String b = "a" + "bc";
        System.out.println(a == b);

    }
}
