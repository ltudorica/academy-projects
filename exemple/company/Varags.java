package teme.exemple.company;

public class Varags {
    public static long sum(int... values) {
        long sum = 0;

        for (int x : values) {
            sum += x;
        }
        return sum;
    }

    public static void main(String[] args) {
        sum(); // nu intr in for
        sum(2, 3, 4);

        System.out.printf("%s", "asd");
    }
}
