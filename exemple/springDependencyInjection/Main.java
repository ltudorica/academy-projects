package teme.exemple.springDependencyInjection;
// main poate fi ioc container -> cakuta clasa care vrem sa o cream cu ajutorul interfetei comune Sortable

//PRINCIPIUL DIP -> Strategy / loose coupling
public class Main {
    public static void main(String[] args) {
        MyData data = new MyData();
        //data.sort();
//        data.sort(1);
//        data.sort(2);
        data.sort(new BubbleSort());
        data.sort(new InterchangeSort());

        MyData data2 = new MyData(new InterchangeSort());
        data2.sort();
    }
}
