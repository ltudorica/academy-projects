package teme.exemple.springDependencyInjection;

public interface Sortable {
    void sort(int[] values);
}
