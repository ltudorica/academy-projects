package teme.exemple.springDependencyInjection;

public class InterchangeSort implements Sortable {

    public void interchangeSort(int[] values) {
        System.out.println("Sor[ting with InterChange");
    }

    @Override
    public void sort(int[] values) {
        this.interchangeSort(values);
    }
}
