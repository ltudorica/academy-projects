package teme.exemple.springDependencyInjection;

public class MyData {
    int[] values;
    Sortable mySortAlgorithm = null; //initializam cu contructor injector sau setInjector in cazul in care vrem sa
    // fortam progr sa ne dea un algortm de sortare pt construirea obj MyData

    public MyData() {
        mySortAlgorithm = new BubbleSort();
    }

    //-> constructor injection
    public MyData(Sortable sortable) {
        mySortAlgorithm = sortable;
    }

    //-> setter injection
    public void setSortAlgorithm(Sortable sort) {
        this.mySortAlgorithm = sort;
    }

    public void sort(Sortable sortAlgorithm) { //loose coupling
        sortAlgorithm.sort(this.values);
    }


    //Version 5 -> prin constructor injector, avem garantia ca obiectul e initializat la creare,
    // altfel (prin setter) treb sa verificam daca obj a fost creat
    public void sort() {
        this.mySortAlgorithm.sort(this.values);
    }

    //     Version 1
//    public void sort() {
//        System.out.println("Bubble sorting the values");
//    }

    //Version2
//    public void sort() {
//        BubbleSort bubbleSort = new BubbleSort();
//        bubbleSort.bubbleSort(this.values);
//    }
//    Version 3
//    public void sort(int type) {
//        if(type == 1){
//            BubbleSort bubbleSort = new BubbleSort();
//            bubbleSort.bubbleSort(values);
//        }
//        if(type == 2) {
//            InterchangeSort interchangeSort = new InterchangeSort();
//            interchangeSort.interchangeSort(values);
//        }
//    }


}
