package teme.exemple.springDependencyInjection;

public class BubbleSort implements Sortable {

    public void bubbleSort(int[] values) {
        System.out.println("Bubble sorting the values from BubbleSort");
    }

    @Override
    public void sort(int[] values) {
        this.bubbleSort(values);
    }
}
