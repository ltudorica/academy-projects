package teme.exemple.threads.ThreadExampleWeek3Day2;

import java.util.Vector;
import java.util.concurrent.Callable;

public class SumCallable implements Callable<Integer> {

    private Vector<Integer> values = new Vector<>();

    public SumCallable(Integer... value) {
        for (Integer i : value) {
            values.add(i);
        }
    }

    @Override
    public Integer call() throws Exception {
//        int sum = 0;
//        for (Integer i : values) {
//            sum += i;
//        }
//
//        return sum;

        //values.stream().mapToInt(i -> (int)i).sum();
        return values.stream().reduce((a, b) -> a + b).get(); // ppti verifica daca ai valoare cu .isPresent() in loc de get()
        //values.stream().filter(x -> x % 2 == 0).reduce((a, b) -> a + b).get(); // -> suma numerelor pare gen
    }
}
