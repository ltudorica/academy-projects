package teme.exemple.threads.ThreadExampleWeek3Day2;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Task t = new Task();
        t.start();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SumCallable s1 = new SumCallable(1, 2, 3);
        SumCallable s2 = new SumCallable(4, 5, 6, 7, 8, 9);

        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        Future<Integer> f1 = threadPool.submit(s1); // aici ar fi executie secventiala - doar un future
        Future<Integer> f2 = threadPool.submit(s2);

        threadPool.shutdown();
        List<Future<Integer>> futures = threadPool.invokeAll(Arrays.asList(s1, s2)); // .invokeAll e blocant -> le ast pe toate sa termine (ca get())
        System.out.println("Test");

        //...

        try {
            System.out.println(f1.get() + " " + f2.get()); //aici .get() e blocant / asteapta pana cand termina, un fel de join
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("Main ended");
    }
}
