package teme.exemple.threads.ThreadsWeek2Day4Day5.ProducerConsumer;

public class Market {
    public static final int MAX_QUANTITY = 100;
    private volatile int quantity;

    public Market(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public synchronized void supply(int noProducts) throws InterruptedException {
        this.notifyAll();
        if (this.quantity > MAX_QUANTITY) {
            System.out.println("******************Blocking producer: " + this.quantity);
            this.wait();
        } else {
            if (this.quantity + noProducts < 100) {
                this.quantity += noProducts;
            } else {
                this.quantity = 100;
            }

            System.out.println("Stock after supply: " + this.quantity);
        }


    }

    public synchronized void buy(int noProducts) throws InterruptedException {
        this.notifyAll();
        if (noProducts > this.quantity) {
            System.out.println("Blocking buyer: " + this.quantity);
            this.wait();
        } else {
            this.quantity -= noProducts;
        }
        System.out.println("Stock after buy: " + this.quantity);
    }
}
