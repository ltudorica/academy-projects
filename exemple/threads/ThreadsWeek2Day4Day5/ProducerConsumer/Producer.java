package teme.exemple.threads.ThreadsWeek2Day4Day5.ProducerConsumer;

import java.util.Random;

public class Producer extends Thread {

    Market market;

    public Producer(Market market) {
        this.market = market;
    }

    @Override
    public void run() {
        while (true) {
            Random random = new Random();
            int noProducts = random.nextInt(10);

            try {

                System.out.println("Producer creates " + noProducts);
                market.supply(noProducts);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
