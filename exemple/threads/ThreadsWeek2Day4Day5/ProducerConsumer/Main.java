package teme.exemple.threads.ThreadsWeek2Day4Day5.ProducerConsumer;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Market market = new Market(10);
        Thread producer = new Thread(new Producer(market));
        Thread buyer = new Thread(new Consumer(market));

        producer.start();
        buyer.start();

        producer.join();
        buyer.join();

        System.out.println("The end");
    }
}
