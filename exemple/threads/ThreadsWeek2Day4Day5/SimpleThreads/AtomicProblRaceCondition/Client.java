package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.AtomicProblRaceCondition;

import java.util.Random;

public class Client implements Runnable {
    String name;
    BankAccount account;

    public Client(String name, BankAccount account) {
        this.name = name;
        this.account = account;
    }

    @Override
    public void run() {
        while (this.account.getBalance() > 0) {
            Random random = new Random();
            double value = random.nextInt(10);
            System.out.println("Name: " + this.name + " / Account balance = " + this.account.getBalance());
            this.account.pay(value);
        }

        System.out.println("Exit account " + this.account.getBalance());
    }
}
