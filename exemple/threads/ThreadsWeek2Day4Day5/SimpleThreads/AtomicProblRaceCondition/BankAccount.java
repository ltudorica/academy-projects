package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.AtomicProblRaceCondition;

public class BankAccount {

    private double balance;

    public BankAccount(double balance) {
        this.balance = balance;
    }

    public synchronized void pay(double value) { // fctie atomica
        if (value <= this.balance) {
            System.out.println("Payment confirmation " + value);
            this.balance -= value;
        }
    }

    public double getBalance() {
        return this.balance;
    }
}
