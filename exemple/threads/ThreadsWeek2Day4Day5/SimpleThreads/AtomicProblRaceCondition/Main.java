package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.AtomicProblRaceCondition;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        BankAccount account = new BankAccount(1000);

        Thread john = new Thread(new Client("John", account));
        Thread alice = new Thread(new Client("Alice", account));

        john.start();
        alice.start();

        john.join();
        alice.join();

        //balanta iese negativa pt ca threadurile apeleaza metoda in acelasi timp / degeaba verifica in if daca balanta e mai mare de 0
    }
}
