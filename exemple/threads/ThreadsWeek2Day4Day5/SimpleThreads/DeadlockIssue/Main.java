package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.DeadlockIssue;

public class Main {

    public static class Friend {
        String name;

        public Friend(String name) {
            this.name = name;
        }

        public void hello(Friend friend) {
            System.out.println(String.format("%s says hello to %s", this.name, friend.name));
            friend.helloBack(this);
        }

        public void helloBack(Friend t) {
            System.out.println(String.format("%s responds with hello to %s", t.name, this.name));
        }

///         synchronized pe ambele metode: deadlock - blocheaza contextul
//        public synchronized void hello(Friend friend) {
//            System.out.println(String.format("%s says hello to %s", this.name, friend.name));
//            friend.helloBack(this);
//        }
//
//        public synchronized void helloBack(Friend t) {
//            System.out.println(String.format("%s responds with hello to %s", t.name, this.name));
//        }

    }

    public static void main(String[] args) throws InterruptedException {

        Friend friendJohn = new Friend("John");
        Friend friendAlice = new Friend("Alice");

        Thread john = new Thread(() -> friendJohn.hello(friendAlice));
        john.setName("John");
        Thread alice = new Thread(() -> friendAlice.hello(friendJohn));
        john.setName("Alice");

        john.start();
        alice.start();

        john.join();
        alice.join();

        System.out.println("Party is over!");
    }
}
