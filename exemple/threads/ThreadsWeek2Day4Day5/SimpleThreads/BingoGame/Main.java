package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.BingoGame;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        BingoGame game = new BingoGame(20);
        Thread[] players = new Thread[5];
        players[0] = new Thread(new Player(game, 13));
        players[0].setName("John");
        players[1] = new Thread(new Player(game, 9));
        players[1].setName("Alice");
        players[2] = new Thread(new Player(game, 4));
        players[2].setName("Bob");
        players[3] = new Thread(new Player(game, 15));
        players[3].setName("Luke");
        players[4] = new Thread(new Player(game, 15));
        players[4].setName("Maria");

        for (Thread t : players) {
            t.start();
        }

        Thread theGame = new Thread(game);
        theGame.start();

        for (Thread t : players) {
            t.join();
        }
        theGame.join();
    }
}
