package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.BingoGame;

import java.util.Random;

public class BingoGame implements Runnable {
    private volatile int theNumber = 0;
    private int limit;

    public BingoGame(int limit) {
        this.limit = limit;
    }

    public int getTheNumber() {
        return this.theNumber;
    }


    @Override
    public void run() {
        int noIterations = limit;
        while (noIterations-- != 0) {
            Random random = new Random();

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            theNumber = random.nextInt(limit);
            System.out.println("The number is " + this.theNumber);

        }
        this.theNumber = -1;
    }
}
