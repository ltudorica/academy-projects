package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.BingoGame;

public class Player implements Runnable {

    BingoGame game;
    int luckyNumber;

    public Player(BingoGame game, int luckyNumber) {
        this.game = game;
        this.luckyNumber = luckyNumber;
    }

    @Override
    public void run() {
        while (game.getTheNumber() != -1) {
            //System.out.println("Checking " + game.getTheNumber());
            if (game.getTheNumber() == luckyNumber) {
                System.out.println(Thread.currentThread().getName() + " gets a prize");
                return;
            }
        }
    }
}
