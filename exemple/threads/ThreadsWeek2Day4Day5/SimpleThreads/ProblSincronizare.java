package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads;

public class ProblSincronizare {

    public static class MyHelloThread implements Runnable {

        private String name;

        public MyHelloThread(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                System.out.println("Hello " + name + " from " + Thread.currentThread().getName());
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        // void run() {}
        // () -> {}

        MyHelloThread myHelloThread = new MyHelloThread("John");
        Thread t1 = new Thread(myHelloThread);

        Thread t2 = new Thread(new MyHelloThread("Alice"));

        Thread t3 = new Thread(() -> {
            for (int i = 0; i < 3; i++) {
                System.out.println("Hello from Anonymous" + (i + 1));
            }
        });

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();
        //probl: thredurile se executa dupa main daca nu dam join() inainte de terminakrea mainulul (procesului)

        System.out.println("Bye !");


    }

}
