package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.ImcompleteAtomicOp;

import java.util.concurrent.atomic.AtomicInteger;

public class BankAccount {

    private AtomicInteger balance;

    public BankAccount(int balance) {
        this.balance = new AtomicInteger();
        this.balance.set(balance);
    }

    public void pay(int value) { // fctie atomica
        int balance = this.balance.get();

        if (value <= balance) {
            System.out.println("Payment confirmation " + value);
            this.balance.addAndGet(-1 * value);
        }
    }

    public int getBalance() {
        return -1;
    }
}
