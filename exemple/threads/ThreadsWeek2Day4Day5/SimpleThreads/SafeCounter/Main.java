package teme.exemple.threads.ThreadsWeek2Day4Day5.SimpleThreads.SafeCounter;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static class Counter {
        AtomicInteger counter = new AtomicInteger();

        public void increment() {
            this.counter.incrementAndGet();
        }

        public int getCounter() {
            return this.counter.get();
        }
    }

    public static class IntervalCheck extends Thread {
        int lowLimit;
        int upLimit;
        Counter counter;

        public IntervalCheck(int lowLimit, int upLimit, Counter counter) {
            this.lowLimit = lowLimit;
            this.upLimit = upLimit;
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = lowLimit; i < upLimit; i++) {
                if (i % 3 == 0) {
                    counter.increment();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // 1 thread

        Counter counter1 = new Counter();
        Thread t = new IntervalCheck(1, 1_000_000_000, counter1);
        double tStart = System.currentTimeMillis();
        t.start();
        t.join();
        double tFinal = System.currentTimeMillis();
        System.out.println(String.format("Counter for one thread is %d in %f ms", counter1.getCounter(), (tFinal - tStart)));


        //3 threads
        Counter counter2 = new Counter(); // solutia sa ruleze mai rpd pe 3 threaduri ar fi sa facem contori diferiti si sa scoatem atomicinteger
        int limit = 1_000_000_000;
        Thread t1 = new IntervalCheck(1, limit / 3, counter2);
        Thread t2 = new IntervalCheck(limit / 3, 2 * limit / 3, counter2);
        Thread t3 = new IntervalCheck(2 * limit / 3, limit, counter2);

        tStart = System.currentTimeMillis();
        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        tFinal = System.currentTimeMillis();

        System.out.println(String.format("Counter for 3 threads is %d in %f ms", counter2.getCounter(), (tFinal - tStart)));
    }
}
