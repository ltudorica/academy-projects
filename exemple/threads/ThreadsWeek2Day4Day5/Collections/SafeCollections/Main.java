package teme.exemple.threads.ThreadsWeek2Day4Day5.Collections.SafeCollections;

import java.util.Arrays;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class Main {

    public static void testFailFastIterator() throws InterruptedException {


        Vector<Integer> values = new Vector<>();
        values.addAll(Arrays.asList(10, 20, 30, 40, 50, 60));

        Thread t1 = new Thread(() -> {

//            values.forEach( x -> {  // iterator synchronized
//                try {
//                    Thread.sleep(300);
//                    System.out.println("Value is : " + x);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            });

            for (int value : values) {

                try {
                    Thread.sleep(300);
                    System.out.println("Value is : " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(() -> {

            try {
                Thread.sleep(100);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            values.set(0, 100);
            values.set(5, 200);
            values.add(900);


        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();


        for (int value : values) {
            System.out.println("Value = " + value);
        }
    }

    public static void testConcurrentHashMap() throws InterruptedException {
        ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();
//        HashMap<String, Integer> map = new HashMap<>();

        Thread t1 = new Thread(() -> {
            Random random = new Random(200);

            for (int i = 0; i < 10; i++) {
                int value = random.nextInt(20);
                map.put("key: " + value, value);
            }
        });

        Thread t2 = new Thread(() -> {
            Random random = new Random(200);

            for (int i = 0; i < 10; i++) {
                int value = random.nextInt(20);
                map.put("key: " + value, value);
            }
        });


        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println(map);
    }

    public static void main(String[] args) throws InterruptedException {

//        testFailFastIterator();

        testConcurrentHashMap();
    }
}
