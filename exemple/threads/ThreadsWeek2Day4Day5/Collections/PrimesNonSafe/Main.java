package teme.exemple.threads.ThreadsWeek2Day4Day5.Collections.PrimesNonSafe;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;

public class Main {

    public static int MAX_LIMIT = (int) (2 * 1e7);

    public static Integer test1Thread(List<Integer> list) {
        int lowLimit = 0;
        int upLimit = MAX_LIMIT;
        PrimeChecker checker = new PrimeChecker(list, lowLimit, upLimit);
        Thread t = new Thread(checker);
        t.start();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return checker.getContor();
    }

    public static Integer test2Thread(List<Integer> list) {
        list.clear();

        int noThreads = Runtime.getRuntime().availableProcessors();   /// cate core uri am pe PC
        System.out.println("No of threads = " + noThreads);

        int chunkSize = MAX_LIMIT / noThreads;

        Thread[] threads = new Thread[noThreads];

        for (int i = 0; i < noThreads; i++) {
            int lowLimit = i * chunkSize;
            int upLimit = (i == noThreads - 1) ? MAX_LIMIT : (i + 1) * chunkSize;
            threads[i] = new PrimeChecker(list, lowLimit, upLimit);
        }

        for (int i = 0; i < noThreads; i++) {
            threads[i].start();
        }

        for (int i = 0; i < noThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int noPrimes = 0;

        for (Thread p : threads) {
            noPrimes += ((PrimeChecker) p).getContor();
        }

        return noPrimes;
    }

    public static Integer test3Thread(List<Integer> list) {
        list.clear();

        int noThreads = Runtime.getRuntime().availableProcessors();   /// cate core uri am pe PC
        System.out.println("No of threads = " + noThreads);


        Thread[] threads = new Thread[noThreads];

        for (int i = 0; i < noThreads; i++) {
            int lowLimit = (2 * i) + 1;
            int upLimit = MAX_LIMIT;
            threads[i] = new PrimeChecker2(list, lowLimit, upLimit, 2 * noThreads);
        }

        for (int i = 0; i < noThreads; i++) {
            threads[i].start();
        }

        for (int i = 0; i < noThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int noPrimes = 0;

        for (Thread p : threads) {
            noPrimes += ((PrimeChecker2) p).getContor();
        }

        return noPrimes;
    }

//    public static Integer test4Thread(ConcurrentLinkedDeque<Integer> list) {
//        list.clear();
//
//        int noThreads = Runtime.getRuntime().availableProcessors();   /// cate core uri am pe PC
//        System.out.println("No of threads = " + noThreads);
//
//
//        Thread[] threads = new Thread[noThreads];
//
//        for (int i = 0; i < noThreads; i++) {
//            int lowLimit = i;
//            int upLimit = MAX_LIMIT;
//            threads[i] = new PrimeChecker3(list, lowLimit, upLimit, noThreads);
//        }
//
//        for (int i = 0; i < noThreads; i++) {
//            threads[i].start();
//        }
//
//        for (int i = 0; i < noThreads; i++) {
//            try {
//                threads[i].join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        int noPrimes = 0;
//
//        for (Thread p : threads) {
//            noPrimes += ((PrimeChecker3) p).getContor();
//        }
//
//        return noPrimes;
//    }

    public static void benchmark(String title, Function<List<Integer>, Integer> function, List<Integer> primes) {
        primes.clear();

        double tStart = System.currentTimeMillis();
        int noPrimes = function.apply(primes);
        double tFinal = System.currentTimeMillis();

        System.out.println("*********Benchmark " + title);
        System.out.println("No primes " + noPrimes);
        System.out.println("No primes in list " + primes.size());
        System.out.println("Duration: " + (tFinal - tStart) + " ms");

    }

//    public static void benchmark2(String title, Function<ConcurrentLinkedDeque<Integer>, Integer> function, ConcurrentLinkedDeque<Integer> primes) {
//        primes.clear();
//
//        double tStart = System.currentTimeMillis();
//        int noPrimes = function.apply(primes);
//        double tFinal = System.currentTimeMillis();
//
//        System.out.println("*********Benchmark " + title);
//        System.out.println("No primes " + noPrimes);
//        System.out.println("No primes in list " + primes.size());
//        System.out.println("Duration: " + (tFinal - tStart) + " ms");
//
//    }

    public static void main(String[] args) {

        List<Integer> primes = new ArrayList<>();
        benchmark("Test with one thread", Main::test1Thread, primes);
        benchmark("Test with n threads ", Main::test2Thread, primes);

        Vector<Integer> primes2 = new Vector<>();
        benchmark("Test with n threads", Main::test2Thread, primes2);
        System.out.println();
        benchmark("Test with unsafe collection of threads & load balancing -> List", Main::test3Thread, primes);
        System.out.println();
        benchmark("Test with collection & load balancing -> Vector", Main::test3Thread, primes2);

//        System.out.println();
//        ConcurrentLinkedDeque<Integer> test = new ConcurrentLinkedDeque<>();
//        benchmark2("Test with safe collection of threads & load balancing -> Deque", Main::test4Thread, test);

    }
}
