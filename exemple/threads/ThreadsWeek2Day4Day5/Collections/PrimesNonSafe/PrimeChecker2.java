package teme.exemple.threads.ThreadsWeek2Day4Day5.Collections.PrimesNonSafe;

import java.util.List;

public class PrimeChecker2 extends Thread {

    List<Integer> primes;
    int contor = 0;
    int lowLimit;
    int upLimit;
    int pace;

    public PrimeChecker2(List<Integer> primes, int lowLimit, int upLimit, int pace) {
        this.primes = primes;
        this.lowLimit = lowLimit;
        this.upLimit = upLimit;
        this.pace = pace;
    }

    public int getContor() {
        return this.contor;
    }

    @Override
    public void run() {
        for (int i = this.lowLimit; i < upLimit; i += pace) {
            boolean isPrime = true;
            for (int j = 2; j < Math.sqrt(i); j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                this.contor += 1;
                this.primes.add(i);
            }
        }

    }
}
