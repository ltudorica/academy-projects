package teme.exemple.threads.ThreadsWeek3Day1.CompletableFuture;

import java.util.function.Function;

public class WeatherServerConnection implements Function<String, String> {

    @Override
    public String apply(String s) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "the weather is sunny";
    }

    @Override
    public <V> Function<V, String> compose(Function<? super V, ? extends String> before) {
        return Function.super.compose(before);
    }

    @Override
    public <V> Function<String, V> andThen(Function<? super String, ? extends V> after) {
        return Function.super.andThen(after);
    }
}
