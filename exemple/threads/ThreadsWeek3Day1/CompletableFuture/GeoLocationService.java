package teme.exemple.threads.ThreadsWeek3Day1.CompletableFuture;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

public class GeoLocationService implements Callable<String>, Supplier<String> {

    @Override
    public String call() throws Exception {
        System.out.println("Getting your location ...");
        Thread.sleep(2000);
        return "Lat: 45, Long: 16";
    }

    @Override
    public String get() {
        System.out.println("Getting your location ...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Lat: 45, Long: 16";
    }
}
