package teme.exemple.threads.ThreadsWeek3Day1.CompletableFuture;

import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        CompletableFuture<String> weatherTask = new CompletableFuture<>(); //String e rez primit
        CompletableFuture<Void> result = weatherTask.supplyAsync(new GeoLocationService())
                .thenApply((x) -> {
                    // x este locatia
                    System.out.println("Getting weather data for " + x);

                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return "Weather is sunny. Temp is 32";
                }).thenApply((y) -> {  //ce ai nevoie de cate ori ai nevoie de met thenApply
                    System.out.println("Processing weather data " + y);
                    return y.toUpperCase();
                }).thenAccept((z) -> {
                    System.out.println("Weather data " + z);
                });

        result.get();
        System.out.println();

/// exceptie :
        CompletableFuture<String> result2 = weatherTask.supplyAsync(new GeoLocationService())
                .thenApply((x) -> {
                    // x este locatia
                    System.out.println("Getting weather data for " + x);

                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return "32a";
                }).thenApply((y) -> {  //ce ai nevoie de cate ori ai nevoie de met thenApply
                    if (y.isEmpty()) {
                        throw new UnsupportedOperationException();
                    }
                    System.out.println("Processing weather data " + y);
                    return y.toUpperCase();
                })
//                .exceptionally( (ex) -> {   // pt tratarea exceptiilor
//                    System.out.println("Service error " + ex.getCause());
//                    return "No data";
//                })
                .handle((res, ex) -> {  // rezultat, exceptie
                    if (ex != null) {
                        return "Houston, we have a problem " + ex.getCause();
                    }
                    return res.toLowerCase();
                });

        //poti face chain dupa exceptionally in continuare - daca vrei

        String weatherData = result2.get();
        System.out.println("The result is: " + weatherData);

        System.out.println();
        System.out.println("***Calling completable future with class that implements Funtion");

        CompletableFuture<Void> asyncTask = weatherTask.supplyAsync(new GeoLocationService())
                .thenApply(new WeatherServerConnection())
                .thenApply((y) -> {  //ce ai nevoie de cate ori ai nevoie de met thenApply
                    System.out.println("Processing weather data " + y);
                    return y.toUpperCase();
                }).thenAccept((z) -> System.out.println("Weather data: " + z));

        asyncTask.get(); // progr principal sa ruleze - un fel de join


        //Callable vs CompletableFuture
        System.out.println();
        System.out.println("Callable version");

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<String> geoService = executorService.submit(new GeoLocationService());

        while (!geoService.isDone()) {
            System.out.println("Waiting for geo data");
        }

        String geoData = geoService.get();
        System.out.println("Your location is " + geoData);

        CompletableFuture<String> geoAsyncTask = new CompletableFuture<>();
        Future geoServiceAsync = geoAsyncTask.supplyAsync(new GeoLocationService())
                .thenAccept((x) -> {
                    try {
                        Thread.sleep(6000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Your location (again) is " + x);
                });

//        geoServiceAsync.get();

        System.out.println("The end");

    }
}
