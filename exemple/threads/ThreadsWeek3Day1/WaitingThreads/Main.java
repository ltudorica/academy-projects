package teme.exemple.threads.ThreadsWeek3Day1.WaitingThreads;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {

        int noThreads = 3;
        CountDownLatch countDownLatch = new CountDownLatch(noThreads);

        Worker worker1 = new Worker(countDownLatch, 10, 5, 50);
        Worker worker2 = new Worker(countDownLatch, 2, 0, 50);
        Worker worker3 = new Worker(countDownLatch, 1, 3, 50);


        System.out.println("Starting workers ...");

        ExecutorService service = Executors.newFixedThreadPool(noThreads);
        Future<List<Integer>> future1 = service.submit(worker1);
        Future<List<Integer>> future2 = service.submit(worker2);
        Future<List<Integer>> future3 = service.submit(worker3);

        System.out.println("Waiting results ...");

//        countDownLatch.await(); // daca ramane un thread blocat - putem da parametri la await - pt a astepta un anumit timp si atat sa putem continua execu;tia progr
        boolean normalTermination = countDownLatch.await(3, TimeUnit.SECONDS); // daca nu punem timp se asteapta terminarea tuturor threadurilor

        System.out.println("Counter value = " + countDownLatch.getCount());
        System.out.println("Counter termination = " + normalTermination);

        System.out.println("Printing results   ");

        // all threads have finished
        System.out.println(future1.get());
        if (future2.isDone()) {
            System.out.println(future2.get());
        } else {
            System.out.println("Worker 2 still running");
            future2.cancel(true);
        }

        System.out.println(future3.get());

//        service.shutdown();
        System.out.println();
        // same thing cu invokeAll, fara countdownlatch


        List<Worker> workers = Arrays.asList(worker1, worker2, worker3);
        List<Future<List<Integer>>> futures = service.invokeAll(workers, 3, TimeUnit.SECONDS);

        System.out.println("Results: " + futures.size());
        for (Future<List<Integer>> future : futures) {
            if (future.isDone() && !future.isCancelled()) {
                System.out.println("Result: " + future.get().toString());
            } else {
                System.out.println("Task canceled ");
            }
        }

        System.out.println();
        //pt rezultate in ordinea in care au fost prelucrate (threadurile) -> ExecutorCompletionService -> wrapper peste alt execurot service simplu


        ExecutorCompletionService<List<Worker>> executorCompletionService = new ExecutorCompletionService<>(service);
        executorCompletionService.poll();

        service.shutdown();

    }
}
