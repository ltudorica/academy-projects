package teme.exemple.threads.ThreadsWeek3Day1.WaitingThreads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class Worker implements Callable<List<Integer>> {
    CountDownLatch countDownLatch;
    int initialValue;
    int step;
    int upperLimit;

    public Worker(CountDownLatch countDownLatch, int initialValue, int step, int upperLimit) {
        this.countDownLatch = countDownLatch;
        this.initialValue = initialValue;
        this.step = step;
        this.upperLimit = upperLimit;
    }

    @Override
    public List<Integer> call() throws Exception {
        List<Integer> values = new ArrayList<>();

        //to interrupt the thread
        if (step == 0) {
            Thread.sleep(10000); // pt a bloca prohgr si vedea reactia countdown
        }
        for (int value = initialValue; value < upperLimit; value += step) {
            values.add(value);
        }

        countDownLatch.countDown(); //f. imp
        return values;
    }
}
