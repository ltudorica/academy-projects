package teme.exemple.threads.ExecutorWeek2Day5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Thread t = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(400);
                    System.out.println("Hello from " + Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Callable<Void> newT = new Callable<Void>() {
            @Override
            public Void call() {
                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(400);
                        System.out.println("New hello from " + Thread.currentThread().getName());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                return null;
            }
        };

        Callable<Integer> tSuma = () -> {
            List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
            int sum = 0;

            for (int value : list) {
                sum += value;
            }

            return sum;
        };


        ExecutorService service = Executors.newSingleThreadExecutor();
        service.execute(t);
        //service.submit(newT); ///sunt rulate secvential (fctiile din callabale si thread), se poate rula doar un tread cu met asta
        Future<Void> resultThread = service.submit(newT);

        //resultThread.get(); // pt a executa threadurile inainte de terminarea mainului / un fel de join

        while (!resultThread.isDone()) {
            System.out.println("Waiting result");
        }

        // acum rulam ambele threaduri
        ExecutorService service2 = Executors.newFixedThreadPool(5);

        Callable<Void> tException = () -> {
            Thread.sleep(2000);
            throw new UnsupportedOperationException();
        };

        Future resultException = service2.submit(tException);
        service2.submit(t);
        service2.submit(newT);
//        service2.submit(tSuma);

        Future<Integer> resultSuma = service2.submit(tSuma);
        System.out.println("Result - " + resultSuma.get());

        try {
            resultException.get();
        } catch (ExecutionException ex) {
            System.out.println("Am primit exceptia de la Callable " + ex.getCause());
        }

//        while(!service2.isTerminated()) { // un fel de join manual
//
//        }

        service.shutdown();
        if (!service.awaitTermination(3, TimeUnit.SECONDS)) {
            service.shutdownNow();
        }

        service2.shutdown();
        if (!service2.awaitTermination(3, TimeUnit.SECONDS)) {
            service2.shutdownNow();
        }

        System.out.println();
        System.out.println("Start Scheduler");
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
//        scheduler.schedule() //
        scheduler.scheduleAtFixedRate(t, 1000, 5000, TimeUnit.MILLISECONDS); // da drumul la infinit la thread

        Thread.sleep(5000);
        scheduler.shutdown();
        if (!scheduler.awaitTermination(3, TimeUnit.SECONDS)) {
            scheduler.shutdownNow();
        }

        //Lista cu Callable

        ArrayList<Callable<Integer>> threads = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            threads.add(() -> {
                return 5;
            });
        }

        ExecutorService service3 = Executors.newFixedThreadPool(5);
        List<Future<Integer>> results = service3.invokeAll(threads);
        int sum = 0;

        for (Future<Integer> result : results) {
            sum += result.get();
        }

        System.out.println("Threads result = " + sum);

        service3.shutdown();
        if (!service3.awaitTermination(3, TimeUnit.SECONDS)) {
            service3.shutdownNow();
        }

        System.out.println("The end");
    }
}
