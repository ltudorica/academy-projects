package teme.exemple.dateTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class NewDateTime {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);


        LocalTime lt = LocalTime.now();
        System.out.println(lt);

        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(ldt);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMMM-dd GGGG", Locale.forLanguageTag("ro-ro"));
        String val = formatter.format(ldt);
        System.out.println(val);

        ldt.with(ChronoField.MONTH_OF_YEAR, 4); // creating paramekter with temporary

        LocalDateTime date2 = LocalDateTime.of(2021, 9, 24, 11, 20);
        long days = ldt.until(date2, ChronoUnit.DAYS);
        System.out.println("days " + days);

        LocalDateTime bla = LocalDateTime.of(2021, 7, 23, 12, 00);
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("MM, dd yyyy @hh:mm");
        System.out.println(formatter1.format(bla));
        System.out.println();

        String date = "29/8/1995 - 12 50";
        DateTimeFormatter f1 = DateTimeFormatter.ofPattern("dd/M/yyyy - HH mm");
        LocalDateTime trial = LocalDateTime.parse(date, f1);

        DateTimeFormatter toPrint = DateTimeFormatter.ofPattern("MM-dd-yyyy / HH:mm");
        System.out.println(toPrint.format(trial));
    }
}
