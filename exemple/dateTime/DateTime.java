package teme.exemple.dateTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class DateTime {
    public static void main(String[] args) {
        Date date = new Date();   //outdated
        System.out.println(date);

        Date date1 = new Date(121, 6, 29, 11, 9);
        System.out.println(date1);

        System.out.println();
        GregorianCalendar calendar = new GregorianCalendar(); //outdated
        System.out.println(calendar.getTime());
        calendar.roll(Calendar.HOUR, 30);
        System.out.println(calendar.getTime());
        calendar.add(Calendar.HOUR, 30);
        System.out.println(calendar.getTime());
        System.out.println(calendar.getTime().getTime()); // shows in ms since 1970

        //calendar.set(2015, 8, 23, 12, 55, 9);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Masina t = new Masina();
        int z = Masina.field;
        Masina.Motor m = new Masina.Motor();
        m.f();

    }

    static class Masina {
        static {
            System.out.println("Static block");
        }

        public static int field = 0;
        int speed;

        static class Motor {
            void f() {
                //
            }
        }
    }


}
