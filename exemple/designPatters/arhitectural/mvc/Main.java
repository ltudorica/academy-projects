package teme.exemple.designPatters.arhitectural.mvc;

public class Main {
    public static void main(String[] args) {
        Student model = new Student();
        model.setName("Ana");
        model.setRollNo("1234");
//        StudentView view = new StudentView();
        StudentController controller = new StudentController(model);
        controller.updateView();
        controller.setStudentName("John");
        controller.updateView();

    }
}
