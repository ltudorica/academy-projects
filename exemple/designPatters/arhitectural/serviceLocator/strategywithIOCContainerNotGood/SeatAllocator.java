package teme.exemple.designPatters.arhitectural.serviceLocator.strategywithIOCContainerNotGood;

public interface SeatAllocator { //strategy
    int allocateSeat(int capacity);
}
