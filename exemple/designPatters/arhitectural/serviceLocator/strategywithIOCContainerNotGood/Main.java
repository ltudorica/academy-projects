package teme.exemple.designPatters.arhitectural.serviceLocator.strategywithIOCContainerNotGood;

public class Main {
    public static void main(String[] args) {
//        TicketGenerator ticketGenerator = new TicketGenerator(1000);
//        ticketGenerator.setSeatAllocatorStrategy(new ConsecutiveSeatAllocator());
//        ticketGenerator.setSeatAllocatorStrategy(new SocialDistancingSeatAllocator());

        IoC ioC = new IoC();
        ioC.register(SeatAllocator.class, SocialDistancingSeatAllocator.class);
        ioC.register(TicketGenerator.class, TicketGenerator.class);

        TicketGenerator ticketGenerator = ioC.resolve(TicketGenerator.class);
        // new Random()::nextInt simplificat
        // cand avem x -> smt(x) si sa coincida signatura - EX: nextInt primeste un int si
        // returneaza un int / la fel si metoda din interfata

        for (int i = 0; i < 5; i++) {
            try {
                int ticketNo = ticketGenerator.generateTicket();
                System.out.println("Ticket -> seat no. " + ticketNo);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }
}
