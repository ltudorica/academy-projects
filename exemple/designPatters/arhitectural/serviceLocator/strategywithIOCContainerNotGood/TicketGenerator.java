package teme.exemple.designPatters.arhitectural.serviceLocator.strategywithIOCContainerNotGood;

public class TicketGenerator {
    private int capacity = 0;
    private SeatAllocator seatAllocatorStrategy = null;

//initialClass - has a lot of ifs in generateTicket normaly - we inject seatAllocator to get rid of that


    public TicketGenerator(SeatAllocator seatAllocatorStrategy) {
        this.seatAllocatorStrategy = seatAllocatorStrategy;
    }

    public void setSeatAllocatorStrategy(SeatAllocator seatAllocatorStrategy) { //setter injection
        this.seatAllocatorStrategy = seatAllocatorStrategy;
    }

    public int generateTicket() throws Exception {
        if (seatAllocatorStrategy != null) { //numai aici continuam - sa fim siguri ca s-a apelat met set
            return seatAllocatorStrategy.allocateSeat(capacity);
        } else {
            throw new Exception("the strategy isn't a seat");
        }
    }
}
