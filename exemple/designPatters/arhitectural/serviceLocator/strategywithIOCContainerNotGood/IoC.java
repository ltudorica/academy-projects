package teme.exemple.designPatters.arhitectural.serviceLocator.strategywithIOCContainerNotGood;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class IoC {

    private static Map<Class<?>, Class<?>> map = new HashMap<>();

    public void register(Class<?> contract, Class<?> implementation) {
        map.put(contract, implementation);
    }

    public <T> T resolve(Class<?> contract) {
        Class<?> implementation = map.get(contract);

        Constructor<?> ctor = null;
        try {
            ctor = implementation.getDeclaredConstructor();
            var types = ctor.getParameterTypes();
            int k = 0;
            Object[] params = new Object[types.length];
            for (var type : types) {
                params[k++] = resolve(type);
            }
            return (T) ctor.newInstance(params);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        return null;
    }
}
