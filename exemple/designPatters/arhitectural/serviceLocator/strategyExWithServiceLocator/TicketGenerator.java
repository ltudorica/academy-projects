package teme.exemple.designPatters.arhitectural.serviceLocator.strategyExWithServiceLocator;

public class TicketGenerator {
    private int capacity = 0;
    private SeatAllocator seatAllocatorStrategy = null;

//initialClass - has a lot of ifs in generateTicket normaly - we inject seatAllocator to get rid of that


    public TicketGenerator(int capacity) {
        this.capacity = capacity;
//        seatAllocatorStrategy = ServiceLocator2.resolve(SeatAllocator.class);
    }


    public int generateTicket() throws Exception {
        if (seatAllocatorStrategy != null) { //numai aici continuam - sa fim siguri ca s-a apelat met set
            return seatAllocatorStrategy.allocateSeat(capacity);
        } else {
            throw new Exception("the strategy isn't a seat");
        }
    }
}
