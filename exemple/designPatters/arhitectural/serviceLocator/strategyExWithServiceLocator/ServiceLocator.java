package teme.exemple.designPatters.arhitectural.serviceLocator.strategyExWithServiceLocator;

import java.util.HashMap;
import java.util.Map;

public class ServiceLocator {
    //daca sunt statice metodele, nu poate fi testat
    //contract = interfata
    // varianta cu object

    private static Map<Class<?>, Object> map = new HashMap<>();

    public static void register(Class<?> contract, Object implementation) {
        map.put(contract, implementation);
    }

    public static Object resolve(Class<?> contract) {
        return map.get(contract);
    }

}
