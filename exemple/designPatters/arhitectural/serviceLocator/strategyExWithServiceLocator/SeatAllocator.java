package teme.exemple.designPatters.arhitectural.serviceLocator.strategyExWithServiceLocator;

public interface SeatAllocator { //strategy
    int allocateSeat(int capacity);
}
