package teme.exemple.designPatters.arhitectural.serviceLocator.strategyExWithServiceLocator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ServiceLocator2 {
    // varianta cu class - using reflexion
    private static Map<Class<?>, Class<?>> map = new HashMap<>();

    public static void register(Class<?> contract, Class<?> implementation) {
        map.put(contract, implementation);
    }

    public static <T> T resolve(Class<?> contract) {
        Class<?> implementation = map.get(contract);

        try {
            return (T) implementation.getDeclaredConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }
}
