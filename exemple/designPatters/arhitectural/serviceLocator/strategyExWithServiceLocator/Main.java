package teme.exemple.designPatters.arhitectural.serviceLocator.strategyExWithServiceLocator;

public class Main {
    public static void main(String[] args) {
        //ServiceLocator.register(SeatAllocator.class, new SocialDistancingSeatAllocator());
        ServiceLocator2.register(SeatAllocator.class, ConsecutiveSeatAllocator.class);
        TicketGenerator ticketGenerator = new TicketGenerator(1000);
//        ticketGenerator.setSeatAllocatorStrategy(new ConsecutiveSeatAllocator());
//        ticketGenerator.setSeatAllocatorStrategy(new SocialDistancingSeatAllocator());
//
//        ticketGenerator.setSeatAllocatorStrategy(x -> new Random().nextInt(x) );
        // new Random()::nextInt simplificat
        // cand avem x -> smt(x) si sa coincida signatura - EX: nextInt primeste un int si
        // returneaza un int / la fel si metoda din interfata

        for (int i = 0; i < 5; i++) {
            try {
                int ticketNo = ticketGenerator.generateTicket();
                System.out.println("Ticket -> seat no. " + ticketNo);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }
}
