package teme.exemple.designPatters.creationalPatterns.factory;

public class DataBaseServerFactory {
    private final static String DEVELOPMENT = "dev";
    private final static String PRODUCTION = "prod";
    private final static String PREPRODUCTION = "preprod";

    public static DataBaseServer getServer(String server) {
        if (DEVELOPMENT.equals(server)) {
            return new DevelopmentDataBaseServer();
        }
        if (PRODUCTION.equals(server)) {
            return new ProductionDataBaseServer();
        }
        if (PREPRODUCTION.equals(server)) {
            return new PreProductionDataBaseServer();
        }
        return null;
    }

    private static class PreProductionDataBaseServer implements DataBaseServer {

        private PreProductionDataBaseServer() {

        }

        @Override
        public void connect() {
            System.out.println("Pre Production DataBase Server");
        }
    }
}


