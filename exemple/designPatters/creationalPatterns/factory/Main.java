package teme.exemple.designPatters.creationalPatterns.factory;

public class Main {
    public static void main(String[] args) {

        DataBaseServer server = DataBaseServerFactory.getServer("dev");
        server.connect();
    }
}
