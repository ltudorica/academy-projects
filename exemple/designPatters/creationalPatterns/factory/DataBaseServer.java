package teme.exemple.designPatters.creationalPatterns.factory;

public interface DataBaseServer {
    void connect();
}
