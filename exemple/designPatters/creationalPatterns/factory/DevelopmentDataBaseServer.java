package teme.exemple.designPatters.creationalPatterns.factory;

public class DevelopmentDataBaseServer implements DataBaseServer {

    protected DevelopmentDataBaseServer() {

    }

    @Override
    public void connect() {
        System.out.println("Development server");
    }
}
