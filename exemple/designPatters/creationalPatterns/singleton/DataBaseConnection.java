package teme.exemple.designPatters.creationalPatterns.singleton;

public class DataBaseConnection {
    private static DataBaseConnection instance = null; // eager instantiation new DataBaseConnection();
    private static String address = "127 0.0.1";

    private DataBaseConnection() {

    }

    public static synchronized DataBaseConnection getInstance() {
        System.out.println(address);
        if (instance == null) {
            instance = new DataBaseConnection();
        }
        return instance;
    }

    public synchronized void reset() {
        System.out.println("Connection reset");
    }

    public synchronized void connect(String address) { // se deconecteaza de vechea adresa si se conecteaza la una noua
        DataBaseConnection.address = address;
        System.out.println(address);
    }
}
