package teme.exemple.designPatters.creationalPatterns.singleton;

public enum DataBaseConnectionV3 {
    INSTANCE;

    public synchronized void reset() {
        System.out.println("Connection with enum reset");
    }
}
