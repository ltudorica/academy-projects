package teme.exemple.designPatters.creationalPatterns.singleton;

public class Main {
    public static void main(String[] args) {
        DataBaseConnection connection = DataBaseConnection.getInstance();
        connection.reset();
        connection.connect("192.169.10.0");

        DataBaseConnectionV2 connectionV2 = DataBaseConnectionV2.instance;
        connectionV2.reset();

        DataBaseConnectionV3 connectionV3 = DataBaseConnectionV3.INSTANCE;
        connectionV3.reset();
    }
}
