package teme.exemple.designPatters.creationalPatterns.singleton;

public class DataBaseConnectionV2 {
    public static final DataBaseConnectionV2 instance = new DataBaseConnectionV2();

    private DataBaseConnectionV2() {
    }

    public synchronized void reset() {
        System.out.println("Version 2 reset");
    }
}
