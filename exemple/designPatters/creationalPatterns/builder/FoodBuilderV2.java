package teme.exemple.designPatters.creationalPatterns.builder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FoodBuilderV2 {
    private String name;
    private float price;
    private List<String> ingredients;
    private LocalDate expireDate;
    private boolean isGlutenFree;
    private boolean isVegetarian;
    private boolean isVegan;

    public FoodBuilderV2 addName(String name) {
        this.name = name;
        return this;
    }

    public FoodBuilderV2 addPrice(float price) {
        this.price = price;
        return this;
    }

    public FoodBuilderV2 addIngredients(List<String> ingredients) {
        this.ingredients = new ArrayList<>();
        if (ingredients != null) {
            this.ingredients.addAll(ingredients);
        }

        return this;
    }

    public FoodBuilderV2 addExpireDate(LocalDate date) {
        this.expireDate = date;
        return this;
    }

    public FoodBuilderV2 setIsGlutenFree(boolean isGlutenFree) {
        this.isGlutenFree = isGlutenFree;
        return this;
    }

    public FoodBuilderV2 setIsVegetarian(boolean isVegetarian) {
        this.isVegetarian = isVegetarian;
        return this;
    }

    public FoodBuilderV2 setIsVegan(boolean isVegan) {
        this.isVegan = isVegan;
        return this;
    }

    public Food build() {
        Food food = new Food(name, price, ingredients, expireDate,
                isGlutenFree, isVegetarian, isVegan);
        name = null;
        price = 0;
        ingredients = null;
        expireDate = null;
        isGlutenFree = false;
        isVegetarian = false;
        isVegan = false;
        return food;
    }
}
