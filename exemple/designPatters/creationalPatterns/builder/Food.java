package teme.exemple.designPatters.creationalPatterns.builder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Food implements Cloneable {
    private String name;
    private float price;
    private List<String> ingredients;
    private LocalDate expireDate;
    private boolean isGlutenFree;
    private boolean isVegetarian;
    private boolean isVegan;

    private Food() {

    }

    Food(String name, float price, List<String> ingredients,
         LocalDate expireDate, boolean isGlutenFree,
         boolean isVegetarian, boolean isVegan) {
        this.name = name;
        this.price = price;
        this.ingredients = new ArrayList<>();
        if (ingredients != null) {
            this.ingredients.addAll(ingredients);
        }
        this.expireDate = expireDate;
        this.isGlutenFree = isGlutenFree;
        this.isVegetarian = isVegetarian;
        this.isVegan = isVegan;
    }

    private Food(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Food food = new Food();
        food.name = name;
        food.price = price;
        food.ingredients = new ArrayList<>();
        if (ingredients != null) {
            food.ingredients.addAll(ingredients);
        }
        food.expireDate = expireDate;
        food.isGlutenFree = isGlutenFree;
        food.isVegetarian = isVegetarian;
        food.isVegan = isVegan;
        return food;
    }

    public static class FoodBuilder {
        private Food food = new Food();

        public FoodBuilder addName(String name) {
            food.name = name;
            return this;
        }

        public FoodBuilder addPrice(float price) {
            food.price = price;
            return this;
        }

        public FoodBuilder addIngredients(List<String> ingredients) {
            food.ingredients = new ArrayList<>();

            if (ingredients != null) {
                food.ingredients.addAll(ingredients);
            }
            return this;
        }

        public FoodBuilder addExpireDate(LocalDate date) {
            food.expireDate = date;
            return this;
        }

        public FoodBuilder setIsGlutenFree(boolean isGlutenFree) {
            food.isGlutenFree = isGlutenFree;
            return this;
        }

        public FoodBuilder setIsVegetarian(boolean isVegetarian) {
            food.isVegetarian = isVegetarian;
            return this;
        }

        public FoodBuilder setIsVegan(boolean isVegan) {
            food.isVegan = isVegan;
            return this;
        }

        public Food build() {
            Food copy = null;
            try {
                copy = (Food) food.clone();
                food = new Food();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            } finally {
                return copy;
            }
        }
    }
}
