package teme.exemple.designPatters.creationalPatterns.builder;

import java.time.LocalDate;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Food.FoodBuilder builder = new Food.FoodBuilder();
        Food food = builder.addName("Chocolate").addPrice(8)
                .addExpireDate(LocalDate.now())
                .setIsGlutenFree(true).build();

        System.out.println(food.getName());

        builder.addName("bla");

        System.out.println(food.getName());

        FoodBuilderV2 builderV2 = new FoodBuilderV2();
        Food food2 = builderV2.addName("bread").addPrice(5)
                .setIsVegan(true).build();

        System.out.println(food2.getName());
        Food food4 = builderV2.addIngredients(Arrays.asList("flour", "water", "salt")).build();


    }
}
