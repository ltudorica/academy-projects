package teme.exemple.designPatters.behavioral.observer;

public interface Observer {
    void update();
}
