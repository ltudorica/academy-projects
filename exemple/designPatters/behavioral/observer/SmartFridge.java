package teme.exemple.designPatters.behavioral.observer;

import java.util.List;
import java.util.Vector;

public class SmartFridge implements Subject {
    //ar fi putut sa fie doar cls asta, fara Subject
    // daca vrem sa fie pe threads, metodele ar treb sa fie synchronized

    public List<Observer> observers = new Vector<>();

    @Override
    public void subscribe(Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unsubscribe(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.update();
        }
    }
}
