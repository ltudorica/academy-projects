package teme.exemple.designPatters.behavioral.observer;

public class Programmer implements Observer {

    private String name;

    public Programmer(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        gotToFridge();
    }

    private void gotToFridge() {
        System.out.println("The programmer " + name + " is going to the fridge!");
    }
}
