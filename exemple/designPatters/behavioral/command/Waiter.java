package teme.exemple.designPatters.behavioral.command;

import java.util.List;
import java.util.Vector;

public class Waiter {
    // INVOKER

    List<FoodOrder> orders = new Vector<>(); //-> asta poate sa fie doar un obj de tip FoodOrder

    public void addOrder(FoodOrder order) {

        orders.add(order);
    }

    public void sendOrders() {
        for (int i = 0; i < orders.size(); i++) {
            orders.get(i).execute();
            //orders.remove(f);
        }
        orders.clear();

    }

}
