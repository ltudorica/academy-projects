package teme.exemple.designPatters.behavioral.command;

public interface FoodOrder {
    void execute();
}
