package teme.exemple.designPatters.behavioral.command;

public class Main {
    public static void main(String[] args) {
        Waiter waiter = new Waiter();
        Chef chef = new Chef();
        FoodOrder foodOrder1 = new PizzaOrder(chef);
        FoodOrder foodOrder2 = new PizzaOrder(chef);
        FoodOrder foodOrder3 = new BurgerOrder(chef);

        waiter.addOrder(foodOrder1);
        waiter.addOrder(foodOrder2);
        waiter.addOrder(foodOrder3);

        waiter.sendOrders();
        System.out.println("mesaj");
        waiter.addOrder(foodOrder2);
        waiter.sendOrders();
        System.out.println("test");
    }
}
