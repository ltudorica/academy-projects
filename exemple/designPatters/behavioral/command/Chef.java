package teme.exemple.designPatters.behavioral.command;

public class Chef {
    public void cookPizza() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Pizza is ready");
    }

    public void cookBurger() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Burger is ready");
    }
}
