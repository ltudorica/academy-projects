package teme.exemple.designPatters.behavioral.strategy;

public interface SeatAllocator { //strategy
    int allocateSeat(int capacity);
}
