package teme.exemple.designPatters.structuralPatterns.composite;

import java.util.ArrayList;
import java.util.List;

public class Manager implements Staff {
    private String name;
    private List<Staff> subordinates = new ArrayList<>();

    public Manager() {

    }

    public Manager(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void display() {
        System.out.println("Manager: " + name);
        for (var e : subordinates) {
            System.out.print("   ");
            e.display();
        }
    }

    @Override
    public void addStaff(Staff staff) {
        subordinates.add(staff);
    }

    @Override
    public void removeStaff(Staff staff) {
        subordinates.remove(staff); //ar trebui sa supradefinim hashcode si equals dupa nume sa fim siguri ca se sterge de unde treb
    }

    @Override
    public Staff getStaff(int index) {
        if (index >= 0 && index < subordinates.size()) {
            return subordinates.get(index);
        }
        throw new UnsupportedOperationException();
    }
}
