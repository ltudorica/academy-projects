package teme.exemple.designPatters.structuralPatterns.composite;

public class Main {
    public static void main(String[] args) {
        Employee e1 = new Employee("Vasile");
        Employee e2 = new Employee("Cristi");
        Manager e3 = new Manager("Viorel");
        Employee e4 = new Employee("Florin");

        Employee m1 = new Employee("Ionel");
        e3.addStaff(e2);
        e3.addStaff(m1);

        Manager m2 = new Manager("Marius");
        m2.addStaff(e4);

        Manager m3 = new Manager("Gigel");
        m3.addStaff(e1);
        m3.addStaff(e3);
        m3.addStaff(m2);

        m3.display();

        m3.removeStaff(e1);
        m3.display();
        System.out.println();
        m3.getStaff(1).display();

    }
}
