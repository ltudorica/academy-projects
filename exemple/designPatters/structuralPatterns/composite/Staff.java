package teme.exemple.designPatters.structuralPatterns.composite;

public interface Staff {
    // composite - poate sa fie cls abstracta sau interfata
    void display();

    void addStaff(Staff staff);

    void removeStaff(Staff staff);

    Staff getStaff(int index);

}
