package teme.exemple.designPatters.structuralPatterns.adapter;

import teme.exemple.designPatters.structuralPatterns.adapter.exceptions.OccupiedSeatException;
import teme.exemple.designPatters.structuralPatterns.adapter.newSolution.StadiumCovidTicketAllocator;
import teme.exemple.designPatters.structuralPatterns.adapter.oldSolution.TicketPicker;
// TicketPicker -> target
// StadiumCovidTicketAllocator -> adaptee

public class OldPickerToNewPickerClassAdapter extends StadiumCovidTicketAllocator implements TicketPicker {
    private int currentPlace = -1;


    public OldPickerToNewPickerClassAdapter(int capacity) {
        super(capacity);
    }


    @Override
    public void pickPlace(int place) throws OccupiedSeatException {
        int seat = super.allocateSeat();
        if (seat == -1) {
            throw new OccupiedSeatException();
        }

    }

}
