package teme.exemple.designPatters.structuralPatterns.adapter.oldSolution;

import teme.exemple.designPatters.structuralPatterns.adapter.exceptions.OccupiedSeatException;

public interface TicketPicker {
    //interfata veche (target)
    void pickPlace(int place) throws OccupiedSeatException;
}
