package teme.exemple.designPatters.structuralPatterns.adapter.oldSolution;

import teme.exemple.designPatters.structuralPatterns.adapter.exceptions.OccupiedSeatException;

public class StadiumTicketPicker implements TicketPicker {
    private int capacity;
    private boolean occupiedSeats[];

    public StadiumTicketPicker(int capacity) {
        this.capacity = capacity;
        occupiedSeats = new boolean[capacity];
    }

    @Override
    public void pickPlace(int place) throws OccupiedSeatException {
        if (occupiedSeats[place - 1]) {
            throw new OccupiedSeatException();
        } else {
            occupiedSeats[place - 1] = true;
            System.out.println("Seat " + place + " was allocated to you.");
        }
    }
}
