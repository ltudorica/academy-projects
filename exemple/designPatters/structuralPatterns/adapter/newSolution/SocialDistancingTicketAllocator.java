package teme.exemple.designPatters.structuralPatterns.adapter.newSolution;

public interface SocialDistancingTicketAllocator {

    int allocateSeat();
}
