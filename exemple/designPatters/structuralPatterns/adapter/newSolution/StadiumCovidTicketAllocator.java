package teme.exemple.designPatters.structuralPatterns.adapter.newSolution;

public class StadiumCovidTicketAllocator implements SocialDistancingTicketAllocator {
    // app noua - vreme de covid - adaptee
    private int capacity;
    private int latestGeneratedSeat = 1;

    public StadiumCovidTicketAllocator(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public int allocateSeat() {
        int allocatedSeats = latestGeneratedSeat + 2;
        if (allocatedSeats < capacity) {
            latestGeneratedSeat = allocatedSeats;
            System.out.println("Allocated seat: " + allocatedSeats);
            return allocatedSeats;
        } else {
            return -1;
        }
    }
}
