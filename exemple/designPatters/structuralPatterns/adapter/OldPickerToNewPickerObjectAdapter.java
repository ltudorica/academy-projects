package teme.exemple.designPatters.structuralPatterns.adapter;

import teme.exemple.designPatters.structuralPatterns.adapter.exceptions.OccupiedSeatException;
import teme.exemple.designPatters.structuralPatterns.adapter.newSolution.SocialDistancingTicketAllocator;
import teme.exemple.designPatters.structuralPatterns.adapter.oldSolution.TicketPicker;

public class OldPickerToNewPickerObjectAdapter implements TicketPicker {
    private SocialDistancingTicketAllocator socialDistancingTicketAllocator = null; //injectare clasa noua

    public OldPickerToNewPickerObjectAdapter(SocialDistancingTicketAllocator allocator) {
        this.socialDistancingTicketAllocator = allocator;
    }

    @Override
    public void pickPlace(int place) throws OccupiedSeatException {
        int seat = socialDistancingTicketAllocator.allocateSeat();
        if (seat == -1) {
            throw new OccupiedSeatException();
        }
    }
}