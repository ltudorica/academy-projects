package teme.exemple.designPatters.structuralPatterns.adapter;

import teme.exemple.designPatters.structuralPatterns.adapter.exceptions.OccupiedSeatException;
import teme.exemple.designPatters.structuralPatterns.adapter.newSolution.SocialDistancingTicketAllocator;
import teme.exemple.designPatters.structuralPatterns.adapter.newSolution.StadiumCovidTicketAllocator;
import teme.exemple.designPatters.structuralPatterns.adapter.oldSolution.StadiumTicketPicker;
import teme.exemple.designPatters.structuralPatterns.adapter.oldSolution.TicketPicker;

public class Main {
    //
    public static void main(String[] args) {
        SocialDistancingTicketAllocator ticketAllocator = new StadiumCovidTicketAllocator(1000);

        for (int i = 0; i < 3; i++) {
            ticketAllocator.allocateSeat();
        } // new need

        TicketPicker oldTicketPicker = new StadiumTicketPicker(1000);
        try {
            oldTicketPicker.pickPlace(5);
            oldTicketPicker.pickPlace(7);
            //oldTicketPicker.pickPlace(5);
        } catch (OccupiedSeatException e) {
            e.printStackTrace();
        } //old seat picker


        //Class adapter
        TicketPicker newTicketPicker = new OldPickerToNewPickerClassAdapter(1000);
        try {
            newTicketPicker.pickPlace(5);
            newTicketPicker.pickPlace(7);
            //oldTicketPicker.pickPlace(5);
        } catch (OccupiedSeatException e) {
            e.printStackTrace();
        } //adapter for class


        //Object adapter
        TicketPicker newTicketPickerV2 = new OldPickerToNewPickerObjectAdapter(ticketAllocator);
        try {
            newTicketPicker.pickPlace(5);
            newTicketPicker.pickPlace(7);
            //oldTicketPicker.pickPlace(5);
        } catch (OccupiedSeatException e) {
            e.printStackTrace();
        } //adapter for object

    }
}
