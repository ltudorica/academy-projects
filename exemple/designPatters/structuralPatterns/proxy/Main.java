package teme.exemple.designPatters.structuralPatterns.proxy;

public class Main {
    public static void main(String[] args) {

        AccessInterface door = new FrondDoor();
        System.out.println(door.isAccessGranted(456));

        FrontDoorProxy proxy = new FrontDoorProxy(door);
        proxy.setTemperature(39);
        door = proxy;
        System.out.println(door.isAccessGranted(456));

        door = new FrontDoorProxy(new FrondDoor(), 37);
        System.out.println(door.isAccessGranted(456));


    }
}
