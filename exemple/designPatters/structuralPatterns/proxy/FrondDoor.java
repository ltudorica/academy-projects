package teme.exemple.designPatters.structuralPatterns.proxy;

import java.util.Arrays;
import java.util.List;

public class FrondDoor implements AccessInterface {
    private List<Integer> accessCards = Arrays.asList(123, 456);

    @Override
    public boolean isAccessGranted(int id) {
        return accessCards.contains(id);
    }
}
