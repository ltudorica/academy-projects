package teme.exemple.designPatters.structuralPatterns.proxy;

public class FrontDoorProxy implements AccessInterface {
    private AccessInterface frondDoor;
    private int temperature;

    public FrontDoorProxy(AccessInterface frondDoor) {
        this.frondDoor = frondDoor;
    }

    public FrontDoorProxy(AccessInterface frondDoor, int temperature) {
        this.frondDoor = frondDoor;
        this.temperature = temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    @Override
    public boolean isAccessGranted(int id) {
        if (temperature < 35 || temperature > 38) {
            return false;
        } else {
            return frondDoor.isAccessGranted(id);
        }
    }
}
