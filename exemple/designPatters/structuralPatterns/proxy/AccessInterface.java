package teme.exemple.designPatters.structuralPatterns.proxy;

public interface AccessInterface {
    boolean isAccessGranted(int id);
}
