package teme.exemple.OOPWeek2Day1Day2.Virtualization;

import teme.exemple.OOPWeek2Day1Day2.Virtualization.Interfete.Payable;
import teme.exemple.OOPWeek2Day1Day2.Virtualization.Models.AbstractTicket;
import teme.exemple.OOPWeek2Day1Day2.Virtualization.Models.BusTicket;
import teme.exemple.OOPWeek2Day1Day2.Virtualization.Models.MovieTicket;
import teme.exemple.OOPWeek2Day1Day2.Virtualization.Models.SubwayTicket;

public class Main {
    public static void main(String[] args) {

        // use abstract classes and interfaces as reference type
        // contract between programmers
        AbstractTicket ticket = null;
        Payable payOption = null;

        BusTicket busTicket = new BusTicket();
        SubwayTicket subwayTicket = new SubwayTicket("Hi", 3, 2);

        BusTicket[] busTickets = new BusTicket[5];
        SubwayTicket[] subwayTickets = new SubwayTicket[5];

        busTickets[0] = new BusTicket();
        subwayTickets[0] = new SubwayTicket("M2", 3, 1);

        //upcasting
        ticket = busTicket;
        ticket = subwayTicket;

        AbstractTicket[] tickets = new AbstractTicket[3];

        tickets[0] = busTicket;
        tickets[1] = subwayTicket;
        tickets[2] = new BusTicket();

        //down-casting
        SubwayTicket subwayTicket1 = (SubwayTicket) tickets[1];

        for (AbstractTicket t : tickets) {
            if (t instanceof SubwayTicket) {
                SubwayTicket subwayTicket2 = (SubwayTicket) t;
                subwayTicket2.checkNoOfEntries();
            }
        }

        Payable[] payables = new Payable[4];
        payables[0] = busTicket;
        payables[1] = subwayTicket;
        payables[2] = new BusTicket();
        payables[3] = new MovieTicket();

        for (Payable p : payables) {
            p.pay();
        }

    }
}
