package teme.exemple.OOPWeek2Day1Day2.Virtualization.Interfete;

public interface Payable {
    public void pay();

    void cancelPayment();

    public default void printInfo() {
        System.out.println("This is a payment option");
    }
}
