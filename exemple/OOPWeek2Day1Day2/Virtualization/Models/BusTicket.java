package teme.exemple.OOPWeek2Day1Day2.Virtualization.Models;

import teme.exemple.OOPWeek2Day1Day2.Virtualization.Interfete.Payable;

public class BusTicket extends AbstractTicket implements Payable {

    public BusTicket() {
        super("Bus Ticket", 3);
    }

    @Override
    public void pay() {
        System.out.println("Pay bus ticket");
    }

    @Override
    public void cancelPayment() {
        System.out.println("Cancel bus ticket");
    }

    @Override
    public void getDescription() {
        System.out.println("A bus ticket");
    }
}
