package teme.exemple.OOPWeek2Day1Day2.Virtualization.Models;

import teme.exemple.OOPWeek2Day1Day2.Virtualization.Interfete.Payable;

public class SubwayTicket extends AbstractTicket implements Payable {

    int noOfEntries;

    public SubwayTicket(String name, float price, int noOfEntries) {
        super(name, price);
        this.noOfEntries = noOfEntries;
    }

    @Override
    public void getDescription() {
        System.out.println("A subway ticket");
    }

    @Override
    public void pay() {
        System.out.println("Pay subway ticket");
    }

    @Override
    public void cancelPayment() {
        System.out.println("Cancel subway ticket");
    }

    public void checkNoOfEntries() {
        System.out.println("Number of entries: " + this.noOfEntries);
    }

}
