package teme.exemple.OOPWeek2Day1Day2.Virtualization.Models;

public abstract class AbstractTicket {

    protected String name;
    protected float basePrice;

    public AbstractTicket(String name, float basePrice) {
        this.name = name;
        this.basePrice = basePrice;
        System.out.println("Calling AbstractTicket constructor");
    }

    public String getName() {
        return this.name;
    }

    public abstract void getDescription();


}
