package teme.exemple.OOPWeek2Day1Day2.Virtualization.Models;

import teme.exemple.OOPWeek2Day1Day2.Virtualization.Interfete.Payable;

public class MovieTicket implements Payable {

    private String movieName;

    @Override
    public void pay() {
        System.out.println("Pay for a movie ticket");
    }

    @Override
    public void cancelPayment() {
        System.out.println("Cancel payment for movie ticket");
    }
}
