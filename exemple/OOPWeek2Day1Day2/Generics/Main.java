package teme.exemple.OOPWeek2Day1Day2.Generics;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Player player = new Player(5, 8.5, "PlayerOne");
        Player player2 = new Player(100, "Unlimited", "Player2");

        Frigider frigider = new Frigider();
        Floare floare = new Floare();

        //Box cutieFrigider = new Box(frigider);
        Box cutieFrigider = new Box(floare);
        //Frigider frigiderPrimit = (Frigider) cutieFrigider.getContent();

        GenericBox<Frigider> cutieGenericaFrigider = new GenericBox<Frigider>(frigider);

        //collections
        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<Floare> flowers = new ArrayList<>();

        numbers.add(5);
        numbers.add(5);
        numbers.add(10);
        numbers.add(7);

        flowers.add(new Floare());
        flowers.add(new Floare());
        flowers.add(new Floare());

        for (int number : numbers) {
            System.out.print(number + " ");
        }

        System.out.println();
        for (Floare flower : flowers) {
            System.out.println(flower);
        }

        Set<Integer> uniqueNumbers = new HashSet<>();
        uniqueNumbers.add(5);
        uniqueNumbers.add(5);
        uniqueNumbers.add(10);
        uniqueNumbers.add(7);

        for (int number : uniqueNumbers) {
            System.out.println(number);
        }

        Set<Floare> uniqueFlowers = new TreeSet<>();
        uniqueFlowers.add(new Floare());
        uniqueFlowers.add(floare);
        uniqueFlowers.add(floare);

        for (Floare flower : uniqueFlowers) {
            System.out.println(flower);
        }

        Map<Integer, String> players = new HashMap<>();
        players.put(100, "John");
        players.put(25, "Alice");
        players.put(99, "Vader");

        String playerName = players.get(25);
        if (playerName != null) {
            System.out.println("Player name = " + playerName);
        } else {
            System.out.println("Player does not exist");
        }

    }
}
