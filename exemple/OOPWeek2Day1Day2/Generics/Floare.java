package teme.exemple.OOPWeek2Day1Day2.Generics;


public class Floare implements Comparable {

    int id;

    @Override
    public int compareTo(Object o) {
        Floare floare = (Floare) o;
        if (this.id == floare.id) {
            return 0;
        } else if (this.id < floare.id) {
            return -1;
        }
        return 1;
    }
}
