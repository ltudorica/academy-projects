package teme.exemple.OOPWeek2Day1Day2.Generics;

public class Player<T extends Number, TipGeneric> {
    T points;
    TipGeneric credits;
    String name;


    public Player(T points, TipGeneric credits, String name) {
        this.points = points;
        this.credits = credits;
        this.name = name;
    }

    public T getPoint() {
        return this.points;
    }
}
