package teme.exemple.OOPWeek2Day1Day2.Generics;

public class GenericBox<T> {
    T content;

    public GenericBox(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }
}
