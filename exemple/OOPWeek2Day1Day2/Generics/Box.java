package teme.exemple.OOPWeek2Day1Day2.Generics;

public class Box {
    Object content;

    public Box(Object content) {
        this.content = content;
    }

    public Object getContent() {
        return content;
    }
}
