package teme.exemple.OOPWeek2Day1Day2.Debugging;

public class Main {

    public static int functie1(int vb) {
        vb += 1;
        return vb;
    }

    public static int functie2(int vb) {
        vb *= 20;
        vb = functie1(vb);
        if (vb > 0) {
            throw new UnsupportedOperationException();
        }
        return vb;
    }

    public static int functie3(int vb) {
        vb -= 5;
        vb = functie2(vb);
        return vb;
    }

    public static int functie4(int vb) {
        vb = 10;
        vb = functie3(vb);
        return vb;
    }

    public static void main(String[] args) {

        int valoare = 0;
        valoare = functie4(valoare);
        System.out.println("Rezultat final = " + valoare);
    }
}
