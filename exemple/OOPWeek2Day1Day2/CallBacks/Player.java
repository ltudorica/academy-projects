package teme.exemple.OOPWeek2Day1Day2.CallBacks;

import java.util.ArrayList;

public class Player {
    String name;
    ArrayList<Integer> hoursPlayedDaily = new ArrayList<>();
    Computable computable = null;

    public Player(String name, ArrayList<Integer> hours) {
        this.name = name;
        this.hoursPlayedDaily = (ArrayList<Integer>) hours.clone();
    }

    public void addNewPlaySession(int hours) {
        this.hoursPlayedDaily.add(hours);
    }

    public void setComputable(Computable c) {  // nu treb clonat, ne intereseaza doaor comportamentul
        this.computable = c;
    }

    public double compute() throws UnsupportedOperationException {
        if (this.computable != null) {
            return this.computable.compute((ArrayList<Integer>) this.hoursPlayedDaily.clone());
        }
        throw new UnsupportedOperationException();
    }

    public double compute(Computable computable) throws UnsupportedOperationException { // operatie atomica
        if (computable != null) {
            return computable.compute(this.hoursPlayedDaily);
        }

        throw new UnsupportedOperationException();
    }

}
