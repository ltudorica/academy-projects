package teme.exemple.OOPWeek2Day1Day2.CallBacks;

import java.util.ArrayList;

public class SumOp implements Computable {

    @Override
    public double compute(ArrayList<Integer> values) {
        double sum = 0;
        for (int v : values) {
            sum += v;
        }

        return sum;
    }
}
