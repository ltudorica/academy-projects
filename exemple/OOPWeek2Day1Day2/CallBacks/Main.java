package teme.exemple.OOPWeek2Day1Day2.CallBacks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {

        List<Integer> hours = Arrays.asList(10, 20, 30, 40, 50);
        ArrayList<Integer> hoursPlayed = new ArrayList<>();

        for (int hour : hours) {
            hoursPlayed.add(hour);
        }

        Player player1 = new Player("John", hoursPlayed);
        player1.addNewPlaySession(60);

        player1.setComputable(new SumOp()); // setam "prietenul"(metoda pe care o vrem)
        double totalHours = player1.compute();
        System.out.println("Result: " + totalHours);

        totalHours = player1.compute(new SumOp());
        System.out.println("Result atomic version: " + totalHours);

        double maxHours = player1.compute(new Computable() { //cls anonima
            @Override
            public double compute(ArrayList<Integer> values) {
                if (values.size() == 0) {
                    return 0;
                }

                double max = values.get(0);
                for (int hours : values) {
                    max = max < hours ? hours : max;

                }
                return max;
            }
        });

        System.out.println("Max hours: " + maxHours);

        InterfataSuma functieSuma = (int a, int b) -> {
            return a + b;
        }; // expresie lambda
        InterfataSuma functieSuma3 = (int a, int b) -> a + b;  // mai simplu
        InterfataSuma functieSuma4 = (a, b) -> a + b;   // cel mai simplu


        InterfataSuma functieSuma2 = new InterfataSuma() {  // varianta lunga
            @Override
            public int suma(int a, int b) {
                return a + b;
            }
        };

        operatieGenerica(2, 3, functieSuma2);
        operatieGenerica(2, 3, functieSuma3);
        int bla = operatieGenerica(2, 3, (a, b) -> a - b);
        System.out.println("test" + bla);
        bla += 10;
        System.out.println("test" + bla);

        InterfataPrintare print = (x) -> System.out.println("Valoarea este: " + x);
        print.actiune(23);

        Consumer<Integer> print2 = (x) -> System.out.println("Valoarea este: " + x); //interfate implementate de java direct
        print2.accept(25);


    }

    public static int aduna(int a, int b) {
        return a + b;
    }

    public static int operatieGenerica(int a, int b, InterfataSuma operatie) {
        return operatie.suma(a, b);
    }
}
