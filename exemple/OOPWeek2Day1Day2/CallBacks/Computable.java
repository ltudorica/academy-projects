package teme.exemple.OOPWeek2Day1Day2.CallBacks;

import java.util.ArrayList;

public interface Computable {

    public double compute(ArrayList<Integer> values);
}
