package teme.exemple.OOPWeek2Day1Day2.TranferParametri;

public class Main {
    public static void interschimbare(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }

    public static void interschimbare2(Integer a, Integer b) { // nu e ok
        int temp = a;
        a = b; // nu se copiaza val, se copiaza adresele de ref (pt obiecte cu egal), iar Integer e cls inmutable
        b = temp;
    }

    public static void interschimbare3(MyInteger a, MyInteger b) {
        int temp = a.getValoare();
        a.setValoare(b.getValoare());
        b.setValoare(temp);
    }

    public static void main(String[] args) {

        int vb1 = 10;
        int vb2 = 20;
        Integer vb3 = 30;

        System.out.println(String.format("vb1 = %d, vb2 = %d", vb1, vb2)); // acela'si cu system out printf - inlocuit % cu val

        int temp = vb1;
        vb1 = vb2;
        vb2 = temp;

        System.out.println(String.format("vb1 = %d, vb2 = %d", vb1, vb2));

        interschimbare(vb1, vb2);
        System.out.println(String.format("vb1 = %d, vb2 = %d", vb1, vb2));

        Integer VB1 = vb1;
        Integer VB2 = vb2;
        interschimbare2(VB1, VB2);

        System.out.println(String.format("VB1 = %d, VB2 = %d", VB1, VB2));
        System.out.println();

        MyInteger mVb1 = new MyInteger(vb1);
        MyInteger mVb2 = new MyInteger(vb2);

        interschimbare3(mVb1, mVb2);
        System.out.println(String.format("version 3 vb1 = %d, vb2 = %d", mVb1.getValoare(), mVb2.getValoare()));

        vb1 = mVb1.getValoare();
        vb2 = mVb2.getValoare();
    }
}
