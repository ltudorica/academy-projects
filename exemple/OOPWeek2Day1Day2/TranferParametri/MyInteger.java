package teme.exemple.OOPWeek2Day1Day2.TranferParametri;

public class MyInteger {
    int valoare;

    public MyInteger(int valoare) {
        this.valoare = valoare;
    }

    public int getValoare() {
        return this.valoare;
    }

    public void setValoare(int valoare) {
        this.valoare = valoare;
    }
}
