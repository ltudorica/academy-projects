package teme.exemple.OOPWeek2Day1Day2.StringPool;

public class StringPool {
    public static void main(String[] args) {
        // Teste Java String Constant Pool

        //String player1 = "John";
        String player2 = "John";
        String player1 = new String("John");

        if (player1 == player2) {
            System.out.println("sunt egale");
        } else {
            System.out.println("sunt diferite");
        }

        if (player1.equals(player2)) {
            System.out.println("sunt egale");
        } else {
            System.out.println("sunt diferite");
        }

        String player3 = new String("John");
        player3 = player3.intern();

        if (player3 == player2) {
            System.out.println("sunt egale referintele");
        } else {
            System.out.println("sunt diferite referintele");
        }

        Integer vb1 = -128;
        //Integer vb2 = new Integer(23);
        Integer vb2 = -128;

        // Integer Pool -128 pana la 127 se stocheaza in a acest spatiu
        if (vb1 == vb2) { // nu e ok - treb fol .equals pt clase (wrapper Integer)
            System.out.println("Cele 2 ref. sunt egale");
        } else {
            System.out.println("Cele 2 ref. sunt dif");
        }


    }
}
