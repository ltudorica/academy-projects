package teme.exemple.OOPWeek2Day1Day2.ShallowDeepCopy;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {

        Player player1 = new Player("John", 21);
        Player player2 = new Player("Alice", 20);

        player1 = player2;

        System.out.println(String.format("Name = %s, Age = %d", player1.getName(), player1.getAge()));

        player2.setAge(25);
        System.out.println(String.format("Name = %s, Age = %d", player1.getName(), player1.getAge()));

        PublicProfile profile = new PublicProfile("PlayOne", "player.jpg");

        Player player3 = new Player("Bob", 21, profile);
        Player player4 = new Player("Vader", 23, profile);


        System.out.println("Bob: " + player3.getProfile().publicName);
        player4.setName("Vader");
        System.out.println("Bob: " + player3.getProfile().publicName);

        player3 = (Player) player4.clone();
        player4.getProfile().setPublicName("Luke");

        System.out.println("Player 3: " + player3.getProfile().getPublicName());
    }
}
