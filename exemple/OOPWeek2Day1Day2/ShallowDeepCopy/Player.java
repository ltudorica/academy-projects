package teme.exemple.OOPWeek2Day1Day2.ShallowDeepCopy;

public class Player implements Cloneable {
    String name;
    int age;
    PublicProfile profile;

    public Player(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Player(String name, int age, PublicProfile profile) {
        this.name = name;
        this.age = age;
        //this.profile = profile -> shallow copy - wrong
        this.profile = profile.clone();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Player(this.name, this.age, this.profile);
    }

    public PublicProfile getProfile() {
        return this.profile.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
