package teme.exemple.OOPWeek2Day1Day2.ShallowDeepCopy;

public class PublicProfile implements Cloneable {
    String publicName;
    String avatarImage;

    public PublicProfile(String publicName, String avatarImage) {
        this.publicName = publicName;
        this.avatarImage = avatarImage;
    }

    @Override
    public PublicProfile clone() {
        return new PublicProfile(this.publicName, this.avatarImage);
    }

    public String getPublicName() {
        return publicName;
    }

    public void setPublicName(String publicName) {
        this.publicName = publicName;
    }

    public String getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }
}
