package teme.exemple.OOPWeek2Day1Day2.Framework;

import teme.exemple.OOPWeek2Day1Day2.Framework.modele.Game;
import teme.exemple.OOPWeek2Day1Day2.Framework.modele.GameWithPlayers;

public class Main {
    public static void main(String[] args) {
        //Game game = new Game();
        Game game = new Game("StarWars", 0.1f);
        System.out.println("Version: " + game.getVersion());

        game.printDescription();
        System.out.println();

        GameWithPlayers superGame = new GameWithPlayers("Star 2", 1.2f, 50);
        System.out.println("Game name " + superGame.getName());
        System.out.println("Game version " + superGame.getVersion());
        System.out.println();
        superGame.printDescription();


    }
}
