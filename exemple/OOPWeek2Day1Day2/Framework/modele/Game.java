package teme.exemple.OOPWeek2Day1Day2.Framework.modele;

public class Game implements InterfaceVersion {
    protected String name;
    protected float version;

    public Game(String name, float version) {
        this(name);
        this.setVersion(version);
        System.out.println("Apel constructor " + Game.class);
    }

    public Game(String name) {
        this.name = name;
    }

    private Game() {
    }

    public String getName() {
        return name;
    }

    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        if (version > 0) {
            this.version = version;
        } else {
            throw new UnsupportedOperationException();
        }

    }

    public void printDescription() {
        System.out.println(String.format("The %s game has version %f", this.name, this.version));
    }
}
