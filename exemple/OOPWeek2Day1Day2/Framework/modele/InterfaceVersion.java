package teme.exemple.OOPWeek2Day1Day2.Framework.modele;

public interface InterfaceVersion {

    public float getVersion();

    public void setVersion(float version);
}
