package teme.exemple.OOPWeek2Day1Day2.Framework.modele;

public class GameWithPlayers extends Game {

    int maxPlayers;

    public GameWithPlayers() {
        super("Game With Players", 1);
    }

    public GameWithPlayers(int maxPlayers) {
        super("Game With Players", 1);
        this.maxPlayers = maxPlayers;
    }

    public GameWithPlayers(String name, float version, int maxPlayers) {
        super(name, version);
        this.maxPlayers = maxPlayers;
        System.out.println("Apel constructor din " + GameWithPlayers.class);
    }

    @Override
    public void printDescription() {
        super.printDescription();
        System.out.println(String.format("The %s game has version %f with %d max players", this.name, this.version, this.maxPlayers));
    }
}
