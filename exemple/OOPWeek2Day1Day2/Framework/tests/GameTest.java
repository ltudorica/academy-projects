package teme.exemple.OOPWeek2Day1Day2.Framework.tests;

import teme.exemple.OOPWeek2Day1Day2.Framework.modele.Game;
import org.junit.Test;


import static org.junit.Assert.*;

public class GameTest {

    @Test
    public void testSetVersionCorrectValue() {
        Game game = new Game("Test");
        float newVersion = 1;
        game.setVersion(newVersion);

        assertEquals(newVersion, game.getVersion(), 0);

    }
}