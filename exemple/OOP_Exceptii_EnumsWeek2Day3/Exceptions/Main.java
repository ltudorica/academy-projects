package teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Exceptions;

import teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Exceptions.Exceptii.AgeIllegalValueException;

public class Main {
    public static void main(String[] args) {


        try {

            Player player = new Player("John", 21);
            System.out.println("Avem un player");

            //Player player2 = new Player(null, 21);
            Player player3 = new Player("Alice", -21);

            System.out.println("Sfarsit try");
        } catch (NullPointerException ex) {
            System.out.println("Am folosit null");
        } catch (AgeIllegalValueException e) {
            System.out.println("Invalid value for age " + e.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {

            System.out.println("The end !");
        }
    }
}
