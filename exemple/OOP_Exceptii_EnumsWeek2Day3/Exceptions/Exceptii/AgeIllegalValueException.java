package teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Exceptions.Exceptii;

public class AgeIllegalValueException extends Exception {

    public AgeIllegalValueException() {

    }

    public AgeIllegalValueException(String msg) {
        super(msg);
    }
}
