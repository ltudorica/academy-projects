package teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Exceptions;

import teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Exceptions.Exceptii.AgeIllegalValueException;

public class Player {
    String name;
    int age;

    public Player(String name, int age) throws AgeIllegalValueException {
        this.setName(name);
        this.setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws NullPointerException, IllegalArgumentException {
        if (name == null) {
            throw new NullPointerException("Received name is null!");
        }

        if (name.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws AgeIllegalValueException {
        if (age < 5 || age > 100) {
            throw new AgeIllegalValueException();
        }
        this.age = age;
    }


}
