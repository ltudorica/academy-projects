package teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Enums;

public enum PlayerCategory {
    BEGINNER(5, 100, "New player", 1),
    AVERAGE(10, 300, "New Player", 2),
    ADVANCED(15, 500, "Advanced", 3),
    PRO(25, 1000, "The Pro", 4);

    private float bonus;
    private int minHours;
    private int id;
    private String label;

    private PlayerCategory(float bonus, int minHours, String label, int id) {
        this.bonus = bonus;
        this.minHours = minHours;
        this.label = label;
        this.id = id;
    }

    public static PlayerCategory valueOf(int id) {
        for (PlayerCategory category : values()) {
            if (category.id == id) {
                return category;
            }
        }
        return BEGINNER;
    }

    public float getBonus() {
        return this.bonus;
    }

    public String getLabel() {
        return this.label;
    }
}
