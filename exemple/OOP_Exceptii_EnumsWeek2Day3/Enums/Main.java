package teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Enums;


public class Main {
    public static void main(String[] args) {

        Player player = new Player();
        player.setCategory(PlayerCategory.PRO);

        System.out.println("Player category " + player.getCategory());
        player.addBonus();

        PlayerCategory category = PlayerCategory.valueOf("PRO"); // pt a valida din interfata string in enum
        System.out.println(category.name());

        PlayerCategory category1 = PlayerCategory.valueOf(2);  // pt a valida din interfata int in enum
        System.out.println(category1);
    }
}
