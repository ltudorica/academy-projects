package teme.exemple.OOP_Exceptii_EnumsWeek2Day3.Enums;

public class Player {

    String name;
    int age;
    PlayerCategory category;

    public void setCategory(PlayerCategory category) {
        this.category = category;
    }

    public PlayerCategory getCategory() {
        return category;
    }

    public void addBonus() {
        System.out.println("Bonus: " + this.category.getBonus());
//        switch (this.category) { //poate fi scrisa frumos in enum
//                System.out.println("Bonus 5%");
//                break;
//            case AVERAGE:
//                System.out.println("Bonus 10%");
//                break;
//            case PRO:
//                System.out.println("Bonus 15%");
//                break;
//            default:
//                System.out.println("No Bonus!");
//      }

    }

}
