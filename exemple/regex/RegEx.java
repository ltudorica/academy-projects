package teme.exemple.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("playtika", Pattern.CASE_INSENSITIVE);
        String str = "I am at Playtika";
        Matcher matcher = pattern.matcher(str);

        boolean find = matcher.find();
        System.out.println(find);


        String line = "The order was placed for QT3000! OK?";
        String matchingString = "(.*)(\\d+)(.*)"; //capturing groups

        Pattern pattern2 = Pattern.compile(matchingString);
        Matcher matcher2 = pattern2.matcher(line);
        System.out.println();
        int k = 0;

        if (matcher2.find()) {
            System.out.println(matcher2.group(0));
            System.out.println(matcher2.group(1));
            System.out.println(matcher2.group(2));
            System.out.println(matcher2.group(3));
        }
    }
}
