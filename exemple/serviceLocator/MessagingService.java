package teme.exemple.serviceLocator;

public interface MessagingService {
    String getMessageBody();

    String getServiceName();
}
