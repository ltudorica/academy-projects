package teme.exemple.serviceLocator;

public class SMSService implements MessagingService {
    @Override
    public String getMessageBody() {
        return "sms message";
    }

    @Override
    public String getServiceName() {
        return "SMS service";
    }
}
