package teme.exemple.serviceLocator;

public class Main {
    public static void main(String[] args) {
        MessagingService service = ServiceLocator.getService("EmailService");
        String email = service.getMessageBody();

        MessagingService smsService = ServiceLocator.getService("SMSService");
        String sms = smsService.getMessageBody();

        MessagingService emailService = ServiceLocator.getService("EmailService");
        String newEmail = smsService.getMessageBody();
    }
}
