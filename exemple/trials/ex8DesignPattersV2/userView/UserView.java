package teme.exemple.trials.ex8DesignPattersV2.userView;

public class UserView {

    /// aici pot sa bag mai multe metode - una sa mi returneze un strig cu userul (sau userul) si una sa imi
    // apeleze notify (observer) dupa enter - ca sa notifice controllerul ca s-a terminat treaba si sa reseteze userul
    // si vu ajutorul metodei asteia pot sa fac si un command sa decuplez view de controller
    // to update cu scanner


    public void login(String email, String password) {
        System.out.println("Email: " + email);
        System.out.println("Password: " + password);
        System.out.println("Press enter to login ");
    }
}
