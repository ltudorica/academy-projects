package teme.exemple.trials.ex8DesignPattersV2.userController;

import teme.exemple.trials.ex8DesignPattersV2.InMemoryRepo;
import teme.exemple.trials.ex8DesignPattersV2.Repository;
import teme.exemple.trials.ex8DesignPattersV2.userModel.User;
import teme.exemple.trials.ex8DesignPattersV2.exceptions.IncorrectPasswordException;
import teme.exemple.trials.ex8DesignPattersV2.exceptions.WrongEmailException;
import teme.exemple.trials.ex8DesignPattersV2.userView.UserView;

import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserController {
    private User user;
    private UserView userView;
    private Repository repo;
    private static final Scanner sc = new Scanner(System.in);
    private static final int MIN_PASSWORD_LENGTH = 5;

    public UserController() {
        this.user = new User();
        this.userView = new UserView();
        this.repo = new InMemoryRepo<>();
    }

    public User getUser() {
        return this.user;
    }

    public void setUserEmail() throws WrongEmailException {
        System.out.println("Email: ");
        String email = sc.nextLine();
        if (isEmailValid(email)) {
            user.setEmail(email);
        } else {
            throw new WrongEmailException();
        }
    }

    public String getEmail() {
        return user.getEmail();
    }

    public void setUserPassword() throws IncorrectPasswordException {
        System.out.println("Password: ");
        String pass = sc.nextLine();
        if (isPasswordLengthMinLength(pass)) {
            user.setPassword(pass);
        } else {
            throw new IncorrectPasswordException();
        }
    }

    public String getUserPassword() {
        return user.getPassword();
    }

    public void setUserLastLoggedInTime() {
        user.setLastLoggedIn(LocalDateTime.now());
    }

    public LocalDateTime getUserLastLoggedInTime() {
        return user.getLastLoggedIn();
    }

    public Repository<User> getRepo() {
        return this.repo;
    }

    public boolean isEmailValid(String email) {
        Pattern p = Pattern.compile("^(.+)@(.+)$");
        Matcher m = p.matcher(email);
        return m.find();
    }

    public boolean isPasswordLengthMinLength(String pass) {
        return pass.length() >= MIN_PASSWORD_LENGTH;
    }

    public boolean login() {
        threadSleep();
        try {
            setUserEmail();
            setUserPassword();
        } catch (WrongEmailException e) {
            System.out.println("The email is not valid!");
            e.printStackTrace();
            return false;
        } catch (IncorrectPasswordException e) {
            System.out.println("The password should contain minimum " + MIN_PASSWORD_LENGTH + " characters! ");
            e.printStackTrace();
            return false;
        }
        setUserLastLoggedInTime();
        userViewLogin();
        insertUserInRepo();
//        resetUser();
        return true;
    }

    private void threadSleep() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void userViewLogin() {
        userView.login(user.getEmail(), user.getPassword());
        sc.nextLine();
    }

    private void insertUserInRepo() {
        repo.insert(user);
    }

}
