package teme.exemple.trials.ex8DesignPattersV2;

import teme.exemple.trials.ex8DesignPattersV2.userController.UserController;
import teme.exemple.trials.ex8DesignPattersV2.userModel.User;

public class Main {
    public static void main(String[] args) {
        UserController userController = new UserController();
        userController.login();
        userController.login();
        userController.login();

        Repository<User> users = userController.getRepo();
        System.out.println(users.getAll());
        System.out.println(userController.getUser());

    }
}
