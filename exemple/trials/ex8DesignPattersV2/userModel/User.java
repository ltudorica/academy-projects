package teme.exemple.trials.ex8DesignPattersV2.userModel;

import java.time.LocalDateTime;

public class User {
    private String email;
    private String password;
    private LocalDateTime lastLoggedIn;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getLastLoggedIn() {
        return lastLoggedIn;
    }

    public void setLastLoggedIn(LocalDateTime lastLoggedIn) {
        this.lastLoggedIn = lastLoggedIn;
    }

    @Override
    public String toString() {
        return "User email: " + this.email + ", password: " + this.password +
                ", last logged in at " + this.lastLoggedIn;
    }
}
