package teme.exemple.trials.ex8DesignPattersV2;

public interface ObserverUser {
    void resetUser();
}
