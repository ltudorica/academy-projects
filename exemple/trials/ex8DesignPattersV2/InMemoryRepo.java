package teme.exemple.trials.ex8DesignPattersV2;

import java.util.List;
import java.util.Vector;

public class InMemoryRepo<T> implements Repository<T> {
    List<T> users = new Vector<>();

    @Override
    public void insert(T entry) {
        users.add(entry);
    }

    @Override
    public void delete(T entry) {
        users.remove(entry);
    }

    @Override
    public void update(T oldEntry, T newEntry) {
        users.set(users.indexOf(oldEntry), newEntry);
    }

    @Override
    public List<T> getAll() {
        List<T> result = new Vector<>();
        for (T t : users) {
            result.add(t);
        }
        return result;
    }
}
