package teme.exemple.trials.ex8DesignPattersV2;

import java.util.List;

public interface Repository<T> {
    void insert(T entry);

    void delete(T entry);

    void update(T oldEntry, T newEntry);

    List<T> getAll();
}
