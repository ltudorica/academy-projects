package teme.exemple.trials.challange2InClassBingo.config;

import teme.exemple.trials.challange2InClassBingo.exceptions.NoAvailablePlayersException;
import teme.exemple.trials.challange2InClassBingo.exceptions.NoGameSettingsException;
import teme.exemple.trials.challange2InClassBingo.models.BingoNumber;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GameSettings implements Cloneable {
    public static int NO_CARDS;
    private int noPlayers;
    private List<String> playerNames = new ArrayList<>();
    private List<BingoNumber> forbiddenCombinations = new ArrayList<>();

    public GameSettings(String settingsFileName) throws IOException, NoAvailablePlayersException {
        File settingsFile = new File(settingsFileName);
        if (!settingsFile.exists()) {
            throw new NoGameSettingsException();
        }
        FileReader reader = new FileReader(settingsFileName);

        try (BufferedReader bufferedReader = new BufferedReader(reader)) {
            String buffer = "";
            buffer = bufferedReader.readLine();
            GameSettings.NO_CARDS = Integer.parseInt(buffer);
            //noPlayers
            buffer = bufferedReader.readLine();
            this.noPlayers = Integer.parseInt(buffer);

            if (this.noPlayers == 0) {
                throw new NoAvailablePlayersException();
            }

            for (int i = 0; i < this.noPlayers; i++) {
                buffer = bufferedReader.readLine();
                this.playerNames.add(buffer);
            }

            //bingo combinations
            while ((buffer = bufferedReader.readLine()) != null) {
                String[] bingoLine = buffer.split(",");
                String column = bingoLine[0];
//                List<Integer> numbers = new ArrayList<>();
                for (int i = 1; i < bingoLine.length; i++) {
//                    numbers.add(Integer.parseInt(bingoLine[i]));
                    BingoNumber bingoNumber = new BingoNumber(column, Integer.parseInt(bingoLine[i]));
                    if (bingoNumber.isValidCombination(column, Integer.parseInt(bingoLine[i]))) {
                        this.forbiddenCombinations.add(bingoNumber);
                    }
                }
            }
        }

    }

    public static int getNoCards() {
        return NO_CARDS;
    }

    public int getNoPlayers() {
        return noPlayers;
    }

    public List<String> getPlayerNames() {
        return (List<String>) ((ArrayList<String>) playerNames).clone();
    }

    private GameSettings() {

    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        GameSettings gameSettings = new GameSettings();
        gameSettings.noPlayers = this.noPlayers;
        gameSettings.playerNames = this.getPlayerNames();
        gameSettings.forbiddenCombinations =
                (List<BingoNumber>) ((ArrayList<BingoNumber>) this.forbiddenCombinations).clone();
        return gameSettings;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(
                String.format("NO CARDS = %d, No Players = %d\n", NO_CARDS, this.noPlayers));
        sb.append(this.playerNames.toString());
        sb.append("\n");
        for (BingoNumber bingoNumber : this.forbiddenCombinations) {
            sb.append(bingoNumber.print()).append("\n");
        }
        return sb.toString();
    }

    //TODO check -> print & toString here & in BingoNumber
}
