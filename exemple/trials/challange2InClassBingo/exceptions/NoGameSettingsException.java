package teme.exemple.trials.challange2InClassBingo.exceptions;

import java.io.FileNotFoundException;

public class NoGameSettingsException extends FileNotFoundException {
}
