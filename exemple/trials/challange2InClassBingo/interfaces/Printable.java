package teme.exemple.trials.challange2InClassBingo.interfaces;

@FunctionalInterface
public interface Printable {
    String print();
}
