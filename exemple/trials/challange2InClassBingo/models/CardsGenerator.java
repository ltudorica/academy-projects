package teme.exemple.trials.challange2InClassBingo.models;

import java.util.ArrayList;
import java.util.Random;

public class CardsGenerator {

    public static ArrayList<BingoNumber> generateCard() {
        ArrayList<BingoNumber> card = new ArrayList<>();
        for (BingoColumn column : BingoColumn.values()) {
            for (int i = 0; i < column.getNoNumbers(); i++) {
                Random random = new Random();
                int number = random.nextInt(15) + 1;
                card.add(new BingoNumber(column.toString(), number + column.getLow() - 1));
            }
        }
        return card;
    }
}
