package teme.exemple.trials.challange2InClassBingo.models;

public enum BingoColumn {
    B(1, 15, 5),
    I(16, 30, 5),
    N(31, 45, 4),
    G(46, 60, 5),
    O(61, 75, 5);

    private int low;
    private int high;
    private int noNumbers;

    private BingoColumn(int low, int high, int noNumbers) {
        this.low = low;
        this.high = high;
        this.noNumbers = noNumbers;
    }

    public int getLow() {
        return low;
    }

    public int getHigh() {
        return high;
    }

    public int getNoNumbers() {
        return noNumbers;
    }

    public boolean isValid(int number) {
        if (number < this.low || number > this.high) {
            return false;
        }
        return true;
    }
}
