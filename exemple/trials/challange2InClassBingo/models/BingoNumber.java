package teme.exemple.trials.challange2InClassBingo.models;

import teme.exemple.trials.challange2InClassBingo.interfaces.Printable;

public class BingoNumber implements Printable, Cloneable {
    private String column;
    private int number;

    public BingoNumber(String column, int number) {
//        if(!isValidCombination(column, number)) {
//            throw new IllegalArgumentException();
//        }
        this.column = column;
        this.number = number;
    }

    public String getColumn() {
        return column;
    }

    public int getNumber() {
        return number;
    }

    public boolean isValidCombination(String column, int number) {
        try {
            BingoColumn bingoColumn = BingoColumn.valueOf(column.toUpperCase());
//            if(bingoColumn == null) {
//                return false;
//            }
            return bingoColumn.isValid(number);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new BingoNumber(column, number);
    }

    @Override
    public String print() {
        return String.format("%s - %d", this.column, this.number);
    }
}
