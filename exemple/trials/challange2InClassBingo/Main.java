package teme.exemple.trials.challange2InClassBingo;

import teme.exemple.trials.challange2InClassBingo.config.GameSettings;
import teme.exemple.trials.challange2InClassBingo.exceptions.NoAvailablePlayersException;
import teme.exemple.trials.challange2InClassBingo.exceptions.NoGameSettingsException;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws NoGameSettingsException {

        String settingsFileName = "";
        if (args.length != 0) {
            settingsFileName = args[0];
        } else {
            settingsFileName = "settings.txt";
        }

        File settingsFile = new File(settingsFileName);
        if (!settingsFile.exists()) {
            throw new NoGameSettingsException();
        }

        try {
            GameSettings gameSettings = new GameSettings(settingsFileName);
            System.out.println(gameSettings);
        } catch (NoGameSettingsException e) {
            System.out.println("No settings file");
        } catch (IOException e) {
            System.out.println("File issues");
        } catch (NoAvailablePlayersException e) {
            System.out.println("No Players");
        }
        System.out.println("The end");
    }
}
