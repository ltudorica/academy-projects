package teme.exemple.trials.designPatterns.command;

public interface TextFileOperationCommand {
    String execute();
}
