package teme.exemple.trials.designPatterns.command;

public class OpenTextFileOperationImplCommand implements TextFileOperationCommand {
    private TextFileReceiver textFile;

    public OpenTextFileOperationImplCommand(TextFileReceiver textFile) {
        this.textFile = textFile;
    }

    @Override
    public String execute() {
        return textFile.open();
    }
}
