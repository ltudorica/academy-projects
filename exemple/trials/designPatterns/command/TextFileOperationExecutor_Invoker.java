package teme.exemple.trials.designPatterns.command;

import java.util.ArrayList;
import java.util.List;

public class TextFileOperationExecutor_Invoker {
    private final List<TextFileOperationCommand> textFileOperationCommands = new ArrayList<>();

    public String executeOperation(TextFileOperationCommand textFileOperationCommand) {
        textFileOperationCommands.add(textFileOperationCommand);
        return textFileOperationCommand.execute();
    }
}
