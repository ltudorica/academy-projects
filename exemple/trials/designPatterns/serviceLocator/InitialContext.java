package teme.exemple.trials.designPatterns.serviceLocator;

public class InitialContext {

    public Object lookup(String serviceName) {
        if (serviceName.equalsIgnoreCase("Emailservice")) {
            return new EmailService();
        } else if (serviceName.equalsIgnoreCase("SMSservice")) {
            return new SMSService();
        }
        return null;
    }
}
