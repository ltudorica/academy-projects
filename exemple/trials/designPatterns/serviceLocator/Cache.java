package teme.exemple.trials.designPatterns.serviceLocator;

import java.util.ArrayList;
import java.util.List;

public class Cache {
    private List<MessagingService> services = new ArrayList<>();

    public MessagingService getService(String serviceName) {
        if (services != null) {
            for (MessagingService messagingService : services) {
                if (messagingService.getClass().toGenericString().endsWith(serviceName)) {
                    return messagingService;
                }
            }
        }
        return null;
    }

    public void addService(MessagingService newService) {
        if (services != null) {
            services.add(newService);
        }
    }
}
