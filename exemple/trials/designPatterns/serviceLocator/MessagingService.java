package teme.exemple.trials.designPatterns.serviceLocator;

public interface MessagingService {
    String getMessageBody();

    String getServiceName();
}
