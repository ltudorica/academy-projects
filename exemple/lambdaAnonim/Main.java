package teme.exemple.lambdaAnonim;

public class Main {
    public static void main(String[] args) {

        BinaryOperation m = new Multiplication();
        int result = m.operation(4, 5);
        System.out.println(result);

        int x = 8;
        BinaryOperation d = new BinaryOperation() { //fctie anonima
            int finalX = x;

            @Override
            public int operation(int a, int b) {
                return a * b;
            }
        };

        d.operation(3, 4);

        new BinaryOperation() { //obiect anonim
            int finalX = x;

            @Override
            public int operation(int a, int b) {
                return a * b;
            }
        }.operation(3, 4); //trebuie sa bagam fctia aici ca nu mai putem accesa obiectul anonim

        BinaryOperation s = (a, b) -> a - b;

        System.out.println(s.operation(4, 5));
    }
}
