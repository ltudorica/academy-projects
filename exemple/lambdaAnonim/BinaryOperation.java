package teme.exemple.lambdaAnonim;

public interface BinaryOperation {
    int operation(int a, int b);
}
