package teme.exemple.lambdaAnonim;

public class Multiplication implements BinaryOperation {

    @Override
    public int operation(int a, int b) {
        return a * b;
    }
}
