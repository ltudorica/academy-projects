package teme.exemple.filesWeek2Day4;

import java.io.*;

public class TextFiles {
    public static void main(String[] args) throws IOException {

        File file = new File("C:\\Users\\liliana.tudorica\\IdeaProjects\\Java Academy\\src\\Exemple\\OOPWeek2Day1Day2\\StringPool"); // director si/sau fisier

        if (file.exists()) {
            if (file.isDirectory()) {
                System.out.println("The location is a folder");
            }
            File[] files = file.listFiles(); // creeam vector cu toate fisierele din calea data

            for (File innerFile : files) {
                System.out.println(innerFile.getName());
                String type = innerFile.isDirectory() ? "file" : "folder";
                System.out.println(innerFile.getName() + " - " + type);
            }

            // locatia curenta - .
        }
        System.out.println();

        //scrierea in fisier
        File logFile = new File("Logs.txt");

        if (!logFile.exists()) {
            logFile.createNewFile();
        }

        if (logFile.exists()) {
            FileWriter fileWriter = new FileWriter(logFile, false); // are metoda append

            PrintWriter printWriter = new PrintWriter(fileWriter); // pipeline
            printWriter.println("Logs");
            printWriter.println(String.format("%d;%s;%s", 1, "Files example", "Writing into files"));
            printWriter.println(String.format("%d;%s;%s", 2, "Files example", "Writing into files"));
            printWriter.println(String.format("%d;%s;%s", 3, "Files example", "Writing into files"));

            printWriter.close();

        }

        System.out.println("done");


        System.out.println();
        //read text file

        FileReader fileReader = new FileReader(logFile);

        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                //System.out.println("Content: " + line);
                String[] content = line.split(";");

                if (content.length == 3) {
                    int id = Integer.parseInt(content[0]);
                    String msg1 = content[1];
                    System.out.println(String.format("Id = %d, Msg 1 = %s", id, msg1));
                }
            }

        }


        System.out.println("done text parse");
    }
}
