package teme.exemple.filesWeek2Day4;

import java.io.*;

public class BinaryFiles {
    public static void main(String[] args) throws IOException {

        File binaryFile = new File("maydata.bin");
        FileOutputStream fos = new FileOutputStream(binaryFile); // pt scriere
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fos); //poti sa dai marimea la parametru
        DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);

        dataOutputStream.writeInt(2);
        dataOutputStream.writeBoolean(true);
        dataOutputStream.writeUTF("This is a test");
        dataOutputStream.writeFloat(12.4f);

        dataOutputStream.close();


        FileInputStream fis = new FileInputStream(binaryFile); // putem folosi bufferedInput sau nu
        try (DataInputStream dis = new DataInputStream(fis)) {
            int value = dis.readInt();
            boolean bValue = dis.readBoolean();
            String sValue = dis.readUTF();
            float fValue = dis.readFloat();

            String val = bValue ? "true" : "false";

            System.out.println(String.format("%d %s %s %f", value, val, sValue, fValue));
        }
    }
}
