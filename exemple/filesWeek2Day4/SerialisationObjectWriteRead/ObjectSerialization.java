package teme.exemple.filesWeek2Day4.SerialisationObjectWriteRead;

import java.io.Serializable;

public class ObjectSerialization implements Serializable {
    int value;
    boolean bValue;
    transient String sValue;
    float fValue;
    String[] values = new String[]{"Hello", "World"};
    MyOtherClass ref = new MyOtherClass();

    public ObjectSerialization(int value, boolean bValue, String sValue, float fValue) {
        this.value = value;
        this.bValue = bValue;
        this.sValue = sValue;
        this.fValue = fValue;
    }
}
