package teme.exemple.filesWeek2Day4.SerialisationObjectWriteRead;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File objectFile = new File("myobject.bin");
        FileOutputStream fos = new FileOutputStream(objectFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        ObjectSerialization os = new ObjectSerialization(2, true, "This is a test", 12.4f);
        oos.writeObject(os);

        oos.close();

        FileInputStream fis = new FileInputStream(objectFile);

        try (ObjectInputStream ois = new ObjectInputStream(fis)) {
            ObjectSerialization oosClone = (ObjectSerialization) ois.readObject();
            System.out.println(String.format("%d %s %f", oosClone.value, oosClone.sValue, oosClone.fValue));
        }

    }
}
