package teme.exemple.solid_example.locator;

import teme.exemple.solid_example.shapes.Circle;
import teme.exemple.solid_example.shapes.contracts.Shape;
import teme.exemple.solid_example.ui.LargeCircle;
import teme.exemple.solid_example.ui.contracts.LargeShape;

public class ServiceLocator {
    public static Shape getShape() {
        return new Circle(5);
    }

    public static LargeShape getLargeShape() {
        return new LargeCircle();
    }
}
