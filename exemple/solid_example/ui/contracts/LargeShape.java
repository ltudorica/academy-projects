package teme.exemple.solid_example.ui.contracts;

import teme.exemple.solid_example.shapes.contracts.Shape;

public interface LargeShape {

    boolean isLargeShape(Shape shape);

}
