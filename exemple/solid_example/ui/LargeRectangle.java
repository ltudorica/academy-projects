package teme.exemple.solid_example.ui;

import teme.exemple.solid_example.ui.contracts.LargeShape;
import teme.exemple.solid_example.shapes.contracts.Shape;

public class LargeRectangle implements LargeShape {

    @Override
    public boolean isLargeShape(Shape shape) {
        return shape.area() > 100;
    }
}
