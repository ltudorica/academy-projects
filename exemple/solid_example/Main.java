package teme.exemple.solid_example;

import teme.exemple.solid_example.locator.ServiceLocator;
import teme.exemple.solid_example.shapes.contracts.Shape;
import teme.exemple.solid_example.ui.contracts.LargeShape;

public class Main {
    public static void main(String[] args) {
        Shape s = ServiceLocator.getShape();
        LargeShape x = ServiceLocator.getLargeShape();
        System.out.println(x.isLargeShape(s));

        var z = 5;
//        z = "abc"; -> nu putem da alt tip, java deduce tipul


    }

    private static void f() {

        final int LEGAL_AGE = 18;
        int age = 0;
        if (age >= LEGAL_AGE) {
            //VDKMFK
        }

    }
}
