package teme.exemple.solid_example.shapes;

import teme.exemple.solid_example.shapes.contracts.Shape;

public class Circle implements Shape {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public int area() {
        return (int) (radius * radius * 3.14); //dk ai nevoie de alt tip returnat (ex: double), mai
        // bine implementezi o alta interfata gen preciseShape care returneaza un double
    }
}
