package teme.exemple.solid_example.shapes;

public class Rectangle {

    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int area() {
        // for a square we would modify this method - but it's not good - we should make a diff. class that inherites this one
        return width * height;
    }
}
