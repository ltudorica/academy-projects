package teme.exemple.solid_example.shapes;

import teme.exemple.solid_example.shapes.contracts.Shape;

public class Square implements Shape {

    private int width;

    public Square(int width) {
        this.width = width;
    }

    @Override
    public int area() {
        return width * width;
    }
}
