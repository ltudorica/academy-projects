package teme.exemple.solid_example.shapes.contracts;

public interface Shape {
    int area();
}
