package teme.exemple.command;

public class TextFileReceiver {
    private String name;

    public TextFileReceiver(String name) {
        this.name = name;
    }

    public String open() {
        return "Opening file " + name;
    }

    public String save() {
        return "Saving file " + name;
    }
}
