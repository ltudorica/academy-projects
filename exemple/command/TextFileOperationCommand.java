package teme.exemple.command;

public interface TextFileOperationCommand {
    String execute();
}
