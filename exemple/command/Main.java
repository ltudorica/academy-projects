package teme.exemple.command;

public class Main {
    public static void main(String[] args) {
        TextFileOperationExecutor_Invoker textFileOperationExecutor_invoker = new TextFileOperationExecutor_Invoker();
        textFileOperationExecutor_invoker.executeOperation(new OpenTextFileOperationImplCommand(new TextFileReceiver("bla")));

    }
}
