package teme.exemple.command;

public class SaveTextFileOperationImplCommand implements TextFileOperationCommand {
    private TextFileReceiver textFile;

    @Override
    public String execute() {
        return textFile.save();
    }
}
