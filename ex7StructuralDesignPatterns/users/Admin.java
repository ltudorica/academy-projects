package teme.ex7StructuralDesignPatterns.users;

import java.util.ArrayList;
import java.util.List;

public class Admin implements User {
    private String email;
    private List<User> addedUsers = new ArrayList<>();

    public Admin(String email) {
        this.email = email;
    }

    @Override
    public void printDetails() {
        System.out.println("Admin user: " + this.email);
        for (User user : addedUsers) {
            user.printDetails();
        }
    }

    @Override
    public void addUser(User user) {
        if (user != null) {
            this.addedUsers.add(user);
        }
    }

    @Override
    public void removeUser(User user) {
        this.addedUsers.remove(user);
    }

    @Override
    public User getUser(int index) {
        return this.addedUsers.get(index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Admin admin = (Admin) o;
        return this.email.equals(admin.email);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        return prime * email.hashCode();
    }
}
