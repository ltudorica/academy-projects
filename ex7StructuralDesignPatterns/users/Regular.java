package teme.ex7StructuralDesignPatterns.users;

import teme.ex7StructuralDesignPatterns.exceptions.NoSuchRightException;

import java.util.Objects;

public class Regular implements User {
    private String email;

    public Regular(String email) {
        this.email = email;
    }

    @Override
    public void printDetails() {
        System.out.println("Regular user: " + this.email);
    }

    @Override
    public void addUser(User user) throws NoSuchRightException {
        throw new NoSuchRightException();
    }

    @Override
    public void removeUser(User user) throws NoSuchRightException {
        throw new NoSuchRightException();
    }

    @Override
    public User getUser(int index) throws NoSuchRightException {
        throw new NoSuchRightException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Regular regular = (Regular) o;
        return this.email.equals(regular.email);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        return prime * email.hashCode();
    }
}
