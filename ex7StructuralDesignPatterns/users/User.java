package teme.ex7StructuralDesignPatterns.users;

import teme.ex7StructuralDesignPatterns.exceptions.NoSuchRightException;

public interface User {
    void printDetails();

    void addUser(User user) throws NoSuchRightException;

    void removeUser(User user) throws NoSuchRightException;

    User getUser(int index) throws NoSuchRightException;
}
