package teme.ex7StructuralDesignPatterns.oldVersion;

import teme.ex7StructuralDesignPatterns.exceptions.PasswordDoesntMatchException;

public interface CreateUser {
    void createUser(String email, String password) throws PasswordDoesntMatchException;
}
