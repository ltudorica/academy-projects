package teme.ex7StructuralDesignPatterns;

import teme.ex7StructuralDesignPatterns.newVersion.Registration;
import teme.ex7StructuralDesignPatterns.newVersion.UserRegistration;

import java.time.Duration;
import java.time.LocalDateTime;

public class RegistrationProxy implements UserRegistration {
    private LocalDateTime timeAccessedLimit;
    private static String lastEmail = "";
    private static int counter = 0;
    private UserRegistration userRegistration;

    public RegistrationProxy(UserRegistration userRegistration) {
        this.userRegistration = userRegistration;
    }

    @Override
    public boolean register(String email, String password, String repeatedPassword) {
        LocalDateTime timeNow = LocalDateTime.now();

        if (email.equals(lastEmail)) {
            if (counter == 2) {
                timeAccessedLimit = timeNow;
                counter++;
            } else if (counter >= 3) {
                Duration duration = Duration.between(timeAccessedLimit, timeNow);
                if (duration.getSeconds() < 20) {
                    System.out.println("You've tried too many times! Please try again in 20 seconds");
                    return false;
                } else {
                    counter = 1;
                    lastEmail = "";
                }
            } else {
                counter++;
            }
        } else {
            lastEmail = email;
            counter = 1;
        }

        return userRegistration.register(email, password, repeatedPassword);
    }


}
