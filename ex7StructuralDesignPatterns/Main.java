package teme.ex7StructuralDesignPatterns;

import teme.ex7StructuralDesignPatterns.exceptions.PasswordDoesntMatchException;
import teme.ex7StructuralDesignPatterns.newVersion.Registration;
import teme.ex7StructuralDesignPatterns.newVersion.UserRegistration;
import teme.ex7StructuralDesignPatterns.oldVersion.CreateUser;
import teme.ex7StructuralDesignPatterns.users.Admin;
import teme.ex7StructuralDesignPatterns.users.Manager;
import teme.ex7StructuralDesignPatterns.users.Regular;

public class Main {
    public static void main(String[] args) {

        UserRegistration userRegistration = new Registration();

        CreateUser createUser = new CreateUserToRegisterAdapter(userRegistration);
        try {
            createUser.createUser("bla@yahoo.com", "dschbdkjcn");
//            createUser.createUser("bla@yahoo.com", "dvfjvbndfnj");
        } catch (PasswordDoesntMatchException e) {
            e.printStackTrace();
        }

        Regular regular1 = new Regular("regular1@yahoo.com");
        Regular regular2 = new Regular("regular2@yahoo.com");
        Regular regular3 = new Regular("regular3@yahoo.com");

        Manager manager1 = new Manager("manager1@aol.com");
        Manager manager2 = new Manager("manager2@aol.com");
        Admin admin1 = new Admin("admin@hotmail.com");
        admin1.addUser(manager1);
        manager1.addUser(regular1);
        manager1.addUser(regular2);
        manager2.addUser(regular3);

        regular1.printDetails();
        System.out.println();
        manager1.printDetails();
        System.out.println();
        admin1.printDetails();

        RegistrationProxy proxy = new RegistrationProxy(userRegistration);
        proxy.register("bla@bla.com", "dfjhsbfjk", "dfjhsbfjk");
        proxy.register("bla@bla.com", "dfjhsbfjk", "dfjhsbfjk");
        proxy.register("bla@bla.com", "dfjhsbfjk", "dfjhsbfjk");
        proxy.register("bla@bla.com", "dfjhsbfjk", "dfjhsbfjk");

    }
}
