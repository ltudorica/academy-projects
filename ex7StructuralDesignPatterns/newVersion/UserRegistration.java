package teme.ex7StructuralDesignPatterns.newVersion;

public interface UserRegistration {
    boolean register(String email, String password, String repeatedPassword);
}
