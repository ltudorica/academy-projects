package teme.ex7StructuralDesignPatterns.newVersion;

import java.util.HashMap;
import java.util.Map;

public class Registration implements UserRegistration {
    private Map<String, String> registeredUsers = new HashMap<>();

    @Override
    public boolean register(String email, String password, String repeatedPassword) {
        if (password.equals(repeatedPassword)) {
            if (registeredUsers.containsKey(email)) {
                System.out.println("You are already registered, please login");
                return false;
            } else {
                registeredUsers.put(email, password);
                System.out.println("You were successfully registered");
                return true;
            }
        }

        System.out.println("Wrong Password!");
        return false;
    }

}
