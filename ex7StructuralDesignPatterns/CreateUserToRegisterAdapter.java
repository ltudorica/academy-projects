package teme.ex7StructuralDesignPatterns;

import teme.ex7StructuralDesignPatterns.exceptions.PasswordDoesntMatchException;
import teme.ex7StructuralDesignPatterns.newVersion.UserRegistration;
import teme.ex7StructuralDesignPatterns.oldVersion.CreateUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateUserToRegisterAdapter implements CreateUser {
    private UserRegistration userRegistration = null;

    public CreateUserToRegisterAdapter(UserRegistration userRegistration) {
        this.userRegistration = userRegistration;
    }
//
//    public Map<String, String> map() {
//        return userRegistration.ge
//    }

    @Override
    public void createUser(String email, String password) throws PasswordDoesntMatchException {
        if (!userRegistration.register(email, password, password)) {
            throw new PasswordDoesntMatchException();
        }


    }
}
