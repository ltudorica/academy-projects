package teme.ex3;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String s = "24.07.2021 abcd.defg12 5.4\n" +
                "25.07.2021 abcd.defg12 4.3\n" +
                "24.07.2021 abhjd.hasghd 3.5\n" +
                "26.07.2021 $sdhsgdh.ahdgh12 1.2\n" +
                "25.07.2021 alex.ionescu93 2.7\n" +
                "24.07.2021 dan.ion 2.9\n" +
                "24.07.2021 ana.popescu00 3.1\n" +
                "24.07.2021 ioanavasilescu12 3.5\n" +
                "26.07.2021 gloria.abcd23 1.9\n" +
                "25.07.2021 fast.4you23 1.5\n" +
                "25.07.2021 dan.marin45 5.3";

        String[] users = s.split("\n");
        int validUsersNo = 0;
        String[] aux0 = new String[users.length];

        for (String user : users) {
            String[] allInfo = user.split(" ");
            if (validate(allInfo[1])) {
                //System.out.println(user);
                aux0[validUsersNo++] = user;
            }
        }

        String[] validUsers = new String[validUsersNo];
        int j = 0;
        for (int i = 0; i < validUsersNo; i++) {
            validUsers[i] = aux0[i];
        }

        printStuff(validUsers);

    }

    public static void printStuff(String[] validUsers) {
        Arrays.sort(validUsers);
        //System.out.println(Arrays.toString(validUsers));
        double countTime = 0;
        int countUsers = 0;
        String auxS = validUsers[0];

        String[] auxArr = auxS.split(" ");
        String date = auxArr[0];

        for (String goodUser : validUsers) {
            String[] infoGoodUser = goodUser.split(" ");
            if (goodUser.startsWith(date)) {
                double time = Double.parseDouble(infoGoodUser[2]);
                countTime += time;
                countUsers++;
            } else {
                System.out.println("Date: " + niceDate(date) + " " + (float) (countTime / countUsers));
                //System.out.printf("Date: " + niceDate(date) + " %.2f" + "h\n", (double) (countTime / countUsers));
                countUsers = 1;
                float time = Float.parseFloat(infoGoodUser[2]);
                countTime = time;
                date = infoGoodUser[0];
            }
        }

        System.out.println("Date: " + niceDate(date) + " " + (float) (countTime / countUsers));
    }

    public static String niceDate(String date) {
        DateTimeFormatter formatter0 = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter0);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMMM-dd EEEE");
        return formatter.format(localDate);
    }

    public static boolean validate(String username) {
        if (username != null) {
            String matchingString = "([a-zA-Z][a-zA-Z]+)[.]([a-zA-Z][a-zA-Z]+)(\\d{2})";
            Pattern p = Pattern.compile(matchingString);
            Matcher m = p.matcher(username);

            return m.matches();
        }

        return false;
    }

//    The game validates the username to be in the following format: at least two letters followed by a point,
//    then followed by at least two letters which are followed by two decimals.
//    All the users that do not respect the format are considered cheaters and should be ignored in the computations.

//    Use a regex to validate the username and compute the average playing time per day for the valid users only,
//    displaying the result in the following format:

//    Date: 2021-July-24 Saturday. Avg playing time:  4.25 h

//    Date: 2021-July-25 Sunday. Avg playing time:  4.1 h

//    Date: 2021-July-26 Monday. Avg playing time:  1.9 h
}
