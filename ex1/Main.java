package teme.ex1;

public class Main {
    public static void main(String[] args) {
        double d = 37.88;
        int[] bla = countCoins(d);
        for (int i : bla) {
            System.out.print(i + " ");
        }
    }

    public static int[] countCoins(double amount) {
        // inmultesc suma castigata cu 100;
        // pun monezile intregi sa scap de virgula;

        if (amount > 1000) {  // limit for test range
            return null;
        }

        int newAmount = (int) Math.round(amount * 100.0); // sau Math.floor ca metoda de rotunjire la cel mai mare intreg
        int[] coins = {1, 2, 5, 10, 20, 50, 100, 200};

        int l = coins.length;
        int[] res = new int[l];
        //System.out.println(newAmount);

        for (int i = l - 1; i >= 0; i--) {
            if (newAmount / coins[i] >= 1) {
                res[i] = newAmount / coins[i];
                newAmount %= coins[i];
                //System.out.println(newAmount);
            }
        }


        // for reversing Collections.Reverse(lista);
        return res;
    }
}
