package teme.ex1.tests;

import teme.ex1.Main;
import teme.ex1.tests.categories.ImportantTest;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;

public class MainTests {
    private static int[] expectedOutput;

   /*@Before
    public void initializeArray() {
        expectedOutput = new int[8];
    }*/

    @BeforeClass
    public static void initializeArray() {
        expectedOutput = new int[8];
    }

    @Test
    public void jackpotForMinCoins() {
        float input = 0;

        int[] result = Main.countCoins(input);

        assertArrayEquals("Jackpot for 0 is not an empty array", expectedOutput, result);
    }

    @Category(ImportantTest.class)
    @Test
    public void jackpotForOneCent() {
        float input = 0.01f;

        int[] result = Main.countCoins(input);
        expectedOutput[0] = 1;
        assertArrayEquals("Jackpot for 1 is not correct", expectedOutput, result);
    }

//    @After
//    public void colleoctArray() {
//        expectedOutput = null;  //nu e recomandat, oricum il colecteaza garbage colector
//    }

    @After
    public void collectArray() {
        for (int i = 0; i < expectedOutput.length; i++) { // ca sa nu mai creem mereu arrayul in memorie
            expectedOutput[i] = 0;
        }

    }
}
