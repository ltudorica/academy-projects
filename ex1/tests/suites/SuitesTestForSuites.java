package teme.ex1.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(AllTests.class)
public class SuitesTestForSuites {
}
