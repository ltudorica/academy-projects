package teme.ex1.tests.suites;

import teme.ex1.tests.categories.ImportantTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Suite.SuiteClasses({AllTests.class})
@Categories.IncludeCategory(ImportantTest.class)
public class ImportantTests {
}
