package teme.ex1.tests.suites;

import teme.ex1.tests.FakeTests;
import teme.ex1.tests.MainTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MainTests.class, FakeTests.class})
public class AllTests {

}
