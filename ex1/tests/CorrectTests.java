package teme.ex1.tests;

import teme.ex1.Main;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class CorrectTests {

    @Test
    public void testCentSubUnit() {
        int[] result = Main.countCoins(8.012f);
        assertThat(result).containsExactly(1, 0, 0, 0, 0, 0, 0, 4);
    } // conformance

    @Test(timeout = 1000) //in ms s /1000
    public void testBigJackpot() {
        int[] result = Main.countCoins(1000.01);
        assertThat(result).isNull();
    }

    @Test
    public void testInverse() { // inverse relationship
        int[] coins = {1, 2, 5, 10, 20, 50, 100, 200};
        int[] result = Main.countCoins(37.88);

        long sum = 0;
        for (int i = 0; i < coins.length; i++) {
            sum += coins[i] * result[i];
        }

        assertThat(sum).isEqualTo(3788);
    }
}
