package teme.ex1.tests;

import org.junit.Test;

import static org.junit.Assert.*;

public class FakeTests {
    @Test
    public void testSum() {
        int expected = 3;

        int result = 1 + 2;

        assertEquals("The sum is not correct", expected, result);
    }
}
